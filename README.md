[//]: # (<p >)

[//]: # (    <p align="center" >)

[//]: # (        <strong>愿世界和平</strong> <br /><br />)

[//]: # (        铭记苦难，心系巴勒斯坦，哀悼逝者。<br />)

[//]: # (        中国屹立东方，自强自立，方能更好地守护和平，担当国际责任，为世界公正贡献力量。<br />)

[//]: # (        愿悲剧不再重演，和平永驻人间。)

[//]: # (    </p>)

[//]: # (    <div align="center">)

[//]: # (        <img src="./img/national/img.png" alt="Easy-Manager-Tool" width="75" style="align-content: center" /> )

[//]: # (        peace & love)

[//]: # (        <img src="./img/national/img_1.png" alt="Easy-Manager-Tool" width="99" style="align-content: center" />)

[//]: # (    </div>)

[//]: # (</p>)


<h1 align="center" style="text-align:center;">
    <img src="./img/logo.png" alt="Easy-Manager-Tool" width="150" style="align-content: center" />
    <div>
        Easy-Manager-Tool
    </div>
</h1>

<p align="center">
	<strong>打造软件行业首款集成工具，不管你是程序员，测试，运维等都可以使用该软件来提升自己的工作效率。</strong>
</p>

<p align="center">
    <a target="_blank" href="https://www.apache.org/licenses/LICENSE-2.0.txt">
		<img src="https://img.shields.io/badge/license-AGPL%203.0-red" alt="Apache 2" />
	</a>
   <a target="_blank" href="https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html">
		<img src="https://img.shields.io/badge/JDK-8-green.svg" alt="jdk-8" />
	</a>
    <a target="_blank" href="https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html">
		<img src="https://img.shields.io/badge/JDK-11-green.svg" alt="jdk-11" />
	</a>
    <a target="_blank" href="https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html">
		<img src="https://img.shields.io/badge/JDK-17-green.svg" alt="jdk-17" />
	</a>
    <a target="_blank" href='https://gitee.com/noear/socketd/stargazers'>
        <img src='https://gitee.com/noear/socketd/badge/star.svg' alt='gitee star'/>
    </a>
    <a href="https://jq.qq.com/?_wv=1027&k=kjB5JNiC">
	    <img src="https://img.shields.io/badge/%E5%BE%AE%E4%BF%A1%20%E5%92%8C%20QQ-875730567-red"/>
    </a>
</p>

Easy-Manager-Tool 的诞生是为了解决软件行业众多参与者使用种类繁多的工具问题，想象做为一个后端开发者，尤其是中小型公司的开发者，您可能不止需要会idea为您写业务代码，可能还需要下载Navicat、Redis、Kafka等等众多工具来管理项目中所用到的中间件，包括运维还需要使用ssh、sftp、jenkins、docker等。众多软件使软件参与者疲于安装、破解和管理，为此我们研发了一套工具管理器来解决这样的问题。

Easy-Manager-Tool 集成各类工具的核心使用方法，去繁就简，打造集成化程度高且专业的开、测、维一体化管理工具。

> ⭕本项目采用 `AGPL` 开源协议（抄袭牟利索赔100万），使用必须遵守国家法律法规，⛔不允许非法项目使用，后果自负❗

> 就连五星上将麦克阿瑟评论道：如果不使用一下Easy-Manager-Tool，你的职业生涯是不会完整的。

# 功能说明
* 服务器管理：解决服务器信息维护，支持批量修改服务器密码，批量添加服务器，可按组按角色分配服务器
* SSH SFTP：像FinalShell一样的终端连接，支持ctrl+c复制，ctrl+v粘贴，支持文件上传
* 操作日志：对SSH操作进行记录，可以查看到真个操作过程的命令和结果，进行审计
* 多联终端：可以同时操作太多服务器，可独立连接，上传文件同时持有
* 脚本管理：添加脚本，支持钩子调用，定时执行
* 自定义监控：支持 Prometheus metrics数据格式（会转成JSON）做数据源，支持自定义JSON格式数据源，支持自定义指标等
* 告警管理：自定义告警，同时可以通知 QQ 邮箱，钉钉等，自带告警管理页面，可以记录告警解决方案
* AI交流：支持讯飞以及openAI的AI智能交流
* 任务面板：可以通过任务面板管理团队计划以及任务分配完成情况
* Docker管理：Docker常规管理，包括拉取，上传，运行，终端等
* Docker云仓：支持建立Docker仓库，支持权限管理Docker仓库，方便多个服务器拉取镜像以及运行容器
* 权限管理：基于按钮的权限管理，以服务器为权限资源，不同角色分配不同的服务器组，管理不同的服务器，使团队协作更放心

[服务器管理使用教程](https://www.bilibili.com/video/BV18M411D7j2/?spm_id_from=333.999.0.0)

[监控管理使用教程](https://www.bilibili.com/video/BV1ru4y1N7oj/?spm_id_from=333.999.0.0)

# 运行配置

### 环境
* JDK1.8 -> JDK17
* MYSQL 8.0 及以上

### 配置
* JVM 参数需要添加 -Dpolyglot.engine.WarnInterpreterOnly=false，以免出现告警。
* 修改 application.yml
```yaml
config:
  # 为本机局域网IP，或公网IP，或域名，具体看你的访问方式
  intranet-ip: 
  database:
    # 需要改成你自己的数据库连接地址，数据库以及表会自动创建
    url: 
  file:
    # 文件存储位置，包括：临时文件，操作日志等都在这里(默认是linux 服务路径可以不改，win 可以改 D:\download\ 自己决定)
    path: 
```
### 启动
启动运行成功后可以直接通过url访问项目：
```text
http://内网IP:8081/easy
```
 默认账号：admin2023，默认密码：Admin@2023

### docker部署
Docker 版本的没有具体版本号，所以你只需要使用 latest 即可，每次拉取都是最新的，如果想了解新版本变化请关注版本更新。

>提示！
> 
>端口必须是是 8081 所以端口不需要改；
> 
>config.database.url 需要改成你自己的数据库连接地址，数据库以及表会自动创建，如果是本机那也得是宿主机的IP不能是127.0.0.1；
> 
>--config.intranet-ip 这个地址需要改成你宿主机的IP地址；
> 
>MySQL 要求8.0及以上；

```shell
# 拉取镜像
docker pull 875730567/easy-manager-tool:latest

# 运行容器，注意库名改变
docker run -d -p 8081:8081 \
--name easy-manager-tool \
-e JAVA_ARGS="--config.database.url=127.0.0.1:3306/easy_manager_tool
--config.database.name=root
--config.database.password=Dev@root2021
--config.intranet-ip=192.168.41.23" \
875730567/easy-manager-tool
```

运行成功后访问需要在端口后面加 /easy 才能访问成功，注意：这里一定使用的是宿主机的 IP，包括如果本机是 Docker，在本机访问也不要使用 127.0.0.1 或 localhost。

```
如：
http://192.168.41.23:8081/easy
```

账号：admin2023，密码：Admin@2023

# 项目安全
本项目按照等保三级的要求进行了相关整改，所有重要数据的请求都以加密的方式进行传输，包括数据库重要数据也是以加密的方式存在，可以放心使用。

> 如果要部署生产环境，请联系我微信:875730567。因为代码开源，意味着私钥被公布，数据很轻易攻破，所以部署生产环境联系我，修改密钥;

# UI展示

<img src="./img/img.png" alt="主界面" />
<img src="./img/img_2.png" alt="远程操作" />
<img src="./img/img_1.png" alt="操作日志" />
<img src="./img/img_6.png" alt="多联终端" />
<img src="./img/img_3.png" alt="指标设置" />
<img src="./img/img_4.png" alt="自定义监控面板" />
<img src="./img/img_5.png" alt="查看监控数据" />
<img src="./img/img_7.png" alt="任务面板" />
<img src="./img/img_8.png" alt="讯飞AI" />