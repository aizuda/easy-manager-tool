#!/bin/bash
if [ "$3" = "true" ]; then
  echo "docker rmi -f $1"
  docker rmi -f $1
  if [ $? -eq 0 ]; then
    echo "Successfully deleted the image"
  else
    echo "Failed to delete image.Status 500"
    exit
  fi
  echo "docker rm -f $4"
  docker rm -f $4
  if [ $? -eq 0 ]; then
    echo "Successfully deleted container"
  else
    echo "Failed to delete container.Status 500"
    exit
  fi
fi
echo "docker pull $1"
docker pull $1
# 判断命令是否执行成功
if [ "$2" != \"\" ] && [ $? -eq 0 ]; then
  eval $2
  if [ $? -eq 0 ]; then
    echo "Execution successful.Status 200"
  else
    echo "$2 Execution failed.Status 500"
  fi
  exit
else
  echo "Mirror pull failed.Status 500"
  exit
fi
echo "Please check the operating environment.Status 500"