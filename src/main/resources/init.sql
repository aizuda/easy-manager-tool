CREATE TABLE IF NOT EXISTS `tool_docker_cloud` (
                                     `id` bigint NOT NULL AUTO_INCREMENT,
                                     `docker_id` bigint DEFAULT NULL,
                                     `server_id` bigint DEFAULT NULL,
                                     `docker_project` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                     `docker_access_level` tinyint DEFAULT '0' COMMENT '0公有 1私有',
                                     `user_id` bigint DEFAULT NULL,
                                     `user_ids` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '多个用户以，分割',
                                     `tenant_id` bigint DEFAULT NULL,
                                     `create_time` datetime DEFAULT NULL,
                                     `update_time` datetime DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `tool_docker_cloud_user` (
                                          `id` bigint NOT NULL AUTO_INCREMENT,
                                          `cloud_id` bigint DEFAULT NULL,
                                          `user_id` bigint DEFAULT NULL,
                                          `tenant_id` bigint DEFAULT NULL,
                                          `create_time` datetime DEFAULT NULL,
                                          `update_time` datetime DEFAULT NULL,
                                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `tool_docker_config` (
                                      `id` bigint NOT NULL AUTO_INCREMENT,
                                      `server_id` bigint DEFAULT NULL,
                                      `config_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `config_user_name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `config_password` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `tenant_id` bigint DEFAULT NULL,
                                      `create_time` datetime DEFAULT NULL,
                                      `update_time` datetime DEFAULT NULL,
                                      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `tool_docker_list` (
                                    `id` bigint NOT NULL AUTO_INCREMENT,
                                    `server_id` bigint DEFAULT NULL,
                                    `list_port` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '2375',
                                    `tenant_id` bigint DEFAULT NULL,
                                    `create_time` datetime DEFAULT NULL,
                                    `update_time` datetime DEFAULT NULL,
                                    `list_ssh` tinyint DEFAULT '0',
                                    `list_local_port` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `tool_gpt_chart` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `gc_title` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                  `user_id` bigint DEFAULT NULL,
                                  `create_time` datetime DEFAULT NULL,
                                  `update_time` datetime DEFAULT NULL,
                                  `tenant_id` bigint DEFAULT NULL,
                                  `gc_contents` json DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `tool_gpt_setting` (
                                    `id` bigint NOT NULL AUTO_INCREMENT,
                                    `gs_open_ai_proxy` tinyint DEFAULT '0',
                                    `gs_open_ai_proxy_ip` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `gs_open_ai_proxy_port` int DEFAULT NULL,
                                    `gs_open_ai_proxy_address` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'https://api.openai.com/',
                                    `gs_open_ai_proxy_keys` text COLLATE utf8mb4_unicode_ci,
                                    `tenant_id` bigint DEFAULT NULL,
                                    `create_time` datetime DEFAULT NULL,
                                    `update_time` datetime DEFAULT NULL,
                                    `gs_xf_appid` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `gs_xf_secret` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `gs_xf_key` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `tool_monitor_alarm` (
                                      `id` bigint NOT NULL AUTO_INCREMENT,
                                      `ma_title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `ma_threshold` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `ma_frequency` bigint DEFAULT NULL,
                                      `ma_value` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `ma_handle` tinyint DEFAULT '0' COMMENT '是否处理',
                                      `ma_handle_result` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '处理结果',
                                      `ma_handle_user` bigint DEFAULT NULL COMMENT '处理人',
                                      `ma_handle_time` datetime DEFAULT NULL COMMENT '处理时间',
                                      `tenant_id` bigint DEFAULT NULL,
                                      `create_time` datetime DEFAULT NULL,
                                      `update_time` datetime DEFAULT NULL,
                                      `ma_text` text COLLATE utf8mb4_unicode_ci COMMENT 'json快照',
                                      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `tool_monitor_data` (
                                     `id` bigint NOT NULL AUTO_INCREMENT,
                                     `md_title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                     `md_pull_time` bigint DEFAULT '5' COMMENT 's',
                                     `md_start` tinyint DEFAULT '0',
                                     `md_system` tinyint NOT NULL DEFAULT '0' COMMENT '0 用户添加 1 系统添加',
                                     `create_time` datetime DEFAULT NULL,
                                     `update_time` datetime DEFAULT NULL,
                                     `tenant_id` bigint DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `tool_monitor_data_index` (
                                           `id` bigint NOT NULL AUTO_INCREMENT,
                                           `md_id` bigint DEFAULT NULL,
                                           `mdi_title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                           `mdi_threshold` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '告警阈值',
                                           `mdi_frequency` tinyint DEFAULT '2' COMMENT '连续次数',
                                           `mdi_alarm` tinyint DEFAULT NULL COMMENT '是否告警',
                                           `mdi_remarks` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
                                           `tenant_id` bigint DEFAULT NULL,
                                           `create_time` datetime DEFAULT NULL,
                                           `update_time` datetime DEFAULT NULL,
                                           PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `tool_monitor_data_index_item` (
                                                `id` bigint NOT NULL AUTO_INCREMENT,
                                                `md_id` bigint DEFAULT NULL,
                                                `mdi_id` bigint DEFAULT NULL,
                                                `mdii_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                                `mdii_exp` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                                `create_time` datetime DEFAULT NULL,
                                                `update_time` datetime DEFAULT NULL,
                                                `tenant_id` bigint DEFAULT NULL,
                                                PRIMARY KEY (`id`),
                                                KEY `mdi_id_idx` (`mdi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `tool_monitor_screen` (
                                       `id` bigint NOT NULL AUTO_INCREMENT,
                                       `ms_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                       `create_time` datetime DEFAULT NULL,
                                       `update_time` datetime DEFAULT NULL,
                                       `tenant_id` bigint DEFAULT NULL,
                                       `ms_json` json DEFAULT NULL COMMENT '存储大屏json数据',
                                       PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `tool_server` (
                               `id` bigint NOT NULL AUTO_INCREMENT,
                               `tenant_id` bigint DEFAULT NULL,
                               `server_group_id` bigint DEFAULT NULL,
                               `server_type_id` bigint DEFAULT NULL,
                               `server_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `server_desc` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `server_ip` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `server_port` bigint DEFAULT NULL,
                               `server_account` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `server_password` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `proxy_id` bigint DEFAULT NULL,
                               `pk_id` bigint DEFAULT NULL,
                               `server_timeout` int DEFAULT NULL,
                               `update_time` datetime DEFAULT NULL,
                               `create_time` datetime DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `tool_server_group` (
                                     `id` bigint NOT NULL AUTO_INCREMENT,
                                     `tenant_id` bigint DEFAULT NULL,
                                     `group_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                     `create_time` datetime DEFAULT NULL,
                                     `update_time` datetime DEFAULT NULL,
                                     `role_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
insert ignore into `tool_server_group` (`id`, `tenant_id`, `group_name`, `create_time`, `update_time`, `role_id`) values('1','1','正式环境','2023-12-05 15:42:48','2023-12-05 15:42:48','1');


CREATE TABLE IF NOT EXISTS `tool_server_log` (
                                   `id` bigint NOT NULL AUTO_INCREMENT,
                                   `user_id` bigint DEFAULT NULL,
                                   `server_id` bigint DEFAULT NULL,
                                   `log_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   `start_time` datetime DEFAULT NULL,
                                   `update_time` datetime DEFAULT NULL,
                                   `end_time` datetime DEFAULT NULL,
                                   `tenant_id` bigint DEFAULT NULL,
                                   `create_time` datetime DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `tool_server_pk` (
                                  `id` int NOT NULL AUTO_INCREMENT,
                                  `server_id` bigint DEFAULT NULL,
                                  `pk_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                  `pk_path` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                  `pk_content` text COLLATE utf8mb4_unicode_ci,
                                  `pk_password` text COLLATE utf8mb4_unicode_ci,
                                  `tenant_id` bigint DEFAULT NULL,
                                  `create_time` datetime DEFAULT NULL,
                                  `update_time` datetime DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `tool_server_proxy` (
                                     `id` bigint NOT NULL AUTO_INCREMENT,
                                     `proxy_mode` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                     `proxy_host` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                     `proxy_port` int DEFAULT NULL,
                                     `proxy_account` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                     `proxy_password` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                     `tenant_id` bigint DEFAULT NULL,
                                     `create_time` datetime DEFAULT NULL,
                                     `update_time` datetime DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `tool_server_type` (
                                    `id` bigint NOT NULL AUTO_INCREMENT,
                                    `type_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `create_time` datetime DEFAULT NULL,
                                    `update_time` datetime DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `tool_server_type` */

insert ignore into `tool_server_type`(`id`,`type_name`,`create_time`,`update_time`) values (1,'linux','2023-07-19 13:19:27','2023-07-19 13:19:21'),(2,'windows','2023-07-19 16:02:05','2023-07-19 16:02:05');

/*Table structure for table `tool_setting_alarm` */

CREATE TABLE IF NOT EXISTS `tool_setting_alarm` (
                                      `id` bigint NOT NULL AUTO_INCREMENT,
                                      `mail_host` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `mail_port` int DEFAULT NULL,
                                      `mail_user_name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `mail_password` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `mail_nick_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `dd_agent_id` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `dd_secret` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `dd_app_key` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `dd_access_token` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `tenant_id` bigint DEFAULT NULL,
                                      `create_time` datetime DEFAULT NULL,
                                      `update_time` datetime DEFAULT NULL,
                                      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `tool_setting_recharge_record` (
                                                `order_number` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
                                                `package_id` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
                                                `package_title` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                                `package_month` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                                `package_price` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                                `create_time` datetime DEFAULT NULL,
                                                `update_time` datetime DEFAULT NULL,
                                                `tenant_id` bigint DEFAULT NULL,
                                                `package_pay_result` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '(NULL)',
                                                `expiration_date` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `tool_setting_user` (
                                     `id` bigint NOT NULL AUTO_INCREMENT,
                                     `tenant_id` bigint DEFAULT NULL,
                                     `role_id` bigint DEFAULT NULL,
                                     `user_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                     `user_account` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                     `user_password` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                     `user_mail` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                     `create_time` datetime DEFAULT NULL,
                                     `update_time` datetime DEFAULT NULL,
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `user_account` (`user_account`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


insert ignore into `tool_setting_user`(`id`,`tenant_id`,`role_id`,`user_name`,`user_account`,`user_password`,`user_mail`,`create_time`,`update_time`) values (1,1,1,'管理员','admin2023','6d79017d98c5f86cc159c7b150e25f20',NULL,'2023-07-26 17:11:00','2023-10-18 11:38:50');

CREATE TABLE IF NOT EXISTS `tool_setting_task` (
                                     `id` bigint NOT NULL AUTO_INCREMENT,
                                     `user_id` bigint DEFAULT NULL,
                                     `task_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                     `task_content` text COLLATE utf8mb4_unicode_ci,
                                     `task_coefficient` decimal(10,1) DEFAULT NULL,
                                     `task_state` tinyint DEFAULT NULL,
                                     `task_open` tinyint DEFAULT '0',
                                     `start_date` date DEFAULT NULL,
                                     `end_date` date DEFAULT NULL,
                                     `create_time` datetime DEFAULT NULL,
                                     `update_time` datetime DEFAULT NULL,
                                     `tenant_id` bigint DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `tool_setting_menu` (
                                     `id` bigint NOT NULL AUTO_INCREMENT,
                                     `menu_name` varchar (600),
                                     `menu_path` varchar (1500),
                                     `menu_parent_id` bigint (20),
                                     `menu_sort` bigint (20),
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('1','面板','/dashboard,/dashboard/index',NULL,'100');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('2','服务器管理','/server',NULL,'200');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('3','服务器管理','/server/manager,/server/find','2','210');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('4','服务器设置','/server/setting','2','290');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('5','添加|编辑','/server/edit,/proxy/find,/serverSetting/findAll,/pk/find','3',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('6','删除','/server/del','3',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('7','远程','/server/findById,/terminal/monitor,/terminal/download,/terminal/del,/terminal/upload','3',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('8','操作日志','/server/log,/serverLog/find','2','220');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('9','删除','/serverLog/del','8',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('10','查看','/file/look','8',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('11','系统设置','/setting',NULL,'800000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('12','员工管理','/setting/staff,/setting/find','11','810000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('13','添加|编辑','/setting/edit,/role/findAll','12',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('14','删除','/setting/del','12',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('15','角色管理','/setting/role,/role/find','11','820000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('17','添加|编辑','/role/menu,/role/edit','15',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('18','删除','/role/del','15',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('20','服务器组','/serverSetting/find','4',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('21','添加|编辑','/serverSetting/edit','20',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('22','删除','/serverSetting/del','20',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('23','告警设置','/setting/alarm,/settingAlarm/find','11','830000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('24','编辑','/settingAlarm/edit','23',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('25','发送','/settingAlarm/test','23',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('26','任务面板','/setting/task,/task/find','11','840000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('27','添加|编辑','/task/edit','26',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('28','删除','/task/del','26',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('29','Docker管理','/docker',NULL,'500');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('30','配置','/server/findAll,/dockerCloud/editConfig','33',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('31','添加|编辑','/setting/findAll,/dockerCloud/edit','33',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('32','删除','/dockerCloud/del','33',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('33','docker云仓','/docker/cloud,/dockerCloud/findConfig,/dockerCloud/find','29','510');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('34','docker服务管理','/docker/list,/dockerList/find','29','520');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('35','添加','/server/findAll,/dockerList/edit','34',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('36','删除','/dockerList/del','34',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('37','镜像','/dockerList/imageList','34',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('38','容器','/dockerList/containerList','34',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('39','挂载卷','/dockerList/volumeList','34',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('40','网络','/dockerList/networkList','34',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('41','镜像详情','/dockerList/imageInfo','37',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('42','拉取镜像','/dockerList/imagePull','37',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('43','推送镜像','/dockerCloud/findConfig,/dockerList/imagePush','37',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('44','删除镜像tag','/dockerList/imageTagDel','37',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('45','删除整个镜像','/dockerList/imageDel','37',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('46','日志','/dockerList/containerLog','38',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('47','详情','/dockerList/containerInfo','38',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('48','终端','/dockerList/containerTerminal','38',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('49','同步数据源','/dockerList/containerSynchronizationData','38',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('50','添加','/dockerList/containerAdd','38',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('51','运行或重启','/dockerList/containerRun','38',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('52','停止','/dockerList/containerStop','38',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('53','删除','/dockerList/containerDel','38',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('54','添加','/dockerList/volumeAdd','39',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('55','删除','/dockerList/volumeDel','39',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('56','删除','/dockerList/networkDel','40',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('57','查看镜像tag','/dockerCloud/tagList','60',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('58','拉取','/server/findAll,/dockerCloud/pullImages','60',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('59','删除tag','/dockerCloud/tagDel','60',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('60','进入','/dockerCloud/todo','33',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('61','监控管理','/monitor',NULL,'400');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('62','数据源管理','/monitor/find,/monitor/manager','61','410');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('63','自定义面板','/monitor/largeScreen,/screen/find','61','420');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('64','告警信息','/monitor/alarm,/alarm/find','61','430');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('65','添加|编辑|启动拉取','/monitor/edit,/monitor/getUrl,/monitor/urls,/monitor/delUrls,/monitor/getAllUrls','62',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('66','删除','/monitor/del','62',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('67','维护指标','/monitor/findIndex','62',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('68','添加指标','/monitor/getAllUrl,/monitor/editIndex,/monitor/findIndexItem','67',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('69','删除指标项','/monitor/delIndexItem','67',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('70','删除指标','/monitor/delIndex','67',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('71','启动告警','/monitor/editIndexAlarm','67',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('72','添加|编辑|制作|面板','/screen/edit,/screen/findById,/screen/findById,/monitor/treeIndexItem','63',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('73','删除','/screen/del','63',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('75','编辑','/alarm/edit','64',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('76','删除','/alarm/del','64',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('77','修改个人信息','/setting/findUser,/setting/editUser','12',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('78','数据通讯管理','/dbc',NULL,'300');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('79','数据库管理','/dbc/db,/db/find,/db/type/find','78',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('80','添加|编辑','/db/env/find,/db/pro/find,/server/findAll,/public/uploadStrategy,/db/driver/del,/db/testCon,/db/edit,/db/pro/del','79',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('81','删除','/db/del','79',NULL);
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('82','Easy-Manager-tool AI','/gpt/find,/gpt/findByContent',NULL,'900000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('84','编辑标题','/gpt/editTitle','82','900000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('85','删除对话','/gpt/del','82','900000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('86','添加新对话','/gpt/addWindow','82','900000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('87','发送对话','/gpt/sendMessage','82','900000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('88','AI设置','/gptSetting/find','82','900000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('89','编辑','/gptSetting/edit','88','900000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('91','多联终端','/remotePart/index','3','1000000000000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('92','上传','/remotePart/upload','91','1000000000000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('93','克隆','/monitor/clone','62','1000000000000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('94','启动拉取','/monitor/changeSwitch','62','1000000000000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('95','添加到数据源','/server/addMonitorData','3','1000000000000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('96','脚本管理','/server/script,/script/find','2','230');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('97','添加|编辑','/script/edit,/server/findAll,/script/command/find,/script/command/del','96','1000000000000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('98','删除','/script/del','96','1000000000000');
insert ignore into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('99','执行','/script/exec','96','1000000000000');
insert ignore  into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('100','日志','/script/log/find','96','1000000000000');
insert ignore  into `tool_setting_menu` (`id`, `menu_name`, `menu_path`, `menu_parent_id`, `menu_sort`) values('101','删除','/script/log/del','100','1000000000000');


CREATE TABLE IF NOT EXISTS `tool_setting_role` (
                                     `id` bigint NOT NULL AUTO_INCREMENT,
                                     `role_name` varchar (600),
                                     `role_password_fun` tinyint (4),
                                     `menu_ids` varchar (900),
                                     `tenant_id` bigint (20),
                                     `create_time` datetime ,
                                     `update_time` datetime,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
insert ignore into `tool_setting_role` (`id`, `role_name`, `role_password_fun`, `menu_ids`, `tenant_id`, `create_time`, `update_time`) values('1','超级管理员','1','1,2,3,5,6,7,91,92,95,8,9,10,96,97,98,99,100,101,4,20,21,22,78,79,81,80,61,62,67,69,68,70,71,66,65,93,94,63,73,72,64,76,75,29,33,60,57,58,59,30,31,32,34,35,36,37,41,42,43,44,45,38,50,46,47,48,49,51,52,53,39,54,55,40,56,11,12,77,13,14,15,17,18,23,24,25,26,27,28,82,84,88,89,87,86,85','1','2023-11-30 11:43:43','2023-12-05 09:31:04');

CREATE TABLE IF NOT EXISTS `tool_monitor_url` (
                                    `id` bigint NOT NULL AUTO_INCREMENT,
                                    `u_url` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `u_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `md_id` bigint DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `tool_server_script` (
                                      `id` bigint NOT NULL AUTO_INCREMENT,
                                      `script_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `server_id` bigint DEFAULT NULL,
                                      `tenant_id` bigint DEFAULT NULL,
                                      `update_time` datetime DEFAULT NULL,
                                      `create_time` datetime DEFAULT NULL,
                                      `script_global_path` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `script_hook` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `tool_server_script_command` (
                                              `id` bigint NOT NULL AUTO_INCREMENT,
                                              `script_id` bigint DEFAULT NULL,
                                              `ssc_command` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                              `ssc_order` int DEFAULT NULL,
                                              `tenant_id` bigint DEFAULT NULL,
                                              `create_time` datetime DEFAULT NULL,
                                              `update_time` datetime DEFAULT NULL,
                                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `tool_server_script_log` (
                                          `id` bigint NOT NULL AUTO_INCREMENT,
                                          `script_id` bigint DEFAULT NULL,
                                          `script_log` text COLLATE utf8mb4_unicode_ci,
                                          `tenant_id` bigint DEFAULT NULL,
                                          `create_time` datetime DEFAULT NULL,
                                          `update_time` datetime DEFAULT NULL,
                                          `log_trigger` tinyint DEFAULT '0' COMMENT '0手动 1钩子 2定时',
                                          `log_status` tinyint DEFAULT '0' COMMENT '0失败 1成功',
                                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;