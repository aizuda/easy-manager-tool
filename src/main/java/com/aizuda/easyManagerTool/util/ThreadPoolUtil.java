package com.aizuda.easyManagerTool.util;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

public class ThreadPoolUtil {

    private ThreadPoolTaskExecutor executor;
    // 核心活跃的线程数
    private Integer coreSize = 10;
    // 最大活跃的线程数
    private Integer maxSize = 50;
    private Integer queueSize = 200;
    // 非核心线程存活时间
    private Integer keepSeconds = 30;

    public ThreadPoolUtil(int core,int max,int queueSize,String name) {
        this.executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(core);
        executor.setMaxPoolSize(max);
        executor.setQueueCapacity(queueSize);
        executor.setKeepAliveSeconds(keepSeconds);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setThreadNamePrefix(name);
        executor.initialize();
    }

    public ThreadPoolUtil(String name) {
        this.executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(coreSize);
        executor.setMaxPoolSize(maxSize);
        executor.setQueueCapacity(queueSize);
        executor.setKeepAliveSeconds(keepSeconds);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setThreadNamePrefix(name);
        executor.initialize();
    }

    public ThreadPoolTaskExecutor getExecutor() {
        return executor;
    }

    public void execute(Runnable runnable){
        executor.execute(runnable);
    }

}
