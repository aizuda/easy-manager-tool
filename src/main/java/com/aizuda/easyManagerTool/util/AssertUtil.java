package com.aizuda.easyManagerTool.util;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easy.security.code.BasicCode;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;

public class AssertUtil {

    public static void objIsNull(Object obj, BasicCode code){
        if(ObjectUtil.isEmpty(obj) || ObjectUtil.isNull(obj)){
            throw new IllegalArgumentException(code.getMsg());
        }
    }

    public static void objIsNull(Object obj, String msg){
        if(ObjectUtil.isEmpty(obj) || ObjectUtil.isNull(obj)){
            throw new IllegalArgumentException(msg);
        }
    }

    public static void objIsNotNull(Object obj, String msg){
        if(ObjectUtil.isNotNull(obj) || ObjectUtil.isNotEmpty(obj)){
            throw new IllegalArgumentException(msg);
        }
    }

    public static void objIsNotNull(Object obj, BasicCode code){
        if(ObjectUtil.isNotNull(obj) || ObjectUtil.isNotEmpty(obj)){
            throw new IllegalArgumentException(code.getMsg());
        }
    }

    public static void objEQ(Object obj1, Object obj2,String msg){
        objIsNull(obj1,msg);
        objIsNull(obj2,msg);
        if(!obj1.equals(obj2)){
            throw new IllegalArgumentException(msg);
        }
    }

    public static void strEQ(Object obj1, Object obj2,String msg){
        objIsNull(obj1,msg);
        objIsNull(obj2,msg);
        if(!obj1.equals(obj2)){
            throw new IllegalArgumentException(msg);
        }
    }

    public static class Time{
        /**
         * 比较日期 start 是否在 end之前
         * @param start
         * @param end
         * @return
         */
        public static void eqDate(String start,String end,String msg){
            LocalDate date1 = LocalDate.parse(start);
            LocalDate date2 = LocalDate.parse(end);
            if(!date1.isBefore(date2)){
                throw new IllegalArgumentException(msg);
            }
        }

        public static void eqDate(Date start, Date end, String msg){
            LocalDate date1 = start.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate date2 = end.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            if(!date1.isBefore(date2)){
                throw new IllegalArgumentException(msg);
            }
        }

        /**
         * 比较时间 start 是否在 end之前
         * @param start
         * @param end
         * @return
         */
        public static void eqDateTime(String start,String end,String msg){
            LocalDateTime date1 = LocalDateTime.parse(start);
            LocalDateTime date2 = LocalDateTime.parse(end);
            if(!date1.isBefore(date2)){
                throw new IllegalArgumentException(msg);
            }
        }

        public static void eqDateTime(Date start, Date end, String msg){
            LocalDateTime date1 = start.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            LocalDateTime date2 = end.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            if(!date1.isBefore(date2)){
                throw new IllegalArgumentException(msg);
            }
        }

    }

    public static class List{

        public static void isEmpty(java.util.List list, String msg){
            objIsNull(list,msg);
            if(CollUtil.isEmpty(list)){
                throw new IllegalArgumentException(msg);
            }
        }

        public static void isEmpty(Object[] obj, String msg){
            objIsNull(obj,msg);
            isEmpty(Arrays.asList(obj),msg);
        }

    }

}
