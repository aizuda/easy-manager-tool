package com.aizuda.easyManagerTool.domain.vo.docker;

import com.aizuda.easyManagerTool.domain.entity.docker.DockerListEntity;
import lombok.Data;

@Data
public class DockerListVO extends DockerListEntity {

    private String serverIp;
    private String serverName;
    private String serverAccount;
    private String serverPassword;
    private String serverPort;
    private Integer serverCpu;
    private Long serverMemory;
    private String dockerRootDir;
    private Integer imageNum;
    private Integer containerNum;
    private Integer volumeNum;
    private Integer networkNum;
    private Boolean state = false;
}
