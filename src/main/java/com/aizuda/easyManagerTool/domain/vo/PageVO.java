package com.aizuda.easyManagerTool.domain.vo;

import com.aizuda.easyManagerTool.domain.dto.PageDTO;

import java.util.List;

public class PageVO<T> {

    private Long current;

    private Long size;

    private Long total;

    private List<T> records;

    public PageVO(PageDTO pageDTO) {
        this.current = pageDTO.getCurrent();
        this.size = pageDTO.getSize();
    }

    public PageVO() {

    }

    public Long getCurrent() {
        return current;
    }

    public PageVO<T> setCurrent(Long current) {
        this.current = current;
        return this;
    }

    public Long getSize() {
        return size;
    }

    public PageVO<T> setSize(Long size) {
        this.size = size;
        return this;
    }

    public Long getTotal() {
        return total;
    }

    public PageVO<T> setTotal(Long total) {
        this.total = total;
        return this;
    }

    public List<T> getRecords() {
        return records;
    }

    public PageVO<T> setRecords(List<T> records) {
        this.records = records;
        return this;
    }
}
