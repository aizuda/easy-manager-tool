package com.aizuda.easyManagerTool.domain.vo.docker;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.dockerjava.api.model.Network;
import lombok.Data;

import java.util.Map;

@Data
public class DockerNetworkListVO {

    private String state;
    private String containerName;

    @JsonProperty("Id")
    private String id;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Scope")
    private String scope;

    @JsonProperty("Driver")
    private String driver;

    @JsonProperty("EnableIPv6")
    private Boolean enableIPv6;

    @JsonProperty("Internal")
    private Boolean internal;

    @JsonProperty("IPAM")
    private Network.Ipam ipam;

    @JsonProperty("Containers")
    private Map<String, Network.ContainerNetworkConfig> containers;

    @JsonProperty("Options")
    private Map<String, String> options;

    @JsonProperty("Attachable")
    private Boolean attachable;

    @JsonProperty("Labels")
    public Map<String, String> labels;

}
