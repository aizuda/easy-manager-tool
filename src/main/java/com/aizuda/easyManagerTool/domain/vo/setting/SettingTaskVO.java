package com.aizuda.easyManagerTool.domain.vo.setting;

import com.aizuda.easyManagerTool.domain.entity.setting.SettingUserEntity;
import lombok.Data;

import java.util.List;

@Data
public class SettingTaskVO {

    private Integer userId;

    // 所有用户
    private List<SettingUserEntity> users;

    // 代办
    private List<SettingTaskFindVO> stays;

    // 执行
    private List<SettingTaskFindVO> executions;

    // 完成
    private List<SettingTaskFindVO> completes;

    // 关闭
    private List<SettingTaskFindVO> closes;

}
