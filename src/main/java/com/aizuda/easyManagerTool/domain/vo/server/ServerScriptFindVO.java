package com.aizuda.easyManagerTool.domain.vo.server;


import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptEntity;
import lombok.Data;

@Data
public class ServerScriptFindVO extends ServerScriptEntity {

    private String serverName;

}
