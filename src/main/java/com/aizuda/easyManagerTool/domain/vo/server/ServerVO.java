package com.aizuda.easyManagerTool.domain.vo.server;

import com.aizuda.easyManagerTool.domain.entity.server.ServerEntity;
import lombok.Data;

@Data
public class ServerVO extends ServerEntity {

    private String groupName;

    private String typeName;

}
