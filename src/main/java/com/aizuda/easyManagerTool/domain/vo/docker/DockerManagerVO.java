package com.aizuda.easyManagerTool.domain.vo.docker;

import com.aizuda.easyManagerTool.service.terminal.impl.TerminalSessionManager;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.ListVolumesResponse;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.api.model.Info;
import com.github.dockerjava.api.model.Network;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Data
@Slf4j
public class DockerManagerVO {

    private DockerClient dockerClient;
    private Info info;
    private List<Network> networks;
    private ListVolumesResponse volumes;
    private List<Image> images;
    private List<Container> containers;
    private TerminalSessionManager.Manager manager;
    private boolean state = false;

    public void refresh(){
        try {
            this.info = this.dockerClient.infoCmd().exec();
            this.networks = this.dockerClient.listNetworksCmd().exec();
            this.volumes = this.dockerClient.listVolumesCmd().exec();
            this.images = this.dockerClient.listImagesCmd().exec();
            this.containers = this.dockerClient.listContainersCmd()
                    .withShowSize(Boolean.TRUE)
                    .withShowAll(Boolean.TRUE).exec();
            this.state = true;
        }catch (Exception e){
            this.state = false;
//            log.debug("docker 连接失败 {}",e.getMessage());
        }
    }

}
