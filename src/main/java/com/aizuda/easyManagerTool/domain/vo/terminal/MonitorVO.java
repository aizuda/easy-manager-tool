package com.aizuda.easyManagerTool.domain.vo.terminal;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MonitorVO {

    private Float cpu;

    private Long memoryTotal;

    private Long memoryUse;

    private List<Disk> diskList = new ArrayList<>();

    @Data
    public static class Disk{
        private String name;
        private Long size;
        private Long use;
    }

}
