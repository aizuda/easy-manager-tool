package com.aizuda.easyManagerTool.domain.vo.dbc.db;


import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBDriverEntity;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBTypeEntity;
import lombok.Data;

import java.util.List;

@Data
public class DBTypeVO extends DBTypeEntity {

    private List<DBDriverEntity> driverList;

}
