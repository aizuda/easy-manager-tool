package com.aizuda.easyManagerTool.domain.vo.dashboard;

import lombok.Data;

@Data
public class DashboardFind15DayList {

    private String date;

    private Integer count;

}
