package com.aizuda.easyManagerTool.domain.vo.docker;

import com.aizuda.easyManagerTool.domain.entity.docker.DockerConfigEntity;
import lombok.Data;

@Data
public class DockerConfigFindVO extends DockerConfigEntity {

    private String serverName;

    private String serverIp;

}
