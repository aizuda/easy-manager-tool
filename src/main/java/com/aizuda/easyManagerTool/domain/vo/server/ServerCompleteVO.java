package com.aizuda.easyManagerTool.domain.vo.server;

import com.aizuda.easyManagerTool.domain.entity.server.ServerEntity;
import lombok.Data;

@Data
public class ServerCompleteVO extends ServerEntity {

    private Integer serverId;

    private String pkName;

    private String pkPath;

    private String pkContent;

    private String pkPassword;

    private String proxyMode;

    private String proxyHost;

    private Integer proxyPort;

    private String proxyAccount;

    private String proxyPassword;

}
