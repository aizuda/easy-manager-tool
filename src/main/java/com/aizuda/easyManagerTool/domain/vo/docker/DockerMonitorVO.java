package com.aizuda.easyManagerTool.domain.vo.docker;

import lombok.Data;

import java.util.Map;

@Data
public class DockerMonitorVO {

    private Double cpuUse;
    private Double memoryUse;
    // 接收
    private Map<String,NetWork> netWork;


    @Data
    public static class NetWork{
        private Double rxDataPackage;
        private Double txDataPackage;
        private String unit;

        public NetWork(Double rxDataPackage, Double txDataPackage, String unit) {
            this.rxDataPackage = rxDataPackage;
            this.txDataPackage = txDataPackage;
            this.unit = unit;
        }
    }
}
