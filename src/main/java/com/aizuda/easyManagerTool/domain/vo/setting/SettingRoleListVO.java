package com.aizuda.easyManagerTool.domain.vo.setting;

import com.aizuda.easyManagerTool.domain.entity.setting.SettingRoleEntity;
import lombok.Data;

import java.util.List;

@Data
public class SettingRoleListVO extends SettingRoleEntity {

    private List<SettingMenuListVO> menus;

}
