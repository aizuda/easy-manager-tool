package com.aizuda.easyManagerTool.domain.vo.setting;

import com.aizuda.easyManagerTool.domain.entity.setting.SettingMenuEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SettingMenuListVO extends SettingMenuEntity {

    private List<SettingMenuListVO> children = new ArrayList<>();

}
