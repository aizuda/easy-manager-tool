package com.aizuda.easyManagerTool.domain.vo.server;

import com.aizuda.easyManagerTool.domain.PublicEntity;
import lombok.Data;

@Data
public class ServerSettingVO extends PublicEntity {

    private String settingName;

    private String settingRoles;

    private Integer id;

}
