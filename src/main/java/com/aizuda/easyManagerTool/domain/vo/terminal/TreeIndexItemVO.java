package com.aizuda.easyManagerTool.domain.vo.terminal;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TreeIndexItemVO {

    private String id;

    private String label;

    private List<TreeIndexItemVO> children = new ArrayList<>();

}
