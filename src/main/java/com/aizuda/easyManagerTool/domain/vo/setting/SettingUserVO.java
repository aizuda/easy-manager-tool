package com.aizuda.easyManagerTool.domain.vo.setting;

import com.aizuda.easyManagerTool.domain.bo.PackagesBO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingUserEntity;
import lombok.Data;

import java.util.List;

@Data
public class SettingUserVO extends SettingUserEntity {

    private String token;

    private PackagesBO packageBO;

    private SettingRoleListVO settingRoleListVO;

    private List<String> path;

}
