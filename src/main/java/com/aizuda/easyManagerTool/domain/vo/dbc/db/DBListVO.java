package com.aizuda.easyManagerTool.domain.vo.dbc.db;

import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBEntity;
import lombok.Data;

@Data
public class DBListVO extends DBEntity {

    private String envName;
    private String typeName;
}
