package com.aizuda.easyManagerTool.domain.vo.socket;

import lombok.Data;

@Data
public class SocketMessageVO<T> {

    private String type;

    private T obj;

}
