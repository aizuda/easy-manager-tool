package com.aizuda.easyManagerTool.domain.vo.terminal;

import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorDataEntity;
import lombok.Data;

@Data
public class TreeIndexItemSelectVO extends MonitorDataEntity {

    private Integer tmdiId;

    private Integer tmdParentId;

    private String mdiTitle;

    private Integer tmdiiId;

    private Integer tmdiParentId;

    private String mdiiName;

    private String mdiiExp;

    private String mdiThreshold;

    private Integer mdiFrequency;

    private Boolean mdiAlarm;
}
