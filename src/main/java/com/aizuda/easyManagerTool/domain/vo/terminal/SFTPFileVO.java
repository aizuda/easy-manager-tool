package com.aizuda.easyManagerTool.domain.vo.terminal;

import lombok.Data;

@Data
public class SFTPFileVO {
    /**
     * 文件的权限
     **/
    private String auth;
    /**
     * 文件的个数
     **/
    private String num;
    /**
     * 拥有者
     **/
    private String user;
    /**
     * 所属用户组
     **/
    private String group;
    /**
     * 表示文件大小。文件大小用byte来表示，而空目录一般都是1024byte，当然可以用其它参数使文件显示的单位不同，如使用ls –k就是用kb莱显示一个文件的大小单位，不过一般我们还是以byte为主。
     **/
    private String size;
    /**
     * 表示最后一次修改时间。以“月，日，时间”的格式表示，如Aug 15 5:46表示8月15日早上5:46分。
     **/
    private String lt;
    /**
     * 表示文件名。我们可以用ls –a显示隐藏的文件名。
     **/
    private String name;
    // 目录
    private Boolean isDir;
    // 普通文件
    private Boolean isReg;
    // 是否为符号链接
    private Boolean isLink;
}
