package com.aizuda.easyManagerTool.domain.vo.setting;

import com.aizuda.easyManagerTool.domain.entity.setting.SettingTaskEntity;
import lombok.Data;

@Data
public class SettingTaskFindVO extends SettingTaskEntity {

    private String userName;

}
