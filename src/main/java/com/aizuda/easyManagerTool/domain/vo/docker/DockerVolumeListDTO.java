package com.aizuda.easyManagerTool.domain.vo.docker;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class DockerVolumeListDTO {

    private String state;
    private String containerName;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Labels")
    private Map<String, String> labels;

    @JsonProperty("Driver")
    private String driver;

    @JsonProperty("Mountpoint")
    private String mountpoint;

    @JsonProperty("Options")
    private Map<String, String> options;

}
