package com.aizuda.easyManagerTool.domain.vo.docker;

import com.aizuda.easyManagerTool.sdk.docker.vo.RegistryGetManifestsVO;
import lombok.Data;

@Data
public class DockerCloudTagListVO {

    private String image;

    private String tag;

    private Integer cloudId;

    private Integer configId;

    private RegistryGetManifestsVO tagInfo;

}
