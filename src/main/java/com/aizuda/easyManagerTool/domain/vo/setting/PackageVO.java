package com.aizuda.easyManagerTool.domain.vo.setting;

import com.aizuda.easyManagerTool.domain.bo.PackagesBO;
import lombok.Data;

import java.util.List;

@Data
public class PackageVO {

    private List<PackagesBO> list;
    private PackagesBO packagesBO;

}
