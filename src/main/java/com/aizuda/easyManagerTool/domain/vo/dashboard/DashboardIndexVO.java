package com.aizuda.easyManagerTool.domain.vo.dashboard;

import lombok.Data;

import java.util.List;

@Data
public class DashboardIndexVO {

    private Integer serverTotal;

    private Integer monitorDataTotal;

    private Integer monitorAlarmTotal;

    private List<DashboardFind15DayList> monitorAlarmList;

}
