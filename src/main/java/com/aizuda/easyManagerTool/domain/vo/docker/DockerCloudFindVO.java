package com.aizuda.easyManagerTool.domain.vo.docker;

import com.aizuda.easyManagerTool.domain.entity.docker.DockerCloudEntity;
import lombok.Data;

import java.util.List;

@Data
public class DockerCloudFindVO extends DockerCloudEntity {

    private Integer imageCount;

    private List<DockerCloudFindImageVO> images;

    private String userName;

}
