package com.aizuda.easyManagerTool.domain.vo.docker;

import lombok.Data;

@Data
public class DockerCloudFindImageVO {

    private String image;

    private Integer tagCount;

    private Integer cloudId;

    private Integer configId;

}
