package com.aizuda.easyManagerTool.domain.vo.setting;

import lombok.Data;

import java.util.List;

@Data
public class SettingMenuVO {

    private List<SettingMenuListVO> allMenu;

    private List<SettingMenuListVO> roleMenu;

}
