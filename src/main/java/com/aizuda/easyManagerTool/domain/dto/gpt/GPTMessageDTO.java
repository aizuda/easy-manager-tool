package com.aizuda.easyManagerTool.domain.dto.gpt;

import com.aizuda.easyManagerTool.domain.entity.gpt.GPTChartEntity;
import com.plexpt.chatgpt.entity.chat.FunctionCallResult;
import lombok.Data;

@Data
public class GPTMessageDTO extends GPTChartEntity {

    private String role;
    private String content;
    private String name;
    private FunctionCallResult function;
    private String type;
    private String model;
}
