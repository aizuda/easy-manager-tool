package com.aizuda.easyManagerTool.domain.dto.docker;

import lombok.Data;

@Data
public class SSHDockerDTO {
    private Integer id;
    private String containerId;
    private String cmd;
    private String randomId;
}
