package com.aizuda.easyManagerTool.domain.dto.docker;


import lombok.Data;

import java.util.List;

@Data
public class DockerListContainerDTO {
    private Integer id;

    private List<String> containerId;
    private String name;
    private Integer num;

    private String randomId;
    private String shMode;
}
