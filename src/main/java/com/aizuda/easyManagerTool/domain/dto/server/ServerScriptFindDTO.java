package com.aizuda.easyManagerTool.domain.dto.server;

import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptCommandEntity;
import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptEntity;
import lombok.Data;

import java.util.List;

@Data
public class ServerScriptFindDTO extends ServerScriptEntity {

    private List<ServerScriptCommandEntity> commands;

}
