package com.aizuda.easyManagerTool.domain.dto.server;

import lombok.Data;

import java.util.List;

@Data
public class ServerSettingDTO {

    private Integer id;
    private String settingName;
    private String key;
    private List<String> settingRoles;
}
