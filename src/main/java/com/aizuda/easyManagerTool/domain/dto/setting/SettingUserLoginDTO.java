package com.aizuda.easyManagerTool.domain.dto.setting;

import lombok.Data;

@Data
public class SettingUserLoginDTO {

    private String account;

    private String password;

    private String code;
}
