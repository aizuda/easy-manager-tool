package com.aizuda.easyManagerTool.domain.dto.docker;

import lombok.Data;

import java.util.List;

@Data
public class DockerPullImagesDTO {

    private List<Integer> serverIds;

    private Integer configId;

    private Integer cloudId;

    private String runScript;

    private String image;

    private String tag;

    private String digest;

    private Boolean delPredecessor;

    private String containerName;

}
