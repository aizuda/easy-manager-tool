package com.aizuda.easyManagerTool.domain.dto.socket;

import com.aizuda.easyManagerTool.domain.dto.terminal.SSHWindowsDTO;
import lombok.Data;

@Data
public class SocketMessageDTO {

    private String type;

    private Object json;

    private Long time;

    private SSHWindowsDTO window;
}
