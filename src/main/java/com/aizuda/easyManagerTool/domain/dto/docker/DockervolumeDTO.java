package com.aizuda.easyManagerTool.domain.dto.docker;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class DockervolumeDTO {

    private Integer id;
    private String name;
    private Map<String, String> labels;
    private String driver;
    private List<Map<String, String>> driverOpts;

}
