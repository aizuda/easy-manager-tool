package com.aizuda.easyManagerTool.domain.dto.setting;


import com.aizuda.easyManagerTool.domain.entity.setting.SettingAlarmEntity;
import lombok.Data;

@Data
public class SettingAlarmTestDTO extends SettingAlarmEntity {

    private String recipients;

    private String ddContent;

    private String tag;

}
