package com.aizuda.easyManagerTool.domain.dto.docker;


import com.aizuda.easyManagerTool.domain.tuple.Tuple2;
import com.aizuda.easyManagerTool.domain.tuple.Tuple3;
import lombok.Data;
import java.util.List;

@Data
public class DockerContainerAddDTO {

    private Integer id;
    private Integer configId;

    private String image;
    private String name;
    private String strategy;
    private String command;

    private List<Tuple3<Integer,Integer,String>> ports;
    private List<Tuple2<String,String>> volumes;
    private List<Tuple2> envs;
    private List<Tuple2<String,String>> labes;
    private Network network;
    private Long memoryReserved;
    private Long memoryLimit;
    private Long cpuNum;
    @Data
    public class Network{
        private String model;
        private String hostName;
        private String domainName;
        private String macAddress;
        private String ipv4Address;
        private String ipv6Address;
        private String primaryDNSServer;
        private String secondaryDNSServer;
    }

}
