package com.aizuda.easyManagerTool.domain.dto.docker;


import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class DockerNetworkDTO{

    private Integer id;
    private String networkId;

    private String name;

    private String driver;

    private Map<String, String> options = new HashMap<>();

    private Ipam ipam;

    private Boolean checkDuplicate;

    private Boolean internal;

    private Boolean enableIpv6;

    private Boolean attachable;

    private Map<String, String> labels;


    @Data
    class Ipam{
        private String driver;

        private List<Config> config = new ArrayList<>();

        private Map<String, String> options = null;
    }


    @Data
    class Config{
        private String subnet;

        private String ipRange;

        private String gateway;

        private String networkID;
    }

}
