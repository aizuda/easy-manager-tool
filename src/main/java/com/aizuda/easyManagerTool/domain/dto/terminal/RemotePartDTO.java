package com.aizuda.easyManagerTool.domain.dto.terminal;

import lombok.Data;

import java.util.List;

@Data
public class RemotePartDTO {

    private List<Integer> list;

    private String token;

    private SSHWindowsDTO window;

}
