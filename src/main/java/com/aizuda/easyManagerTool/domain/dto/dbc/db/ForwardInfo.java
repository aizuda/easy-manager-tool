package com.aizuda.easyManagerTool.domain.dto.dbc.db;


import lombok.Data;

@Data
public class ForwardInfo {

    private String rHost;
    private String rPort;
    private String localPort;

}
