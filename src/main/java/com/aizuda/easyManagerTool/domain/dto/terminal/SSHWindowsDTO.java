package com.aizuda.easyManagerTool.domain.dto.terminal;

import lombok.Data;

@Data
public class SSHWindowsDTO {

    private Integer id;

    private Integer clos;

    private Integer rows;

    private Integer width;

    private Integer height;

}
