package com.aizuda.easyManagerTool.domain.dto;

import lombok.Data;

@Data
public class PageDTO<T> {
    private T data;

    private Long current = 1L;

    private Long size = 20L;
}
