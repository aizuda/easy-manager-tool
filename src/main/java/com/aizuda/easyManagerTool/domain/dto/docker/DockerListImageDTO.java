package com.aizuda.easyManagerTool.domain.dto.docker;


import lombok.Data;

@Data
public class DockerListImageDTO {
    private Integer id;
    private String imageId;
    private String imageName;

    private Integer configId;
    private String oldTagName;
    private String newImageName;
    private String newTagName;

}
