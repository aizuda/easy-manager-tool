package com.aizuda.easyManagerTool.domain.dto.terminal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SSHMessageDTO {

    private Integer typeId;

    private Integer serverId;

    private String method;

    private SSHInfoDTO info;

    private SSHWindowsDTO window;

    private String command;

}
