package com.aizuda.easyManagerTool.domain.dto.terminal;

import lombok.Data;

@Data
public class RemotePartMsgDTO {

    private String token;
    private String command;
}
