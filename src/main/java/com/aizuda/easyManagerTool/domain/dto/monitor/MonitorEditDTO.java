package com.aizuda.easyManagerTool.domain.dto.monitor;


import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorDataEntity;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorUrlEntity;
import lombok.Data;

import java.util.List;

@Data
public class MonitorEditDTO extends MonitorDataEntity {

    private List<MonitorUrlEntity> urls;


}
