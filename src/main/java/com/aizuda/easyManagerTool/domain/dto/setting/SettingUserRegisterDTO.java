package com.aizuda.easyManagerTool.domain.dto.setting;

import lombok.Data;

@Data
public class SettingUserRegisterDTO {
    private Integer id;
    private String name;
    private String mail;
    private String account;
    private String password;
    private String okPassword;
    private Integer roleId;
}
