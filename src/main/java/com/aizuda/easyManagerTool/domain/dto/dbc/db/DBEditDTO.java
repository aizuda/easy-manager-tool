package com.aizuda.easyManagerTool.domain.dto.dbc.db;

import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBDriverEntity;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBEntity;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBEnvEntity;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBPropertiesEntity;
import lombok.Data;

import java.util.List;

@Data
public class DBEditDTO extends DBEntity {

    private DBEnvEntity env;

    private DBDriverEntity driver;

    private List<DBPropertiesEntity> dbProperties;

    private String typeName;

}
