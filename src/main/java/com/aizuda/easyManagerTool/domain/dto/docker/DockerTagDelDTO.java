package com.aizuda.easyManagerTool.domain.dto.docker;


import lombok.Data;

@Data
public class DockerTagDelDTO {

    private Integer configId;

    private Integer dockerId;

    private String image;

    private String tag;

}
