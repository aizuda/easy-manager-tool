package com.aizuda.easyManagerTool.domain.dto.terminal;

import lombok.Data;

@Data
public class SFTPFileDTO {

    private String path;
    private String token;
    private Boolean isDir;
    private String fileName;
}
