package com.aizuda.easyManagerTool.domain.dto.terminal;

import com.aizuda.easyManagerTool.domain.dto.dbc.db.ForwardInfo;
import com.aizuda.easyManagerTool.domain.vo.server.ServerCompleteVO;
import lombok.Data;

@Data
public class SSHInfoDTO extends ServerCompleteVO {

    private ForwardInfo forwardInfo = new ForwardInfo();

}
