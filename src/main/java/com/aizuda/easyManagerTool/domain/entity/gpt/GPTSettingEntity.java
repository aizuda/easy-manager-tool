package com.aizuda.easyManagerTool.domain.entity.gpt;

import com.aizuda.easyManagerTool.config.CiphertextTypeHandler;
import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_gpt_setting",autoResultMap = true)
public class GPTSettingEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "gs_open_ai_proxy")
    private Boolean gsOpenAiProxy;

    @TableField(value = "gs_open_ai_proxy_ip",typeHandler = CiphertextTypeHandler.class)
    private String gsOpenAiProxyIp;

    @TableField(value = "gs_open_ai_proxy_port")
    private Integer gsOpenAiProxyPort;

    @TableField(value = "gs_open_ai_proxy_address",typeHandler = CiphertextTypeHandler.class)
    private String gsOpenAiProxyAddress;

    @TableField(value = "gs_open_ai_proxy_keys",typeHandler = CiphertextTypeHandler.class)
    private String gsOpenAiProxyKeys;

    @TableField(value = "gs_xf_appid",typeHandler = CiphertextTypeHandler.class)
    private String gsXfAppid;

    @TableField(value = "gs_xf_secret",typeHandler = CiphertextTypeHandler.class)
    private String gsXfSecret;

    @TableField(value = "gs_xf_key",typeHandler = CiphertextTypeHandler.class)
    private String gsXfKey;

}
