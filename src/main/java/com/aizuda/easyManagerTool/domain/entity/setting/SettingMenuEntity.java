package com.aizuda.easyManagerTool.domain.entity.setting;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_setting_menu")
public class SettingMenuEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "menu_name")
    private String menuName;

    @TableField(value = "menu_path")
    private String menuPath;

    @TableField(value = "menu_parent_id")
    private Integer menuParentId;

}
