package com.aizuda.easyManagerTool.domain.entity.server;

import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_server_script_command")
public class ServerScriptCommandEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "script_id")
    private Integer scriptId;

    @TableField(value = "ssc_command")
    private String sscCommand;

    @TableField(value = "ssc_order")
    private Integer sscOrder;

}
