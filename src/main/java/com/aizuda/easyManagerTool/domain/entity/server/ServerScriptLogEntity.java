package com.aizuda.easyManagerTool.domain.entity.server;

import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_server_script_log")
public class ServerScriptLogEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "script_id")
    private Integer scriptId;

    @TableField(value = "script_log")
    private String scriptLog;

    @TableField(value = "log_trigger")
    private Integer logTrigger;

    @TableField(value = "log_status")
    private Boolean logStatus;

}
