package com.aizuda.easyManagerTool.domain.entity.monitor;

import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tool_monitor_data_index_item")
public class MonitorDataIndexItemEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "md_id")
    private Integer mdId;

    @TableField(value = "mdi_id")
    private Integer mdiId;

    @TableField(value = "mdii_name")
    private String mdiiName;

    @TableField(value = "mdii_exp")
    private String mdiiExp;

}
