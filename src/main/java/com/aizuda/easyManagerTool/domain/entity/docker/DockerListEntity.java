package com.aizuda.easyManagerTool.domain.entity.docker;

import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
@TableName(value = "tool_docker_list",autoResultMap = true)
public class DockerListEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "server_id")
    private Integer serverId;

    @TableField(value = "list_port")
    private String listPort;

    @TableField(value = "list_ssh")
    private Boolean listSsh;

    @TableField(value = "list_local_port")
    private String listLocalPort;


    @TableField(exist = false)
    private List<Integer> serverIds;

    @TableField(exist = false)
    private String serverName;

}
