package com.aizuda.easyManagerTool.domain.entity.server;


import com.aizuda.easyManagerTool.config.CiphertextTypeHandler;
import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_server_pk",autoResultMap = true)
public class ServerPKEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "server_id")
    private Integer serverId;

    @TableField(value = "pk_name")
    private String pkName;

    @TableField(value = "pk_path",typeHandler = CiphertextTypeHandler.class)
    private String pkPath;

    @TableField(value = "pk_content",typeHandler = CiphertextTypeHandler.class)
    private String pkContent;

    @TableField(value = "pk_password",typeHandler = CiphertextTypeHandler.class)
    private String pkPassword;

}
