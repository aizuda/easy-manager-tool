package com.aizuda.easyManagerTool.domain.entity.gpt;

import com.aizuda.easyManagerTool.config.JacksonTypeHandler;
import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


@Data
@TableName(value = "tool_gpt_chart",autoResultMap = true)
public class GPTChartEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "gc_title")
    private String gcTitle;

    @TableField(value = "gc_contents",typeHandler = JacksonTypeHandler.class)
    private Object gcContents;

    @TableField(value = "user_id")
    private Integer userId;

    @TableField(exist = false)
    private boolean titleAble = false;
}
