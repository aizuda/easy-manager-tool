package com.aizuda.easyManagerTool.domain.entity.server;

import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_server_script")
public class ServerScriptEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "server_id")
    private Integer serverId;

    @TableField(value = "script_name")
    private String scriptName;

    @TableField(value = "script_hook")
    private String scriptHook;

    @TableField(value = "script_global_path")
    private String scriptGlobalPath;

}
