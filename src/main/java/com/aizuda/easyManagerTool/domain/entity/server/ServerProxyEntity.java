package com.aizuda.easyManagerTool.domain.entity.server;

import com.aizuda.easyManagerTool.config.CiphertextTypeHandler;
import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


@Data
@TableName(value = "tool_server_proxy",autoResultMap = true)
public class ServerProxyEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "proxy_mode",typeHandler = CiphertextTypeHandler.class)
    private String proxyMode;

    @TableField(value = "proxy_host",typeHandler = CiphertextTypeHandler.class)
    private String proxyHost;

    @TableField(value = "proxy_port")
    private Integer proxyPort;

    @TableField(value = "proxy_account",typeHandler = CiphertextTypeHandler.class)
    private String proxyAccount;

    @TableField(value = "proxy_password",typeHandler = CiphertextTypeHandler.class)
    private String proxyPassword;


}
