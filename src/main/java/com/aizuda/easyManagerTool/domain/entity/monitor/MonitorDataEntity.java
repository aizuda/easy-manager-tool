package com.aizuda.easyManagerTool.domain.entity.monitor;

import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_monitor_data",autoResultMap = true)
public class MonitorDataEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "md_title")
    private String mdTitle;

    @TableField(value = "md_pull_time")
    private Integer mdPullTime;

    @TableField(value = "md_start")
    private Boolean mdStart;

    @TableField(value = "md_system")
    private Boolean mdSystem;

}
