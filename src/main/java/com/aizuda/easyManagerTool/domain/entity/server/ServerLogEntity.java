package com.aizuda.easyManagerTool.domain.entity.server;

import com.aizuda.easyManagerTool.config.CiphertextTypeHandler;
import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "tool_server_log",autoResultMap = true)
public class ServerLogEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    @TableField(value = "user_id")
    private Integer userId;

    @TableField(value = "server_id")
    private Integer serverId;

    @TableField(value = "log_path",typeHandler = CiphertextTypeHandler.class)
    private String logPath;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(value = "start_time")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(value = "end_time")
    private Date endTime;

    @TableField(exist = false)
    private String serverName;

    @TableField(exist = false)
    private String userName;

}
