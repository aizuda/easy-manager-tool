package com.aizuda.easyManagerTool.domain.entity.dbc.db;


import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_db_driver")
public class DBDriverEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "type_id")
    private Integer typeId;

    @TableField(value = "driver_name")
    private String driverName;

    @TableField(value = "driver_class_name")
    private String driverClassName;

}
