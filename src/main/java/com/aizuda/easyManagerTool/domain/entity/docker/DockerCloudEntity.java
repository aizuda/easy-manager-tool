package com.aizuda.easyManagerTool.domain.entity.docker;


import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_docker_cloud",autoResultMap = true)
public class DockerCloudEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "docker_id")
    private Integer dockerId;

    @TableField(value = "user_id")
    private Integer userId;


    @TableField(value = "server_id")
    private Integer serverId;

    @TableField(value = "docker_project")
    private String dockerProject;

    @TableField(value = "docker_access_level")
    private Boolean dockerAccessLevel;

    @TableField(value = "user_ids")
    private String userIds;

}
