package com.aizuda.easyManagerTool.domain.entity.dbc.db;


import cn.hutool.core.collection.CollUtil;
import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.google.common.collect.Maps;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@TableName(value = "tool_db_properties")
public class DBPropertiesEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "db_id")
    private Integer dbId;

    @TableField(value = "pro_name")
    private String proName;

    @TableField(value = "pro_value")
    private String proValue;

    public static Map<String, Object> toMap(List<DBPropertiesEntity> keyValues) {
        if (CollUtil.isEmpty(keyValues)) {
            return Maps.newHashMap();
        } else {
            Map<String, Object> map = Maps.newHashMap();
            keyValues.forEach(keyValue -> map.put(keyValue.getProName(), String.valueOf(keyValue.getProValue())));
            return map;
        }
    }

}
