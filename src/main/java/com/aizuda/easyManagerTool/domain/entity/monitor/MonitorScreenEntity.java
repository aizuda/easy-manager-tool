package com.aizuda.easyManagerTool.domain.entity.monitor;


import com.aizuda.easyManagerTool.config.JacksonTypeHandler;
import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


@Data
@TableName(value = "tool_monitor_screen",autoResultMap = true)
public class MonitorScreenEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "ms_name")
    private String msName;

    @TableField(value = "ms_json",typeHandler = JacksonTypeHandler.class)
    private Object msJson;

}
