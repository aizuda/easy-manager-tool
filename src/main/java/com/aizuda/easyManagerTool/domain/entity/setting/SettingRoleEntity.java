package com.aizuda.easyManagerTool.domain.entity.setting;


import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_setting_role")
public class SettingRoleEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "role_name")
    private String roleName;

    @TableField(value = "role_password_fun")
    private Boolean rolePasswordFun;

    @TableField(value = "menu_ids")
    private String menuIds;

}
