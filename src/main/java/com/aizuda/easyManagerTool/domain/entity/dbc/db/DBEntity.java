package com.aizuda.easyManagerTool.domain.entity.dbc.db;


import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_db")
public class DBEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "env_id")
    private Integer envId;

    @TableField(value = "type_id")
    private Integer typeId;

    @TableField(value = "driver_id")
    private Integer driverId;

    @TableField(value = "db_name")
    private String dbName;

    @TableField(value = "db_hosts")
    private String dbHosts;

    @TableField(value = "db_port")
    private String dbPort;

    @TableField(value = "db_auth")
    private Boolean dbAuth;

    @TableField(value = "db_account")
    private String dbAccount;

    @TableField(value = "db_password")
    private String dbPassword;

    @TableField(value = "db_url")
    private String dbUrl;

    @TableField(value = "server_id")
    private Integer serverId;

    @TableField(value = "db_server_enable")
    private Boolean dbServerEnable;



}
