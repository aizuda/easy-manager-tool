package com.aizuda.easyManagerTool.domain.entity.setting;

import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;



@Data
@TableName(value = "tool_setting_recharge_record")
public class SettingRechargeRecordEntity extends PublicEntity {

    @TableId(value = "order_number",type = IdType.NONE)
    private String orderNumber;

    @TableField(value = "package_id")
    private String packageId;

    @TableField(value = "package_title")
    private String packageTitle;

    @TableField(value = "package_month")
    private String packageMonth;

    @TableField(value = "package_price")
    private String packagePrice;

    @TableField(value = "package_pay_result")
    private String packagePayResult;

    @TableField(value = "expiration_date")
    private String expirationDate;

}
