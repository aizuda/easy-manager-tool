package com.aizuda.easyManagerTool.domain.entity.monitor;

import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;


@Data
@TableName("tool_monitor_alarm")
public class MonitorAlarmEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "ma_title")
    private String maTitle;

    @TableField(value = "ma_threshold")
    private String maThreshold;

    @TableField(value = "ma_frequency")
    private Integer maFrequency;

    @TableField(value = "ma_value")
    private String maValue;

    @TableField(value = "ma_handle")
    private Boolean maHandle;

    @TableField(value = "ma_handle_result")
    private String maHandleResult;

    @TableField(value = "ma_handle_user")
    private Integer maHandleUser;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(value = "ma_handle_time")
    private Date maHandleTime;

    @TableField(value = "ma_text")
    private String maText;

    @TableField(value = "user_name",exist = false)
    private String userName;

}
