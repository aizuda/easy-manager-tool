package com.aizuda.easyManagerTool.domain.entity.docker;


import com.aizuda.easyManagerTool.config.CiphertextTypeHandler;
import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_docker_config",autoResultMap = true)
public class DockerConfigEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "server_id")
    private Integer serverId;

    @TableField(value = "config_url",typeHandler =  CiphertextTypeHandler.class)
    private String configUrl;

    @TableField(value = "config_user_name",typeHandler =  CiphertextTypeHandler.class)
    private String configUserName;

    @TableField(value = "config_password",typeHandler =  CiphertextTypeHandler.class)
    private String configPassword;

}
