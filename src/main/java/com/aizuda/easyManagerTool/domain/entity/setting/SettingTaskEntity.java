package com.aizuda.easyManagerTool.domain.entity.setting;

import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "tool_setting_task")
public class SettingTaskEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "user_id")
    private Integer userId;

    @TableField(value = "task_title")
    private String taskTitle;

    @TableField(value = "task_content")
    private String taskContent;

    @TableField(value = "task_coefficient")
    private Float taskCoefficient;

    @TableField(value = "task_state")
    private Integer taskState;

    @TableField(value = "task_open")
    private Boolean taskOpen;

    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @TableField(value = "start_date")
    private Date startDate;

    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @TableField(value = "end_date")
    private Date endDate;

}
