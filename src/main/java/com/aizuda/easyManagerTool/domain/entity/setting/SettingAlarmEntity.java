package com.aizuda.easyManagerTool.domain.entity.setting;

import com.aizuda.easyManagerTool.config.CiphertextTypeHandler;
import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tool_setting_alarm",autoResultMap = true)
public class SettingAlarmEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "mail_host",typeHandler = CiphertextTypeHandler.class)
    private String mailHost;

    @TableField(value = "mail_port")
    private Integer mailPort;

    @TableField(value = "mail_user_name",typeHandler = CiphertextTypeHandler.class)
    private String mailUserName;

    @TableField(value = "mail_password",typeHandler = CiphertextTypeHandler.class)
    private String mailPassword;

    @TableField(value = "mail_nick_name")
    private String mailNickName;

    @TableField(value = "dd_agent_id",typeHandler = CiphertextTypeHandler.class)
    private String ddAgentId;

    @TableField(value = "dd_app_key",typeHandler = CiphertextTypeHandler.class)
    private String ddAppKey;

    @TableField(value = "dd_secret",typeHandler = CiphertextTypeHandler.class)
    private String ddSecret;

    @TableField(value = "dd_access_token",typeHandler = CiphertextTypeHandler.class)
    private String ddAccessToken;


}
