package com.aizuda.easyManagerTool.domain.entity.monitor;

import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
@TableName("tool_monitor_data_index")
public class MonitorDataIndexEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "md_id")
    private Integer mdId;

    @TableField(value = "mdi_title")
    private String mdiTitle;

    @TableField(value = "mdi_remarks")
    private String mdiRemarks;

    @TableField(value = "mdi_threshold")
    private String mdiThreshold;

    @TableField(value = "mdi_frequency")
    private Integer mdiFrequency;

    @TableField(value = "mdi_alarm")
    private Boolean mdiAlarm = false;

    @TableField(exist = false)
    private List<MonitorDataIndexItemEntity> exps;

}
