package com.aizuda.easyManagerTool.domain.entity.server;

import com.aizuda.easyManagerTool.config.CiphertextTypeHandler;
import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

@Data
@TableName(value = "tool_server",autoResultMap = true)
public class ServerEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "server_group_id")
    private Integer serverGroupId;

    @TableField(value = "server_type_id")
    private Integer serverTypeId;

    @TableField(value = "server_name")
    private String serverName;

    @TableField(value = "server_desc")
    private String serverDesc;

    @TableField(value = "server_ip",typeHandler = CiphertextTypeHandler.class)
    private String serverIp;

    @TableField(value = "server_port")
    private Integer serverPort;

    @TableField(value = "server_account",typeHandler = CiphertextTypeHandler.class)
    private String serverAccount;

    @TableField(value = "server_password",typeHandler = CiphertextTypeHandler.class)
    private String serverPassword;

    @TableField(value = "proxy_id",updateStrategy = FieldStrategy.IGNORED)
    private Integer proxyId;

    @TableField(value = "pk_id",updateStrategy = FieldStrategy.IGNORED)
    private Integer pkId;

    @TableField(value = "server_timeout")
    private Integer serverTimeout;

}
