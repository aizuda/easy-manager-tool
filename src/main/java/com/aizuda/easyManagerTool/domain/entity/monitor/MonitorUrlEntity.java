package com.aizuda.easyManagerTool.domain.entity.monitor;

import com.aizuda.easyManagerTool.config.CiphertextTypeHandler;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


@Data
@TableName(value = "tool_monitor_url",autoResultMap = true)
public class MonitorUrlEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "u_url",typeHandler = CiphertextTypeHandler.class)
    private String uurl;

    /**
     * json or metrics
     */
    @TableField(value = "u_type")
    private String utype;

    @TableField(value = "md_id")
    private Integer mdId;

}
