package com.aizuda.easyManagerTool.domain.entity.setting;


import com.aizuda.easyManagerTool.config.CiphertextTypeHandler;
import com.aizuda.easyManagerTool.domain.PublicEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
@TableName(value = "tool_setting_user",autoResultMap = true)
public class SettingUserEntity extends PublicEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "role_id")
    private Integer roleId;

    @TableField(value = "user_name")
    private String userName;

    @TableField(value = "user_account")
    private String userAccount;

    @JsonIgnore
    @TableField(value = "user_password")
    private String userPassword;

    @TableField(value = "user_mail",typeHandler = CiphertextTypeHandler.class)
    private String userMail;

    @TableField(exist = false)
    private String okPassword;


    @TableField(exist = false)
    private String roleName;

}
