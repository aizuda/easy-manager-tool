package com.aizuda.easyManagerTool.domain.bo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
public class PackagesBO {

    private Integer id;
    private String title;
    private BigDecimal price;
    private Integer month;
    private List<Item> items;


    @Data
    @AllArgsConstructor
    public static class Item{
        private String desc;
        private Integer num;
        private String unit;
        private Boolean open;
        private String service;
    }

}
