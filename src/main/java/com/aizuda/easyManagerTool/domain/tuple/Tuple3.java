package com.aizuda.easyManagerTool.domain.tuple;

import lombok.Setter;

@Setter
public class Tuple3<T0, T1, T2> extends Tuple {
    private static final long serialVersionUID = 1L;
    public T0 f0;
    public T1 f1;
    public T2 f2;

    public Tuple3() {
    }

    public Tuple3(T0 value0, T1 value1, T2 value2) {
        this.f0 = value0;
        this.f1 = value1;
        this.f2 = value2;
    }

    @Override
    public int getArity() {
        return 3;
    }
    @Override
    public <T> T getField(int pos) {
        switch (pos) {
            case 0:
                return (T) this.f0;
            case 1:
                return (T) this.f1;
            case 2:
                return (T) this.f2;
            default:
                throw new IndexOutOfBoundsException(String.valueOf(pos));
        }
    }
    @Override
    public <T> void setField(T value, int pos) {
        switch (pos) {
            case 0:
                this.f0 = (T0) value;
                break;
            case 1:
                this.f1 = (T1) value;
                break;
            case 2:
                this.f2 = (T2) value;
                break;
            default:
                throw new IndexOutOfBoundsException(String.valueOf(pos));
        }

    }

    public void setFields(T0 value0, T1 value1, T2 value2) {
        this.f0 = value0;
        this.f1 = value1;
        this.f2 = value2;
    }
    @Override
    public String toString() {
        return "(" + StringUtils.arrayAwareToString(this.f0) + "," + StringUtils.arrayAwareToString(this.f1) + "," + StringUtils.arrayAwareToString(this.f2) + ")";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof Tuple3)) {
            return false;
        } else {
            Tuple3 tuple;
            label44: {
                tuple = (Tuple3)o;
                if (this.f0 != null) {
                    if (this.f0.equals(tuple.f0)) {
                        break label44;
                    }
                } else if (tuple.f0 == null) {
                    break label44;
                }

                return false;
            }

            if (this.f1 != null) {
                if (!this.f1.equals(tuple.f1)) {
                    return false;
                }
            } else if (tuple.f1 != null) {
                return false;
            }

            if (this.f2 != null) {
                if (!this.f2.equals(tuple.f2)) {
                    return false;
                }
            } else if (tuple.f2 != null) {
                return false;
            }

            return true;
        }
    }
    @Override
    public int hashCode() {
        int result = this.f0 != null ? this.f0.hashCode() : 0;
        result = 31 * result + (this.f1 != null ? this.f1.hashCode() : 0);
        result = 31 * result + (this.f2 != null ? this.f2.hashCode() : 0);
        return result;
    }
    @Override
    public Tuple3<T0, T1, T2> copy() {
        return new Tuple3(this.f0, this.f1, this.f2);
    }

    public static <T0, T1, T2> Tuple3<T0, T1, T2> of(T0 value0, T1 value1, T2 value2) {
        return new Tuple3(value0, value1, value2);
    }

}
