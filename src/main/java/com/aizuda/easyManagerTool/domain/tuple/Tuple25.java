package com.aizuda.easyManagerTool.domain.tuple;

import lombok.Setter;

@Setter
public class Tuple25<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24> extends Tuple {
    private static final long serialVersionUID = 1L;
    public T0 f0;
    public T1 f1;
    public T2 f2;
    public T3 f3;
    public T4 f4;
    public T5 f5;
    public T6 f6;
    public T7 f7;
    public T8 f8;
    public T9 f9;
    public T10 f10;
    public T11 f11;
    public T12 f12;
    public T13 f13;
    public T14 f14;
    public T15 f15;
    public T16 f16;
    public T17 f17;
    public T18 f18;
    public T19 f19;
    public T20 f20;
    public T21 f21;
    public T22 f22;
    public T23 f23;
    public T24 f24;

    public Tuple25() {
    }

    public Tuple25(T0 value0, T1 value1, T2 value2, T3 value3, T4 value4, T5 value5, T6 value6, T7 value7, T8 value8, T9 value9, T10 value10, T11 value11, T12 value12, T13 value13, T14 value14, T15 value15, T16 value16, T17 value17, T18 value18, T19 value19, T20 value20, T21 value21, T22 value22, T23 value23, T24 value24) {
        this.f0 = value0;
        this.f1 = value1;
        this.f2 = value2;
        this.f3 = value3;
        this.f4 = value4;
        this.f5 = value5;
        this.f6 = value6;
        this.f7 = value7;
        this.f8 = value8;
        this.f9 = value9;
        this.f10 = value10;
        this.f11 = value11;
        this.f12 = value12;
        this.f13 = value13;
        this.f14 = value14;
        this.f15 = value15;
        this.f16 = value16;
        this.f17 = value17;
        this.f18 = value18;
        this.f19 = value19;
        this.f20 = value20;
        this.f21 = value21;
        this.f22 = value22;
        this.f23 = value23;
        this.f24 = value24;
    }
    @Override
    public int getArity() {
        return 25;
    }
    @Override
    public <T> T getField(int pos) {
        switch (pos) {
            case 0:
                return (T) this.f0;
            case 1:
                return (T) this.f1;
            case 2:
                return (T) this.f2;
            case 3:
                return (T) this.f3;
            case 4:
                return (T) this.f4;
            case 5:
                return (T) this.f5;
            case 6:
                return (T) this.f6;
            case 7:
                return (T) this.f7;
            case 8:
                return (T) this.f8;
            case 9:
                return (T) this.f9;
            case 10:
                return (T) this.f10;
            case 11:
                return (T) this.f11;
            case 12:
                return (T) this.f12;
            case 13:
                return (T) this.f13;
            case 14:
                return (T) this.f14;
            case 15:
                return (T) this.f15;
            case 16:
                return (T) this.f16;
            case 17:
                return (T) this.f17;
            case 18:
                return (T) this.f18;
            case 19:
                return (T) this.f19;
            case 20:
                return (T) this.f20;
            case 21:
                return (T) this.f21;
            case 22:
                return (T) this.f22;
            case 23:
                return (T) this.f23;
            case 24:
                return (T) this.f24;
            default:
                throw new IndexOutOfBoundsException(String.valueOf(pos));
        }
    }
    @Override
    public <T> void setField(T value, int pos) {
        switch (pos) {
            case 0:
                this.f0 = (T0) value;
                break;
            case 1:
                this.f1 = (T1) value;
                break;
            case 2:
                this.f2 = (T2) value;
                break;
            case 3:
                this.f3 = (T3) value;
                break;
            case 4:
                this.f4 = (T4) value;
                break;
            case 5:
                this.f5 = (T5) value;
                break;
            case 6:
                this.f6 = (T6) value;
                break;
            case 7:
                this.f7 = (T7) value;
                break;
            case 8:
                this.f8 = (T8) value;
                break;
            case 9:
                this.f9 = (T9) value;
                break;
            case 10:
                this.f10 = (T10) value;
                break;
            case 11:
                this.f11 = (T11) value;
                break;
            case 12:
                this.f12 = (T12) value;
                break;
            case 13:
                this.f13 = (T13) value;
                break;
            case 14:
                this.f14 = (T14) value;
                break;
            case 15:
                this.f15 = (T15) value;
                break;
            case 16:
                this.f16 = (T16) value;
                break;
            case 17:
                this.f17 = (T17) value;
                break;
            case 18:
                this.f18 = (T18) value;
                break;
            case 19:
                this.f19 = (T19) value;
                break;
            case 20:
                this.f20 = (T20) value;
                break;
            case 21:
                this.f21 = (T21) value;
                break;
            case 22:
                this.f22 = (T22) value;
                break;
            case 23:
                this.f23 = (T23) value;
                break;
            case 24:
                this.f24 = (T24) value;
                break;
            default:
                throw new IndexOutOfBoundsException(String.valueOf(pos));
        }

    }

    public void setFields(T0 value0, T1 value1, T2 value2, T3 value3, T4 value4, T5 value5, T6 value6, T7 value7, T8 value8, T9 value9, T10 value10, T11 value11, T12 value12, T13 value13, T14 value14, T15 value15, T16 value16, T17 value17, T18 value18, T19 value19, T20 value20, T21 value21, T22 value22, T23 value23, T24 value24) {
        this.f0 = value0;
        this.f1 = value1;
        this.f2 = value2;
        this.f3 = value3;
        this.f4 = value4;
        this.f5 = value5;
        this.f6 = value6;
        this.f7 = value7;
        this.f8 = value8;
        this.f9 = value9;
        this.f10 = value10;
        this.f11 = value11;
        this.f12 = value12;
        this.f13 = value13;
        this.f14 = value14;
        this.f15 = value15;
        this.f16 = value16;
        this.f17 = value17;
        this.f18 = value18;
        this.f19 = value19;
        this.f20 = value20;
        this.f21 = value21;
        this.f22 = value22;
        this.f23 = value23;
        this.f24 = value24;
    }
    @Override
    public String toString() {
        return "(" + StringUtils.arrayAwareToString(this.f0) + "," + StringUtils.arrayAwareToString(this.f1) + "," + StringUtils.arrayAwareToString(this.f2) + "," + StringUtils.arrayAwareToString(this.f3) + "," + StringUtils.arrayAwareToString(this.f4) + "," + StringUtils.arrayAwareToString(this.f5) + "," + StringUtils.arrayAwareToString(this.f6) + "," + StringUtils.arrayAwareToString(this.f7) + "," + StringUtils.arrayAwareToString(this.f8) + "," + StringUtils.arrayAwareToString(this.f9) + "," + StringUtils.arrayAwareToString(this.f10) + "," + StringUtils.arrayAwareToString(this.f11) + "," + StringUtils.arrayAwareToString(this.f12) + "," + StringUtils.arrayAwareToString(this.f13) + "," + StringUtils.arrayAwareToString(this.f14) + "," + StringUtils.arrayAwareToString(this.f15) + "," + StringUtils.arrayAwareToString(this.f16) + "," + StringUtils.arrayAwareToString(this.f17) + "," + StringUtils.arrayAwareToString(this.f18) + "," + StringUtils.arrayAwareToString(this.f19) + "," + StringUtils.arrayAwareToString(this.f20) + "," + StringUtils.arrayAwareToString(this.f21) + "," + StringUtils.arrayAwareToString(this.f22) + "," + StringUtils.arrayAwareToString(this.f23) + "," + StringUtils.arrayAwareToString(this.f24) + ")";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof Tuple25)) {
            return false;
        } else {
            Tuple25 tuple = (Tuple25)o;
            if (this.f0 != null) {
                if (!this.f0.equals(tuple.f0)) {
                    return false;
                }
            } else if (tuple.f0 != null) {
                return false;
            }

            label301: {
                if (this.f1 != null) {
                    if (this.f1.equals(tuple.f1)) {
                        break label301;
                    }
                } else if (tuple.f1 == null) {
                    break label301;
                }

                return false;
            }

            label294: {
                if (this.f2 != null) {
                    if (this.f2.equals(tuple.f2)) {
                        break label294;
                    }
                } else if (tuple.f2 == null) {
                    break label294;
                }

                return false;
            }

            if (this.f3 != null) {
                if (!this.f3.equals(tuple.f3)) {
                    return false;
                }
            } else if (tuple.f3 != null) {
                return false;
            }

            label280: {
                if (this.f4 != null) {
                    if (this.f4.equals(tuple.f4)) {
                        break label280;
                    }
                } else if (tuple.f4 == null) {
                    break label280;
                }

                return false;
            }

            if (this.f5 != null) {
                if (!this.f5.equals(tuple.f5)) {
                    return false;
                }
            } else if (tuple.f5 != null) {
                return false;
            }

            label266: {
                if (this.f6 != null) {
                    if (this.f6.equals(tuple.f6)) {
                        break label266;
                    }
                } else if (tuple.f6 == null) {
                    break label266;
                }

                return false;
            }

            if (this.f7 != null) {
                if (!this.f7.equals(tuple.f7)) {
                    return false;
                }
            } else if (tuple.f7 != null) {
                return false;
            }

            if (this.f8 != null) {
                if (!this.f8.equals(tuple.f8)) {
                    return false;
                }
            } else if (tuple.f8 != null) {
                return false;
            }

            if (this.f9 != null) {
                if (!this.f9.equals(tuple.f9)) {
                    return false;
                }
            } else if (tuple.f9 != null) {
                return false;
            }

            label238: {
                if (this.f10 != null) {
                    if (this.f10.equals(tuple.f10)) {
                        break label238;
                    }
                } else if (tuple.f10 == null) {
                    break label238;
                }

                return false;
            }

            if (this.f11 != null) {
                if (!this.f11.equals(tuple.f11)) {
                    return false;
                }
            } else if (tuple.f11 != null) {
                return false;
            }

            if (this.f12 != null) {
                if (!this.f12.equals(tuple.f12)) {
                    return false;
                }
            } else if (tuple.f12 != null) {
                return false;
            }

            label217: {
                if (this.f13 != null) {
                    if (this.f13.equals(tuple.f13)) {
                        break label217;
                    }
                } else if (tuple.f13 == null) {
                    break label217;
                }

                return false;
            }

            label210: {
                if (this.f14 != null) {
                    if (this.f14.equals(tuple.f14)) {
                        break label210;
                    }
                } else if (tuple.f14 == null) {
                    break label210;
                }

                return false;
            }

            if (this.f15 != null) {
                if (!this.f15.equals(tuple.f15)) {
                    return false;
                }
            } else if (tuple.f15 != null) {
                return false;
            }

            if (this.f16 != null) {
                if (!this.f16.equals(tuple.f16)) {
                    return false;
                }
            } else if (tuple.f16 != null) {
                return false;
            }

            label189: {
                if (this.f17 != null) {
                    if (this.f17.equals(tuple.f17)) {
                        break label189;
                    }
                } else if (tuple.f17 == null) {
                    break label189;
                }

                return false;
            }

            label182: {
                if (this.f18 != null) {
                    if (this.f18.equals(tuple.f18)) {
                        break label182;
                    }
                } else if (tuple.f18 == null) {
                    break label182;
                }

                return false;
            }

            if (this.f19 != null) {
                if (!this.f19.equals(tuple.f19)) {
                    return false;
                }
            } else if (tuple.f19 != null) {
                return false;
            }

            label168: {
                if (this.f20 != null) {
                    if (this.f20.equals(tuple.f20)) {
                        break label168;
                    }
                } else if (tuple.f20 == null) {
                    break label168;
                }

                return false;
            }

            if (this.f21 != null) {
                if (!this.f21.equals(tuple.f21)) {
                    return false;
                }
            } else if (tuple.f21 != null) {
                return false;
            }

            label154: {
                if (this.f22 != null) {
                    if (this.f22.equals(tuple.f22)) {
                        break label154;
                    }
                } else if (tuple.f22 == null) {
                    break label154;
                }

                return false;
            }

            if (this.f23 != null) {
                if (!this.f23.equals(tuple.f23)) {
                    return false;
                }
            } else if (tuple.f23 != null) {
                return false;
            }

            if (this.f24 != null) {
                if (!this.f24.equals(tuple.f24)) {
                    return false;
                }
            } else if (tuple.f24 != null) {
                return false;
            }

            return true;
        }
    }
    @Override
    public int hashCode() {
        int result = this.f0 != null ? this.f0.hashCode() : 0;
        result = 31 * result + (this.f1 != null ? this.f1.hashCode() : 0);
        result = 31 * result + (this.f2 != null ? this.f2.hashCode() : 0);
        result = 31 * result + (this.f3 != null ? this.f3.hashCode() : 0);
        result = 31 * result + (this.f4 != null ? this.f4.hashCode() : 0);
        result = 31 * result + (this.f5 != null ? this.f5.hashCode() : 0);
        result = 31 * result + (this.f6 != null ? this.f6.hashCode() : 0);
        result = 31 * result + (this.f7 != null ? this.f7.hashCode() : 0);
        result = 31 * result + (this.f8 != null ? this.f8.hashCode() : 0);
        result = 31 * result + (this.f9 != null ? this.f9.hashCode() : 0);
        result = 31 * result + (this.f10 != null ? this.f10.hashCode() : 0);
        result = 31 * result + (this.f11 != null ? this.f11.hashCode() : 0);
        result = 31 * result + (this.f12 != null ? this.f12.hashCode() : 0);
        result = 31 * result + (this.f13 != null ? this.f13.hashCode() : 0);
        result = 31 * result + (this.f14 != null ? this.f14.hashCode() : 0);
        result = 31 * result + (this.f15 != null ? this.f15.hashCode() : 0);
        result = 31 * result + (this.f16 != null ? this.f16.hashCode() : 0);
        result = 31 * result + (this.f17 != null ? this.f17.hashCode() : 0);
        result = 31 * result + (this.f18 != null ? this.f18.hashCode() : 0);
        result = 31 * result + (this.f19 != null ? this.f19.hashCode() : 0);
        result = 31 * result + (this.f20 != null ? this.f20.hashCode() : 0);
        result = 31 * result + (this.f21 != null ? this.f21.hashCode() : 0);
        result = 31 * result + (this.f22 != null ? this.f22.hashCode() : 0);
        result = 31 * result + (this.f23 != null ? this.f23.hashCode() : 0);
        result = 31 * result + (this.f24 != null ? this.f24.hashCode() : 0);
        return result;
    }
    @Override
    public Tuple25<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24> copy() {
        return new Tuple25(this.f0, this.f1, this.f2, this.f3, this.f4, this.f5, this.f6, this.f7, this.f8, this.f9, this.f10, this.f11, this.f12, this.f13, this.f14, this.f15, this.f16, this.f17, this.f18, this.f19, this.f20, this.f21, this.f22, this.f23, this.f24);
    }

    public static <T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24> Tuple25<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24> of(T0 value0, T1 value1, T2 value2, T3 value3, T4 value4, T5 value5, T6 value6, T7 value7, T8 value8, T9 value9, T10 value10, T11 value11, T12 value12, T13 value13, T14 value14, T15 value15, T16 value16, T17 value17, T18 value18, T19 value19, T20 value20, T21 value21, T22 value22, T23 value23, T24 value24) {
        return new Tuple25(value0, value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11, value12, value13, value14, value15, value16, value17, value18, value19, value20, value21, value22, value23, value24);
    }
}