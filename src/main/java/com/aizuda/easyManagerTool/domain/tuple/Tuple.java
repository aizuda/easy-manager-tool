package com.aizuda.easyManagerTool.domain.tuple;

import java.io.Serializable;

public abstract class Tuple implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int MAX_ARITY = 25;
    private static final Class<?>[] CLASSES = new Class[]{Tuple0.class, Tuple1.class, Tuple2.class, Tuple3.class, Tuple4.class, Tuple5.class};

    public Tuple() {
    }

    public abstract <T> T getField(int var1);

    public <T> T getFieldNotNull(int pos) {
        T field = this.getField(pos);
        if (field != null) {
            return field;
        } else {
            throw new NullFieldException(pos);
        }
    }

    public abstract <T> void setField(T var1, int var2);

    public abstract int getArity();

    public abstract <T extends Tuple> T copy();

    public static Class<? extends Tuple> getTupleClass(int arity) {
        if (arity >= 0 && arity <= 25) {
            return (Class<? extends Tuple>) CLASSES[arity];
        } else {
            throw new IllegalArgumentException("The tuple arity must be in [0, 5].");
        }
    }

    public static Tuple newInstance(int arity) {
        switch (arity) {
            case 0:
                return Tuple0.INSTANCE;
            case 1:
                return new Tuple1();
            case 2:
                return new Tuple2();
            case 3:
                return new Tuple3();
            case 4:
                return new Tuple4();
            case 5:
                return new Tuple5();
            default:
                throw new IllegalArgumentException("The tuple arity must be in [0, 5].");
        }
    }
}
