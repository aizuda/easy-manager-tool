package com.aizuda.easyManagerTool.domain.tuple;

import lombok.Setter;

@Setter
public class Tuple4<T0, T1, T2, T3> extends Tuple {
    private static final long serialVersionUID = 1L;
    public T0 f0;
    public T1 f1;
    public T2 f2;
    public T3 f3;

    public Tuple4() {
    }

    public Tuple4(T0 value0, T1 value1, T2 value2, T3 value3) {
        this.f0 = value0;
        this.f1 = value1;
        this.f2 = value2;
        this.f3 = value3;
    }
    @Override
    public int getArity() {
        return 4;
    }
    @Override
    public <T> T getField(int pos) {
        switch (pos) {
            case 0:
                return (T) this.f0;
            case 1:
                return (T) this.f1;
            case 2:
                return (T) this.f2;
            case 3:
                return (T) this.f3;
            default:
                throw new IndexOutOfBoundsException(String.valueOf(pos));
        }
    }
    @Override
    public <T> void setField(T value, int pos) {
        switch (pos) {
            case 0:
                this.f0 = (T0) value;
                break;
            case 1:
                this.f1 = (T1) value;
                break;
            case 2:
                this.f2 = (T2) value;
                break;
            case 3:
                this.f3 = (T3) value;
                break;
            default:
                throw new IndexOutOfBoundsException(String.valueOf(pos));
        }

    }

    public void setFields(T0 value0, T1 value1, T2 value2, T3 value3) {
        this.f0 = value0;
        this.f1 = value1;
        this.f2 = value2;
        this.f3 = value3;
    }
    @Override
    public String toString() {
        return "(" + StringUtils.arrayAwareToString(this.f0) + "," + StringUtils.arrayAwareToString(this.f1) + "," + StringUtils.arrayAwareToString(this.f2) + "," + StringUtils.arrayAwareToString(this.f3) + ")";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof Tuple4)) {
            return false;
        } else {
            Tuple4 tuple;
            label56: {
                tuple = (Tuple4)o;
                if (this.f0 != null) {
                    if (this.f0.equals(tuple.f0)) {
                        break label56;
                    }
                } else if (tuple.f0 == null) {
                    break label56;
                }

                return false;
            }

            label49: {
                if (this.f1 != null) {
                    if (this.f1.equals(tuple.f1)) {
                        break label49;
                    }
                } else if (tuple.f1 == null) {
                    break label49;
                }

                return false;
            }

            if (this.f2 != null) {
                if (!this.f2.equals(tuple.f2)) {
                    return false;
                }
            } else if (tuple.f2 != null) {
                return false;
            }

            if (this.f3 != null) {
                if (!this.f3.equals(tuple.f3)) {
                    return false;
                }
            } else if (tuple.f3 != null) {
                return false;
            }

            return true;
        }
    }
    @Override
    public int hashCode() {
        int result = this.f0 != null ? this.f0.hashCode() : 0;
        result = 31 * result + (this.f1 != null ? this.f1.hashCode() : 0);
        result = 31 * result + (this.f2 != null ? this.f2.hashCode() : 0);
        result = 31 * result + (this.f3 != null ? this.f3.hashCode() : 0);
        return result;
    }
    @Override
    public Tuple4<T0, T1, T2, T3> copy() {
        return new Tuple4(this.f0, this.f1, this.f2, this.f3);
    }

    public static <T0, T1, T2, T3> Tuple4<T0, T1, T2, T3> of(T0 value0, T1 value1, T2 value2, T3 value3) {
        return new Tuple4(value0, value1, value2, value3);
    }
}
