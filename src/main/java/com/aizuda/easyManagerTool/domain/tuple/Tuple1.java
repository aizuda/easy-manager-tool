package com.aizuda.easyManagerTool.domain.tuple;

import lombok.Setter;

@Setter
public class Tuple1<T0> extends Tuple {
    private static final long serialVersionUID = 1L;
    public T0 f0;
    public Tuple1() {
    }

    public Tuple1(T0 value0) {
        this.f0 = value0;
    }
    @Override
    public int getArity() {
        return 1;
    }
    @Override
    public <T> T getField(int pos) {
        switch (pos) {
            case 0:
                return (T) this.f0;
            default:
                throw new IndexOutOfBoundsException(String.valueOf(pos));
        }
    }
    @Override
    public <T> void setField(T value, int pos) {
        switch (pos) {
            case 0:
                this.f0 = (T0) value;
                return;
            default:
                throw new IndexOutOfBoundsException(String.valueOf(pos));
        }
    }

    public void setFields(T0 value0) {
        this.f0 = value0;
    }
    @Override
    public String toString() {
        return "(" + StringUtils.arrayAwareToString(this.f0) + ")";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof Tuple1)) {
            return false;
        } else {
            Tuple1 tuple = (Tuple1)o;
            if (this.f0 != null) {
                if (!this.f0.equals(tuple.f0)) {
                    return false;
                }
            } else if (tuple.f0 != null) {
                return false;
            }

            return true;
        }
    }
    @Override
    public int hashCode() {
        int result = this.f0 != null ? this.f0.hashCode() : 0;
        return result;
    }
    @Override
    public Tuple1<T0> copy() {
        return new Tuple1(this.f0);
    }

    public static <T0> Tuple1<T0> of(T0 value0) {
        return new Tuple1(value0);
    }
}
