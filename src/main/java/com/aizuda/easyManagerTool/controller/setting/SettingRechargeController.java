package com.aizuda.easyManagerTool.controller.setting;


import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.bo.PackagesBO;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingRechargeRecordEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.PackageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.setting.SettingRechargeService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/recharge")
public class SettingRechargeController {

    @Resource
    private SettingRechargeService settingRechargeService;


    @PostMapping("/packages")
    public Rep<PackageVO> packages(@RequestBody Req<Object,SettingUserVO> req) {
        return settingRechargeService.packages(req);
    }

    @PostMapping("/record")
    public Rep<PageVO<SettingRechargeRecordEntity>> record(@RequestBody Req<PageDTO<SettingRechargeRecordEntity>, SettingUserVO> req) {
        return settingRechargeService.record(req);
    }

    @PostMapping("/pay")
    public Rep<String> pay(@RequestBody Req<PackagesBO, SettingUserVO> req) {
        return settingRechargeService.pay(req);
    }


    @PostMapping("/refreshResults")
    public Rep<SettingRechargeRecordEntity> refreshResults(@RequestBody Req<SettingRechargeRecordEntity, SettingUserVO> req) throws Exception {
        return settingRechargeService.refreshResults(req);
    }

    @PostMapping("/delRecode")
    public Rep<SettingRechargeRecordEntity> delRecode(@RequestBody Req<SettingRechargeRecordEntity, SettingUserVO> req) throws Exception {
        return settingRechargeService.delRecode(req);
    }


}
