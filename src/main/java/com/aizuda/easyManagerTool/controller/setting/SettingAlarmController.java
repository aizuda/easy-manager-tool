package com.aizuda.easyManagerTool.controller.setting;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.setting.SettingAlarmTestDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingAlarmEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.setting.SettingAlarmService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
@RequestMapping("/settingAlarm")
public class SettingAlarmController {

    @Resource
    SettingAlarmService settingAlarmService;


    @PostMapping("/find")
    public Rep<SettingAlarmEntity> find(@RequestBody Req<SettingAlarmEntity, SettingUserVO> req) {
        return settingAlarmService.find(req);
    }

//    @PackageAnnotation(index = 2)
    @PostMapping("/edit")
    public Rep<SettingAlarmEntity> edit(@RequestBody Req<SettingAlarmEntity, SettingUserVO> req) {
        return settingAlarmService.edit(req);
    }

//    @PackageAnnotation(index = 2)
    @PostMapping("/test")
    public Rep<SettingAlarmEntity> test(@RequestBody Req<SettingAlarmTestDTO, SettingUserVO> req) {
        return settingAlarmService.test(req);
    }

}
