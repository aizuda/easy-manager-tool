package com.aizuda.easyManagerTool.controller.setting;


import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingTaskEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingTaskFindVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingTaskVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.setting.SettingTaskService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/task")
public class SettingTaskController {

    @Resource
    SettingTaskService settingTaskService;


    @PostMapping("/find")
    public Rep<SettingTaskVO> find(@RequestBody Req<SettingTaskFindVO, SettingUserVO> req) {
        return settingTaskService.find(req);
    }

    @PostMapping("/edit")
    public Rep<SettingTaskEntity> edit(@RequestBody Req<SettingTaskEntity, SettingUserVO> req) {
        return settingTaskService.edit(req);
    }

    @PostMapping("/del")
    public Rep<SettingTaskEntity> del(@RequestBody Req<SettingTaskEntity, SettingUserVO> req) {
        return settingTaskService.del(req);
    }


}
