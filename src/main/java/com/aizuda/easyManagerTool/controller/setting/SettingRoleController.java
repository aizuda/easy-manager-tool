package com.aizuda.easyManagerTool.controller.setting;


import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingRoleEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingMenuVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.setting.SettingRoleService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/role")
public class SettingRoleController {

    @Resource
    private SettingRoleService settingRoleService;

    @PostMapping("/find")
    public Rep<PageVO<SettingRoleEntity>> find(@RequestBody Req<PageDTO<SettingRoleEntity>, SettingUserVO> req) {
        return settingRoleService.find(req);
    }

    @PostMapping("/edit")
    public Rep<SettingRoleEntity> edit(@RequestBody Req<SettingRoleEntity, SettingUserVO> req) {
        return settingRoleService.edit(req);
    }

    @PostMapping("/del")
    public Rep<SettingRoleEntity> del(@RequestBody Req<SettingRoleEntity, SettingUserVO> req) {
        return settingRoleService.del(req);
    }

    @PostMapping("/menu")
    public Rep<SettingMenuVO> menu(@RequestBody Req<SettingRoleEntity, SettingUserVO> req) {
        return settingRoleService.menu(req);
    }

    @PostMapping("/findAll")
    public Rep<List<SettingRoleEntity>> findAll(@RequestBody Req<SettingRoleEntity, SettingUserVO> req) {
        return settingRoleService.findAll(req);
    }

}
