package com.aizuda.easyManagerTool.controller.setting;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.setting.SettingUserLoginDTO;
import com.aizuda.easyManagerTool.domain.dto.setting.SettingUserRegisterDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingUserEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.setting.SettingUserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/setting")
public class SettingUserController {

    @Resource
    SettingUserService settingUserService;

    @PostMapping("/login")
    public Rep<SettingUserVO> login(@RequestBody Req<SettingUserLoginDTO, SettingUserVO> req) {
        return settingUserService.login(req);
    }

    @PostMapping("/code")
    public Map<String,Object> code() {
        return settingUserService.code();
    }

    @PostMapping("/register")
    public Rep<SettingUserVO> register(@RequestBody Req<SettingUserRegisterDTO, SettingUserVO> req) {
        return settingUserService.register(req);
    }

    @PostMapping("/find")
    public Rep<PageVO<SettingUserEntity>> find(@RequestBody Req<PageDTO<SettingUserEntity>, SettingUserVO> req) {
        return settingUserService.find(req);
    }

//    @PackageAnnotation(index = 3)
    @PostMapping("/edit")
    public Rep<SettingUserEntity> edit(@RequestBody Req<SettingUserRegisterDTO, SettingUserVO> req) {
        return settingUserService.edit(req);
    }

    @PostMapping("/editUser")
    public Rep<SettingUserEntity> editUser(@RequestBody Req<SettingUserEntity, SettingUserVO> req) {
        return settingUserService.editUser(req);
    }

    @PostMapping("/findUser")
    public Rep<SettingUserEntity> findUser(@RequestBody Req<SettingUserRegisterDTO, SettingUserVO> req) {
        return settingUserService.findUser(req);
    }

    @PostMapping("/del")
    public Rep<SettingUserEntity> del(@RequestBody Req<SettingUserEntity, SettingUserVO> req) {
        return settingUserService.del(req);
    }

    @PostMapping("/findAll")
    public Rep<List<SettingUserEntity>> findAll(@RequestBody Req<SettingUserEntity, SettingUserVO> req) {
        return settingUserService.findAll(req);
    }


}
