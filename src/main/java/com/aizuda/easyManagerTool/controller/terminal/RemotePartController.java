package com.aizuda.easyManagerTool.controller.terminal;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easy.security.exp.impl.BasicException;
import com.aizuda.easyManagerTool.config.aspect.annotation.PackageAnnotation;
import com.aizuda.easyManagerTool.domain.dto.terminal.RemotePartDTO;
import com.aizuda.easyManagerTool.domain.vo.server.ServerCompleteVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.terminal.RemotePartService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/remotePart")
public class RemotePartController {

    @Resource
    private RemotePartService remotePartService;

    @PackageAnnotation(index = 1)
    @PostMapping("/index")
    public Rep<List<ServerCompleteVO>> index(@RequestBody Req<RemotePartDTO, SettingUserVO> requestData) throws BasicException {
        return remotePartService.index(requestData);
    }

    @PostMapping("/upload")
    public Rep<String> upload(
            @RequestParam("token") String token,
            @RequestParam("path") String path,
            @RequestParam("file") MultipartFile file) {
        return remotePartService.upload(token,path,file);
    }

}
