package com.aizuda.easyManagerTool.controller.terminal;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.terminal.SFTPFileDTO;
import com.aizuda.easyManagerTool.domain.dto.terminal.SSHMessageDTO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.domain.vo.terminal.SFTPFileVO;
import com.aizuda.easyManagerTool.service.terminal.ExecuteService;
import com.aizuda.easyManagerTool.service.terminal.MonitorTerminalService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/terminal")
public class TerminalController {

    @Resource
    private ExecuteService executeService;
    @Resource
    private MonitorTerminalService monitorTerminalService;

    @PostMapping("/del")
    public Rep<List<SFTPFileVO>> del(@RequestBody Req<SFTPFileDTO, SettingUserVO> req) {
        return executeService.del(req);
    }

    @PostMapping("/upload")
    public Rep<String> upload(
            @RequestParam("path") String path,
            @RequestParam("token") String token,
            @RequestParam("file") MultipartFile file) {
        return executeService.upload(path,token,file);
    }

    @PostMapping("/download")
    public Rep<String> download(@RequestBody Req<SFTPFileDTO, SettingUserVO> requestData, HttpServletRequest request){
        return executeService.download(requestData,request);
    }

    @PostMapping("/monitor")
    public Rep<String> monitor(@RequestBody Req<SSHMessageDTO, SettingUserVO> requestData){
        return monitorTerminalService.monitor(requestData);
    }

}
