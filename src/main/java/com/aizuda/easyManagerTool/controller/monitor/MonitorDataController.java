package com.aizuda.easyManagerTool.controller.monitor;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.monitor.MonitorEditDTO;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorDataEntity;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorDataIndexEntity;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorDataIndexItemEntity;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorUrlEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.domain.vo.terminal.TreeIndexItemVO;
import com.aizuda.easyManagerTool.service.monitor.MonitorDataIndexService;
import com.aizuda.easyManagerTool.service.monitor.MonitorDataService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("monitor")
public class MonitorDataController {

    @Resource
    MonitorDataService monitorDataService;
    @Resource
    MonitorDataIndexService monitorDataIndexService;

    @PostMapping("/find")
    public Rep<PageVO<MonitorDataEntity>> find(@RequestBody Req<PageDTO<MonitorDataEntity>, SettingUserVO> req) {
        return monitorDataService.find(req);
    }

    @PostMapping("/edit")
    public Rep<MonitorDataEntity> edit(@RequestBody Req<MonitorEditDTO, SettingUserVO> req) {
        return monitorDataService.edit(req);
    }

    @PostMapping("/urls")
    public Rep<List<MonitorUrlEntity>> urls(@RequestBody Req<MonitorEditDTO, SettingUserVO> req) {
        return monitorDataService.urls(req);
    }

    @PostMapping("/changeSwitch")
    public Rep<List<MonitorUrlEntity>> changeSwitch(@RequestBody Req<MonitorEditDTO, SettingUserVO> req) {
        return monitorDataService.changeSwitch(req);
    }

    @PostMapping("/delUrls")
    public Rep<List<MonitorUrlEntity>> delUrls(@RequestBody Req<MonitorUrlEntity, SettingUserVO> req) {
        return monitorDataService.delUrls(req);
    }

    @PostMapping("/clone")
    public Rep<MonitorDataEntity> clone(@RequestBody Req<MonitorEditDTO, SettingUserVO> req) {
        return monitorDataService.clone(req);
    }

    @PostMapping("/del")
    public Rep<MonitorDataEntity> del(@RequestBody Req<MonitorDataEntity, SettingUserVO> req) {
        return monitorDataService.del(req);
    }

    @PostMapping("/getUrl")
    public Rep<Map<String,Object>> getUrl(@RequestBody Req<MonitorUrlEntity, SettingUserVO> req) {
        return monitorDataService.getUrl(req);
    }

    @PostMapping("/getAllUrl")
    public Rep<Map<String,Object>> getAllUrl(@RequestBody Req<MonitorDataEntity, SettingUserVO> req) {
        return monitorDataService.getAllUrl(req);
    }

    @PostMapping("/getAllUrls")
    public Rep<Map<String,Object>> getAllUrls(@RequestBody Req<MonitorEditDTO, SettingUserVO> req) {
        return monitorDataService.getAllUrls(req);
    }

    @PostMapping("/findIndex")
    public Rep<PageVO<MonitorDataIndexEntity>> findIndex(@RequestBody Req<PageDTO<MonitorDataIndexEntity>, SettingUserVO> req) {
        return monitorDataIndexService.find(req);
    }

    @PostMapping("/editIndex")
    public Rep<MonitorDataIndexEntity> editIndex(@RequestBody Req<MonitorDataIndexEntity, SettingUserVO> req) {
        return monitorDataIndexService.edit(req);
    }

    @PostMapping("/delIndex")
    public Rep<MonitorDataIndexEntity> delIndex(@RequestBody Req<MonitorDataIndexEntity, SettingUserVO> req) {
        return monitorDataIndexService.del(req);
    }

    @PostMapping("/delIndexItem")
    public Rep<MonitorDataIndexItemEntity> delIndexItem(@RequestBody Req<MonitorDataIndexItemEntity, SettingUserVO> req){
        return monitorDataIndexService.delIndexItem(req);
    }

    @PostMapping("/findIndexItem")
    public Rep<List<MonitorDataIndexItemEntity>> findIndexItem(@RequestBody Req<MonitorDataIndexItemEntity, SettingUserVO> req){
        return monitorDataIndexService.findIndexItem(req);
    }

    @PostMapping("/editIndexItem")
    public Rep<MonitorDataIndexItemEntity> editIndexItem(@RequestBody Req<MonitorDataIndexItemEntity, SettingUserVO> req){
        return monitorDataIndexService.editIndexItem(req);
    }

    @PostMapping("/treeIndexItem")
    public Rep<List<TreeIndexItemVO>> treeIndexItem(@RequestBody Req<MonitorDataEntity, SettingUserVO> req){
        return monitorDataIndexService.treeIndexItem(req);
    }

    @PostMapping("/editIndexAlarm")
    public Rep<MonitorDataIndexEntity> editIndexAlarm(@RequestBody Req<MonitorDataIndexEntity, SettingUserVO> req){
        return monitorDataIndexService.editIndexAlarm(req);
    }

}
