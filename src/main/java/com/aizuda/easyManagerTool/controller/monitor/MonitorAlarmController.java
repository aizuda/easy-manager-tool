package com.aizuda.easyManagerTool.controller.monitor;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorAlarmEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.monitor.MonitorAlarmService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("alarm")
public class MonitorAlarmController {

    @Resource
    MonitorAlarmService monitorAlarmService;

    @PostMapping("/find")
    public Rep<PageVO<MonitorAlarmEntity>> find(@RequestBody Req<PageDTO<MonitorAlarmEntity>, SettingUserVO> req) {
        return monitorAlarmService.find(req);
    }

    @PostMapping("/edit")
    public Rep<MonitorAlarmEntity> edit(@RequestBody Req<MonitorAlarmEntity, SettingUserVO> req) {
        return monitorAlarmService.edit(req);
    }

    @PostMapping("/del")
    public Rep<MonitorAlarmEntity> del(@RequestBody Req<MonitorAlarmEntity, SettingUserVO> req) {
        return monitorAlarmService.del(req);
    }

}
