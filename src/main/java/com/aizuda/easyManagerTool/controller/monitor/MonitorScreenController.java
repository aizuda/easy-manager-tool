package com.aizuda.easyManagerTool.controller.monitor;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorScreenEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.monitor.MonitorScreenService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/screen")
public class MonitorScreenController{

    @Resource
    MonitorScreenService monitorScreenService;

    @PostMapping("/find")
    public Rep<PageVO<MonitorScreenEntity>> find(@RequestBody Req<PageDTO<MonitorScreenEntity>, SettingUserVO> req) {
        return monitorScreenService.find(req);
    }

    @PostMapping("/edit")
    public Rep<MonitorScreenEntity> edit(@RequestBody Req<MonitorScreenEntity, SettingUserVO> req) {
        return monitorScreenService.edit(req);
    }

    @PostMapping("/del")
    public Rep<MonitorScreenEntity> del(@RequestBody Req<MonitorScreenEntity, SettingUserVO> req) {
        return monitorScreenService.del(req);
    }

    @PostMapping("/findById")
    public Rep<MonitorScreenEntity> findById(@RequestBody Req<MonitorScreenEntity, SettingUserVO> req) {
        return monitorScreenService.findById(req);
    }



}
