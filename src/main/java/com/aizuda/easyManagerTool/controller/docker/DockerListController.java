package com.aizuda.easyManagerTool.controller.docker;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.docker.*;
import com.aizuda.easyManagerTool.domain.entity.docker.DockerListEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerListVO;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerNetworkListVO;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerVolumeListDTO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.docker.DockerListServer;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("dockerList")
public class DockerListController {
    
    @Resource
    private DockerListServer dockerListServer;
    
    @PostMapping("/find")
    public Rep<PageVO<DockerListVO>> find(@RequestBody Req<PageDTO<DockerListEntity>, SettingUserVO> req) {
        return dockerListServer.find(req);
    }

    @PostMapping("/edit")
    public Rep<DockerListVO> edit(@RequestBody Req<DockerListEntity, SettingUserVO> req) {
        return dockerListServer.edit(req);
    }

    @PostMapping("/del")
    public Rep<DockerListVO> del(@RequestBody Req<DockerListEntity, SettingUserVO> req) {
        return dockerListServer.del(req);
    }

    @PostMapping("/imageList")
    public Rep<List<Image>> imageList(@RequestBody Req<DockerListImageDTO, SettingUserVO> req) {
        return dockerListServer.imageList(req);
    }

    @PostMapping("/imageDel")
    public Rep<List<Image>> imageDel(@RequestBody Req<DockerListImageDTO, SettingUserVO> req) {
        return dockerListServer.imageDel(req);
    }

    @PostMapping("/imageTagDel")
    public Rep<List<Image>> imageTagDel(@RequestBody Req<DockerListImageDTO, SettingUserVO> req) {
        return dockerListServer.imageTagDel(req);
    }

    @PostMapping("/imagePush")
    public Rep<List<Image>> imagePush(@RequestBody Req<DockerListImageDTO, SettingUserVO> req) {
        return dockerListServer.imagePush(req);
    }

    @PostMapping("/imagePull")
    public Rep<List<Image>> imagePull(@RequestBody Req<DockerListImageDTO, SettingUserVO> req) {
        return dockerListServer.imagePull(req);
    }

    @PostMapping("/imageInfo")
    public Rep<String> imageInfo(@RequestBody Req<DockerListImageDTO, SettingUserVO> req) {
        return dockerListServer.imageInfo(req);
    }

    @PostMapping("/containerList")
    public Rep<List<Container>> containerList(@RequestBody Req<DockerListContainerDTO, SettingUserVO> req) {
        return dockerListServer.containerList(req);
    }

    @PostMapping("/containerRun")
    public Rep<List<Container>> containerRun(@RequestBody Req<DockerListContainerDTO, SettingUserVO> req) {
        return dockerListServer.containerRun(req);
    }

    @PostMapping("/containerStop")
    public Rep<List<Container>> containerStop(@RequestBody Req<DockerListContainerDTO, SettingUserVO> req) {
        return dockerListServer.containerStop(req);
    }

    @PostMapping("/containerDel")
    public Rep<List<Container>> containerDel(@RequestBody Req<DockerListContainerDTO, SettingUserVO> req) {
        return dockerListServer.containerDel(req);
    }

    @PostMapping("/containerLog")
    public Rep<List<Container>> containerLog(@RequestBody Req<DockerListContainerDTO, SettingUserVO> req) {
        return dockerListServer.containerLog(req);
    }

    @PostMapping("/containerAdd")
    public Rep<List<Container>> containerAdd(@RequestBody Req<DockerContainerAddDTO, SettingUserVO> req) {
        return dockerListServer.containerAdd(req);
    }

    @PostMapping("/containerInfo")
    public Rep<String> containerInfo(@RequestBody Req<DockerListContainerDTO, SettingUserVO> req) {
        return dockerListServer.containerInfo(req);
    }

    @PostMapping("/containerTerminal")
    public Rep<List<Container>> containerTerminal(@RequestBody Req<DockerListContainerDTO, SettingUserVO> req) {
        return dockerListServer.containerTerminal(req);
    }

    @PostMapping("/containerSynchronizationData")
    public Rep<List<Container>> containerSynchronizationData(@RequestBody Req<DockerListContainerDTO, SettingUserVO> req) {
        return dockerListServer.containerSynchronizationData(req);
    }


    @PostMapping("/volumeList")
    public Rep<List<DockerVolumeListDTO>> volumeList(@RequestBody Req<DockervolumeDTO, SettingUserVO> req) {
        return dockerListServer.volumeList(req);
    }

    @PostMapping("/volumeAdd")
    public Rep<List<DockerVolumeListDTO>> volumeAdd(@RequestBody Req<DockervolumeDTO, SettingUserVO> req) {
        return dockerListServer.volumeAdd(req);
    }

    @PostMapping("/volumeDel")
    public Rep<List<DockerVolumeListDTO>> volumeDel(@RequestBody Req<DockervolumeDTO, SettingUserVO> req) {
        return dockerListServer.volumeDel(req);
    }

    @PostMapping("/networkList")
    public Rep<List<DockerNetworkListVO>> networkList(@RequestBody Req<DockerNetworkDTO, SettingUserVO> req) {
        return dockerListServer.networkList(req);
    }

    @PostMapping("/networkAdd")
    public Rep<List<DockerNetworkListVO>> networkAdd(@RequestBody Req<DockerNetworkDTO, SettingUserVO> req) {
        return dockerListServer.networkAdd(req);
    }

    @PostMapping("/networkDel")
    public Rep<List<DockerNetworkListVO>> networkDel(@RequestBody Req<DockerNetworkDTO, SettingUserVO> req) {
        return dockerListServer.networkDel(req);
    }


}
