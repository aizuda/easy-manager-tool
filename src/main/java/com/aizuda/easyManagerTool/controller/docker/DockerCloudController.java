package com.aizuda.easyManagerTool.controller.docker;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.docker.DockerPullImagesDTO;
import com.aizuda.easyManagerTool.domain.dto.docker.DockerTagDelDTO;
import com.aizuda.easyManagerTool.domain.entity.docker.DockerCloudEntity;
import com.aizuda.easyManagerTool.domain.entity.docker.DockerConfigEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerCloudFindVO;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerCloudTagListVO;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerConfigFindVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.docker.DockerCloudService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("dockerCloud")
public class DockerCloudController {
    
    @Resource
    private DockerCloudService dockerCloudService;
    
    @PostMapping("/find")
    public Rep<PageVO<DockerCloudFindVO>> find(@RequestBody Req<PageDTO<DockerCloudEntity>, SettingUserVO> req) {
        return dockerCloudService.find(req);
    }

    @PostMapping("/findConfig")
    public Rep<List<DockerConfigFindVO>> findConfig(@RequestBody Req<DockerConfigEntity, SettingUserVO> req) {
        return dockerCloudService.findConfig(req);
    }

    @PostMapping("/edit")
    public Rep<DockerCloudEntity> edit(@RequestBody Req<DockerCloudEntity, SettingUserVO> req) {
        return dockerCloudService.edit(req);
    }

    @PostMapping("/del")
    public Rep<DockerCloudEntity> del(@RequestBody Req<DockerCloudEntity, SettingUserVO> req) {
        return dockerCloudService.del(req);
    }

    @PostMapping("/editConfig")
    public Rep<DockerConfigEntity> editConfig(@RequestBody Req<DockerConfigEntity, SettingUserVO> req) {
        return dockerCloudService.editConfig(req);
    }

    @PostMapping("/tagList")
    public Rep<List<DockerCloudTagListVO>> tagList(@RequestBody Req<DockerCloudEntity, SettingUserVO> req) {
        return dockerCloudService.tagList(req);
    }

    @PostMapping("/tagDel")
    public Rep<List<DockerCloudTagListVO>> tagDel(@RequestBody Req<DockerTagDelDTO, SettingUserVO> req) {
        return dockerCloudService.tagDel(req);
    }

    @PostMapping("/pullImages")
    public Rep<List<DockerCloudEntity>> pullImages(@RequestBody Req<DockerPullImagesDTO, SettingUserVO> req) {
        return dockerCloudService.pullImages(req);
    }

}
