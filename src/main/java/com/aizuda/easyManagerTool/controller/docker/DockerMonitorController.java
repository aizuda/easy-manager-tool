package com.aizuda.easyManagerTool.controller.docker;


import com.aizuda.easyManagerTool.service.docker.DockerMonitorService;
import com.github.dockerjava.api.model.Statistics;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("docker")
public class DockerMonitorController {

    @Resource
    DockerMonitorService dockerMonitorService;

    @GetMapping("/monitor/{id}/{containerId}")
    public String monitor(@PathVariable("id") Integer id, @PathVariable("containerId") String containerId) {
        return dockerMonitorService.monitor(id,containerId);
    }


}
