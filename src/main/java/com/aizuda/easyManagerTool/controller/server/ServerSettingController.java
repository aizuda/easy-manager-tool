package com.aizuda.easyManagerTool.controller.server;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.server.ServerSettingDTO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.server.impl.ServerSettingStrategy;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/serverSetting")
public class ServerSettingController {

    @Resource
    ServerSettingStrategy serverSettingStrategy;

    @PostMapping("/find")
    public Rep<List> find(@RequestBody Req<String, SettingUserVO> req){
        List list = serverSettingStrategy.get(req.getData());
        return Rep.ok(list);
    }

    @PostMapping("/findAll")
    public Rep<Map<String,List>> findAll(@RequestBody Req<String, SettingUserVO> req){
        return Rep.ok(serverSettingStrategy.getAll(req.getData(),req.getUser()));
    }

    @PostMapping("/edit")
    public Rep edit(@RequestBody Req<ServerSettingDTO, SettingUserVO> req){
        return serverSettingStrategy.edit(req);
    }

    @PostMapping("/del")
    public Rep del(@RequestBody Req<ServerSettingDTO, SettingUserVO> req){
        return serverSettingStrategy.del(req);
    }

}
