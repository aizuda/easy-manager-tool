package com.aizuda.easyManagerTool.controller.server;


import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.server.ServerScriptFindDTO;
import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptCommandEntity;
import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptLogEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.server.ServerScriptFindVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.server.ServerScriptService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/script")
public class ServerScriptController{

    @Resource
    private ServerScriptService serverScriptService;

    @PostMapping("/find")
    public Rep<PageVO<ServerScriptFindVO>> find(@RequestBody Req<PageDTO<ServerScriptFindDTO>, SettingUserVO> req) {
        return serverScriptService.find(req);
    }

    @PostMapping("/edit")
    public Rep<ServerScriptFindVO> edit(@RequestBody Req<ServerScriptFindDTO, SettingUserVO> req) {
        return serverScriptService.edit(req);
    }

    @PostMapping("/del")
    public Rep<ServerScriptFindVO> del(@RequestBody Req<ServerScriptFindDTO, SettingUserVO> req) {
        return serverScriptService.del(req);
    }

    @PostMapping("/command/find")
    public Rep<List<ServerScriptCommandEntity>> commandFind(@RequestBody Req<ServerScriptFindDTO, SettingUserVO> req) {
        return serverScriptService.commandFind(req);
    }

    @PostMapping("/command/del")
    public Rep<ServerScriptCommandEntity> commandDel(@RequestBody Req<ServerScriptCommandEntity, SettingUserVO> req) {
        return serverScriptService.commandDel(req);
    }

    @PostMapping("/exec")
    public Rep<ServerScriptFindVO> exec(@RequestBody Req<ServerScriptFindDTO, SettingUserVO> req) {
        return serverScriptService.exec(req);
    }

    @PostMapping("/hook/{hookId}")
    public Rep<ServerScriptFindVO> hook(@PathVariable("hookId") String hookId, @RequestBody Map<String,Object> map) {
        return serverScriptService.hook(hookId,map);
    }

    @PostMapping("/log/del")
    public Rep<List<ServerScriptLogEntity>> logDel(@RequestBody Req<ServerScriptLogEntity, SettingUserVO> req) {
        return serverScriptService.logDel(req);
    }

    @PostMapping("/log/find")
    public Rep<List<ServerScriptLogEntity>> logFind(@RequestBody Req<ServerScriptLogEntity, SettingUserVO> req) {
        return serverScriptService.logFind(req);
    }
}
