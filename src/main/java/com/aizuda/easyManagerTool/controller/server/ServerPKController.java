package com.aizuda.easyManagerTool.controller.server;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.server.ServerPKEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.server.ServerPKService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/pk")
public class ServerPKController {

    @Resource
    ServerPKService serverPKService;

    @PostMapping("/find")
    public Rep<List<ServerPKEntity>> find(@RequestBody Req<ServerPKEntity, SettingUserVO> req) {
        return serverPKService.find(req);
    }

    @PostMapping("/edit")
    public Rep<ServerPKEntity> edit(@RequestBody Req<ServerPKEntity, SettingUserVO> req){
        return serverPKService.edit(req);
    }

    @PostMapping("/del")
    public Rep<ServerPKEntity> del(@RequestBody Req<ServerPKEntity, SettingUserVO> req){
        return serverPKService.del(req);
    }

}
