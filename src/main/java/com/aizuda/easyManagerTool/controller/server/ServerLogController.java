package com.aizuda.easyManagerTool.controller.server;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.server.ServerLogEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.server.ServerLogService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("serverLog")
public class ServerLogController {

    @Resource
    private ServerLogService serverLogService;

    @PostMapping("/find")
    public Rep<PageVO<ServerLogEntity>> find(@RequestBody Req<PageDTO<ServerLogEntity>, SettingUserVO> req) {
        return serverLogService.find(req);
    }

    @PostMapping("/del")
    public Rep<ServerLogEntity> del(@RequestBody Req<ServerLogEntity, SettingUserVO> req) {
        return serverLogService.del(req);
    }

}
