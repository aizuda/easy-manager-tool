package com.aizuda.easyManagerTool.controller.server;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.server.ServerEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.server.ServerVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.server.ServerService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/server")
public class ServerController {

    @Resource
    ServerService serverService;

    @PostMapping("/find")
    public Rep<PageVO<ServerVO>> find(@RequestBody Req<PageDTO<ServerEntity>, SettingUserVO> req) {
        return serverService.find(req);
    }

//    @PackageAnnotation(index = 0)
    @PostMapping("/edit")
    public Rep<ServerEntity> edit(@RequestBody Req<ServerEntity, SettingUserVO> req) {
        return serverService.edit(req);
    }

    @PostMapping("/addMonitorData")
    public Rep<ServerEntity> addMonitorData(@RequestBody Req<ServerEntity, SettingUserVO> req) {
        return serverService.addMonitorData(req);
    }

    @PostMapping("/del")
    public Rep<ServerEntity> del(@RequestBody Req<ServerEntity, SettingUserVO> req) {
        return serverService.del(req);
    }

    @PostMapping("/findById")
    public Rep<ServerEntity> findById(@RequestBody Req<ServerEntity, SettingUserVO> req) {
        return serverService.findById(req);
    }

    @PostMapping("/findAll")
    public Rep<List<ServerEntity>> findAll(@RequestBody Req<ServerEntity, SettingUserVO> req) {
        return serverService.findAll(req);
    }

    @GetMapping("/monitor/{id}")
    public Object monitor(@PathVariable("id") Integer id) {
        return serverService.monitor(id);
    }

}
