package com.aizuda.easyManagerTool.controller.server;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.server.ServerProxyEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.server.ServerProxyService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/proxy")
public class ServerProxyController {

    @Resource
    ServerProxyService serverProxyService;

    @PostMapping("/find")
    public Rep<List<ServerProxyEntity>> find(@RequestBody Req<ServerProxyEntity, SettingUserVO> req) {
        return serverProxyService.find(req);
    }

    @PostMapping("/edit")
    public Rep<ServerProxyEntity> edit(@RequestBody Req<ServerProxyEntity, SettingUserVO> req){
        return serverProxyService.edit(req);
    }

    @PostMapping("/del")
    public Rep<ServerProxyEntity> del(@RequestBody Req<ServerProxyEntity, SettingUserVO> req){
        return serverProxyService.del(req);
    }

}
