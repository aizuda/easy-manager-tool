package com.aizuda.easyManagerTool.controller;

import cn.hutool.core.util.StrUtil;
import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.exp.impl.BasicException;
import com.aizuda.easy.security.server.EasySecurityServer;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.UploadFileStrategy;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.UUID;

@RequestMapping("public")
@RestController
@Slf4j
public class PublicController {

    @Value("${config.file.path}")
    private String localPath;
    @Value("${setting.terminal.fullPath}")
    private String publicPath;
    @Resource
    private UploadFileStrategy uploadFileStrategy;
    @Resource
    private EasySecurityServer easySecurityServer;

    @PostMapping("/uploadLocal")
    public Rep<String> uploadLocal(@RequestParam("file") MultipartFile file) throws IOException {
        String name = file.getOriginalFilename();
        String string = UUID.randomUUID().toString().replace("-", "");
        String local = localPath + string + (name.contains(".") ? name.substring(name.indexOf(".")): "");
        File fil = new File(local);
        InputStream inputStream = file.getInputStream();
        FileUtils.copyInputStreamToFile(inputStream, fil);
        inputStream.close();
        return Rep.ok(local);
    }

    @PostMapping("/uploadPublic")
    public Rep<String> uploadPublic(@RequestParam("file") MultipartFile file) throws IOException {
        String name = file.getOriginalFilename();
        String string = UUID.randomUUID().toString().replace("-", "");
        String local = localPath + string + (name.contains(".") ? name.substring(name.indexOf(".")): "");
        String publicP = publicPath + string + name.substring(name.indexOf("."));
        File fil = new File(local);
        InputStream inputStream = file.getInputStream();
        FileUtils.copyInputStreamToFile(inputStream, fil);
        inputStream.close();
        return Rep.ok(publicP);
    }

    @PostMapping("/uploadStrategy")
    public Rep<String> uploadStrategy(@RequestParam("file") MultipartFile file,@RequestParam("path") String path,
        HttpServletRequest httpServletRequest
    ) throws IOException, BasicException {
        String token = httpServletRequest.getHeader("token");
        Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
        String name = file.getOriginalFilename();
        String local = localPath + path + "/"+ name;
        File fil = new File(local);
        InputStream inputStream = file.getInputStream();
        SettingUserVO settingUserVO = null;
        if(StrUtil.isNotEmpty(token)) {
            settingUserVO = (SettingUserVO) easySecurityServer.getAuthUser(token);
        }
        if(uploadFileStrategy.exec(local,file, parameterMap,settingUserVO)) {
            FileUtils.copyInputStreamToFile(inputStream, fil);
        }
        inputStream.close();
        return Rep.ok(local);
    }

}
