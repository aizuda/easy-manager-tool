package com.aizuda.easyManagerTool.controller.gpt;


import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.gpt.GPTMessageDTO;
import com.aizuda.easyManagerTool.domain.entity.gpt.GPTChartEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.gpt.GPTChartService;
import com.plexpt.chatgpt.entity.chat.Message;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("gpt")
public class GPTChartController {

    @Resource
    GPTChartService gptChartService;

    @PostMapping("/find")
    public Rep<List<GPTChartEntity>> find(@RequestBody Req<GPTChartEntity, SettingUserVO> req) {
        return gptChartService.find(req);
    }

    @PostMapping("/findByContent")
    public Rep<List<Message>> findByContent(@RequestBody Req<GPTChartEntity, SettingUserVO> req) {
        return gptChartService.findByContent(req);
    }

    @PostMapping("/sendMessage")
    public Rep<GPTMessageDTO> sendMessage(@RequestBody Req<GPTMessageDTO, SettingUserVO> req) {
        return gptChartService.sendMessage(req);
    }

    @PostMapping("/del")
    public Rep<GPTChartEntity> del(@RequestBody Req<GPTChartEntity, SettingUserVO> req) {
        return gptChartService.del(req);
    }

    @PostMapping("/editTitle")
    public Rep<GPTChartEntity> editTitle(@RequestBody Req<GPTChartEntity, SettingUserVO> req){
        return gptChartService.editTitle(req);
    }

    @PostMapping("/addWindow")
    public Rep<GPTChartEntity> addWindow(@RequestBody Req<GPTChartEntity, SettingUserVO> req){
        return gptChartService.addWindow(req);
    }
}
