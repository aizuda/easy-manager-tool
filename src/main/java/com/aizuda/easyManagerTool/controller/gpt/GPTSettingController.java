package com.aizuda.easyManagerTool.controller.gpt;


import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.gpt.GPTSettingEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.gpt.GPTSettingService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("gptSetting")
public class GPTSettingController {

    @Resource
    GPTSettingService gptSettingService;

    @PostMapping("/find")
    public Rep<GPTSettingEntity> find(@RequestBody Req<GPTSettingEntity, SettingUserVO> req) {
        return gptSettingService.find(req);
    }

    @PostMapping("/edit")
    public Rep<GPTSettingEntity> edit(@RequestBody Req<GPTSettingEntity, SettingUserVO> req) {
        return gptSettingService.edit(req);
    }
}
