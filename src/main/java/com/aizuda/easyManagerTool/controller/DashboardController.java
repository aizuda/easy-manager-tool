package com.aizuda.easyManagerTool.controller;


import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.vo.dashboard.DashboardIndexVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.dashboard.DashboardService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/dashboard")
public class DashboardController {

    @Resource
    DashboardService dashboardService;

    @PostMapping("/index")
    public Rep<DashboardIndexVO> index(@RequestBody Req<Object, SettingUserVO> req) {
        return dashboardService.index(req);
    }

}
