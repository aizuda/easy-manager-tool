package com.aizuda.easyManagerTool.controller.dbc;


import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.dbc.db.DBEditDTO;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBDriverEntity;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBEnvEntity;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBPropertiesEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.dbc.db.DBListVO;
import com.aizuda.easyManagerTool.domain.vo.dbc.db.DBTypeVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.dbc.db.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("db")
public class DBController {

    @Resource
    private DBService dbService;
    @Resource
    private DBTypeService dbTypeService;
    @Resource
    private DBEnvService dbEnvService;
    @Resource
    private DBDriverService dbDriverService;
    @Resource
    private DBPropertiesService dbPropertiesService;

    @PostMapping("/find")
    public Rep<PageVO<DBListVO>> find(@RequestBody Req<PageDTO<DBEditDTO>, SettingUserVO> req) {
        return dbService.find(req);
    }

    @PostMapping("/edit")
    public Rep<DBListVO> edit(@RequestBody Req<DBEditDTO, SettingUserVO> req) {
        return dbService.edit(req);
    }

    @PostMapping("/del")
    public Rep<DBListVO> del(@RequestBody Req<DBEditDTO, SettingUserVO> req) {
        return dbService.del(req);
    }

    @PostMapping("/type/find")
    public Rep<List<DBTypeVO>> typeFind(@RequestBody Req<Object, SettingUserVO> req) {
        return dbTypeService.find(req);
    }

    @PostMapping("/env/find")
    public Rep<List<DBEnvEntity>> envFind(@RequestBody Req<DBEditDTO, SettingUserVO> req) {
        return dbEnvService.find();
    }

    @PostMapping("/driver/del")
    public Rep<List<DBEnvEntity>> driverDel(@RequestBody Req<DBDriverEntity, SettingUserVO> req) {
        return dbDriverService.del(req);
    }

    @PostMapping("/pro/find")
    public Rep<List<DBEnvEntity>> proFind(@RequestBody Req<Integer, SettingUserVO> req) {
        return dbPropertiesService.find(req);
    }

    @PostMapping("/pro/del")
    public Rep<List<DBEnvEntity>> proDel(@RequestBody Req<DBPropertiesEntity, SettingUserVO> req) {
        return dbPropertiesService.del(req);
    }

    @PostMapping("/testCon")
    public Rep<String> testCon(@RequestBody Req<DBEditDTO, SettingUserVO> req) {
        return dbService.testCon(req);
    }


}
