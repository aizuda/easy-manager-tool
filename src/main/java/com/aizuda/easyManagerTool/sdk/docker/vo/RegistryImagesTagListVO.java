package com.aizuda.easyManagerTool.sdk.docker.vo;

import lombok.Data;

import java.util.List;

@Data
public class RegistryImagesTagListVO {

    private String name;

    private List<String> tags;

}
