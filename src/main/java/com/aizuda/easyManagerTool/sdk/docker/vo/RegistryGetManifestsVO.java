package com.aizuda.easyManagerTool.sdk.docker.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

@Data
public class RegistryGetManifestsVO {

    private Integer schemaVersion;

    private String mediaType;

    private Config config;

    private String architecture;

    private String digest;

    @JsonIgnore
    private List<Config> layers;

    @Data
    class Config{
        private String mediaType;
        private String digest;
        private Long size;
    }


}
