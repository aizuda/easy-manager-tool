package com.aizuda.easyManagerTool.sdk.docker;

import com.aizuda.easyManagerTool.sdk.docker.api.DockerRegistryAPI;


public class DockerAPI {

    private DockerConfig dockerConfig;
    public DockerAPI(DockerConfig dockerConfig){
        this.dockerConfig = dockerConfig;
    }

    public DockerRegistryAPI getDockerRegistryAPI(){
        return new DockerRegistryAPI(dockerConfig);
    }


}
