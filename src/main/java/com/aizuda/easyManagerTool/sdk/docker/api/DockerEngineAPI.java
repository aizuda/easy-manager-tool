package com.aizuda.easyManagerTool.sdk.docker.api;

import cn.hutool.json.JSONUtil;
import com.aizuda.easyManagerTool.sdk.docker.APIHttpTemplate;
import com.aizuda.easyManagerTool.sdk.docker.DockerConfig;
import com.aizuda.easyManagerTool.sdk.docker.vo.RegistryImagesTagListVO;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import java.util.Base64;

public class DockerEngineAPI {

    private DockerConfig dockerConfig;
    private APIHttpTemplate apiHttpTemplate = new APIHttpTemplate();
    public DockerEngineAPI(DockerConfig dockerConfig) {
        this.dockerConfig = dockerConfig;
    }
    private static final String IMAGE_PUSH = "images/%s/push";


    public RegistryImagesTagListVO pushImage(String imageName){
        try {
            String url = String.format(IMAGE_PUSH ,dockerConfig.getEngineUrl(),imageName);
            HttpPost httpPost = new HttpPost(url);
            String auth = Base64.getEncoder().encodeToString((dockerConfig.getUserName() + ":" + dockerConfig.getPassword()).getBytes());
            httpPost.setHeader("X-Registry-Auth",auth);
            String get = apiHttpTemplate.BASE(httpPost);
            return JSONUtil.toBean(get,RegistryImagesTagListVO.class);
        }catch (Exception e){
            throw new IllegalArgumentException(e.getMessage());
        }
    }


}
