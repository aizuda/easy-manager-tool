package com.aizuda.easyManagerTool.sdk.docker;

import com.aizuda.easyManagerTool.sdk.docker.api.DockerRegistryAPI;
import com.aizuda.easyManagerTool.sdk.docker.vo.RegistryImageListVO;
import com.aizuda.easyManagerTool.sdk.docker.vo.RegistryImagesTagListVO;
import lombok.Data;


@Data
public class DockerConfig {

    private String registryUrl;

    private String engineUrl;

    private String userName;

    private String password;

    public DockerConfig(Builder builder) {
        this.registryUrl = builder.registryUrl;
        this.userName = builder.userName;
        this.password = builder.password;
    }

    public static class Builder {
        private String registryUrl;

        private String userName;

        private String password;

        public Builder registryUrl(String registryUrl) {
            this.registryUrl = registryUrl;
            return this;
        }

        public Builder userName(String userName) {
            this.userName = userName;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public DockerAPI build() {
            return new DockerAPI(new DockerConfig(this));
        }
    }


}
