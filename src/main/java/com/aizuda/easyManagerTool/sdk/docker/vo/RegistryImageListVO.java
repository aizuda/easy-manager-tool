package com.aizuda.easyManagerTool.sdk.docker.vo;

import lombok.Data;

import java.util.List;

@Data
public class RegistryImageListVO {

    private List<String> repositories;

}
