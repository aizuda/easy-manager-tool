package com.aizuda.easyManagerTool.mapper.docker;

import com.aizuda.easyManagerTool.domain.entity.docker.DockerCloudUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DockerCloudUserMapper extends BaseMapper<DockerCloudUserEntity> {


    void insertBatch(List<DockerCloudUserEntity> collect);

    @Delete("delete from tool_docker_cloud_user where cloud_id = #{id}")
    void clearCloudId(Integer id);
}
