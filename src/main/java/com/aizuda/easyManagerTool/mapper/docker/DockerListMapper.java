package com.aizuda.easyManagerTool.mapper.docker;

import com.aizuda.easyManagerTool.domain.entity.docker.DockerListEntity;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerListVO;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DockerListMapper extends BaseMapper<DockerListEntity> {
    IPage<DockerListVO> find(Page<DockerListEntity> page,@Param("data") DockerListEntity dockerListEntity);

    @InterceptorIgnore(tenantLine = "true")
    List<DockerListVO> findAll();

    List<DockerListEntity> selectByServerIds(@Param("list") List<Integer> serverIds);

    DockerListVO findById(Integer id);
}