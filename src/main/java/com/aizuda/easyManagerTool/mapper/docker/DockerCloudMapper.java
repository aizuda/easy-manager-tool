package com.aizuda.easyManagerTool.mapper.docker;

import com.aizuda.easyManagerTool.domain.entity.docker.DockerCloudEntity;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerCloudFindVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface DockerCloudMapper extends BaseMapper<DockerCloudEntity> {

    IPage<DockerCloudFindVO> find(Page<DockerCloudEntity> page, @Param("data") DockerCloudEntity dockerCloudEntity);

    @Delete("delete from tool_docker_cloud where id = #{id} and user_id = #{id1}")
    int deleteByIdAndUserId(Integer id, Integer id1);

    @Select("select * from tool_docker_cloud where id = #{id} and user_id = #{id1}")
    DockerCloudEntity findIdAndUserId(Integer id, Integer id1);
}
