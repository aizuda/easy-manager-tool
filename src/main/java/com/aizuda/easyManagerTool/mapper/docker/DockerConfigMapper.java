package com.aizuda.easyManagerTool.mapper.docker;

import com.aizuda.easyManagerTool.domain.entity.docker.DockerConfigEntity;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerConfigFindVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DockerConfigMapper extends BaseMapper<DockerConfigEntity> {

    List<DockerConfigFindVO> find(@Param("data") DockerConfigEntity dockerConfigEntity);

    DockerConfigFindVO findByServerId(Integer serverId);

    DockerConfigFindVO findById(Integer configId);
}
