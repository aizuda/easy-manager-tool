package com.aizuda.easyManagerTool.mapper.dbc.db;

import com.aizuda.easyManagerTool.domain.dto.dbc.db.DBEditDTO;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBEntity;
import com.aizuda.easyManagerTool.domain.vo.dbc.db.DBListVO;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface DBMapper extends BaseMapper<DBEntity> {

    @InterceptorIgnore(tenantLine = "true")
    IPage<DBListVO> find(Page<DBEditDTO> page, @Param("data") DBEntity data);

    @Select("select count(*) from tool_db")
    long findCount();
}
