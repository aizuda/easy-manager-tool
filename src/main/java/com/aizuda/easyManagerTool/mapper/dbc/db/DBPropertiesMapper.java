package com.aizuda.easyManagerTool.mapper.dbc.db;

import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBPropertiesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DBPropertiesMapper extends BaseMapper<DBPropertiesEntity> {


    @Select("select * from tool_db_properties where db_id = #{data}")
    List<DBPropertiesEntity> find(Integer data);

    @Delete("delete from tool_db_properties where db_id = #{dbId} and id = #{id}")
    void del(Integer id, Integer dbId);
}
