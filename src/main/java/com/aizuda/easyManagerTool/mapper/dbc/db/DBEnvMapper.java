package com.aizuda.easyManagerTool.mapper.dbc.db;

import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBEnvEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DBEnvMapper extends BaseMapper<DBEnvEntity> {

    @Select("select * from tool_db_env")
    List<DBEnvEntity> findAll();

}
