package com.aizuda.easyManagerTool.mapper.dbc.db;

import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBTypeEntity;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DBTypeMapper extends BaseMapper<DBTypeEntity> {

    @InterceptorIgnore(tenantLine = "true")
    @Select("select * from tool_db_type")
    List<DBTypeEntity> findAll();

}
