package com.aizuda.easyManagerTool.mapper.dbc.db;

import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBDriverEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DBDriverMapper extends BaseMapper<DBDriverEntity> {


    @Select("select * from tool_db_driver")
    List<DBDriverEntity> findAll();
}
