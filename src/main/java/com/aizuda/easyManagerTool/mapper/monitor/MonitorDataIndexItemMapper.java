package com.aizuda.easyManagerTool.mapper.monitor;

import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorDataIndexItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MonitorDataIndexItemMapper extends BaseMapper<MonitorDataIndexItemEntity> {


    @Delete("delete from tool_monitor_data_index_item where mdi_id = #{id}")
    void deleteByMdiId(Integer id);

    @Select("select *  from tool_monitor_data_index_item where mdi_id = #{id}")
    List<MonitorDataIndexItemEntity> find(Integer mdiId);

    @Delete("delete from tool_monitor_data_index_item where md_id = #{id}")
    void deleteByDataId(Integer id);

    void insertBatch(@Param("list") List<MonitorDataIndexItemEntity> collect);

    @Select("select * from tool_monitor_data_index_item where mdi_id = #{id}")
    List<MonitorDataIndexItemEntity> findByMdiId(Integer id);
}
