package com.aizuda.easyManagerTool.mapper.monitor;

import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorScreenEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface MonitorScreenMapper extends BaseMapper<MonitorScreenEntity> {


    IPage<MonitorScreenEntity> find(Page<MonitorScreenEntity> page,@Param("data") MonitorScreenEntity monitorScreenEntity);

    @Select("select * from tool_monitor_screen where id = #{id}")
    MonitorScreenEntity findById(Integer id);
}
