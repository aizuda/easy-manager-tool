package com.aizuda.easyManagerTool.mapper.monitor;

import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorUrlEntity;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MonitorUrlMapper extends BaseMapper<MonitorUrlEntity> {


    @InterceptorIgnore(tenantLine = "true")
    @Delete("delete from tool_monitor_url where md_id = #{id}")
    void deleteByMdId(Integer id);

    @InterceptorIgnore(tenantLine = "true")
    void insertBatch(List<MonitorUrlEntity> urls);

    @InterceptorIgnore(tenantLine = "true")
    List<MonitorUrlEntity> selectByMdId(Integer id);

    @InterceptorIgnore(tenantLine = "true")
    @Delete("delete from tool_monitor_url where id = #{id}")
    void deleteId(Integer id);
}
