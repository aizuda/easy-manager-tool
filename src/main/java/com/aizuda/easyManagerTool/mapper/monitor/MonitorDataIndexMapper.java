package com.aizuda.easyManagerTool.mapper.monitor;

import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorDataIndexEntity;
import com.aizuda.easyManagerTool.domain.vo.terminal.TreeIndexItemSelectVO;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MonitorDataIndexMapper extends BaseMapper<MonitorDataIndexEntity> {


    @Select("select * from tool_monitor_data_index where md_id = #{mdi.mdId} order by create_time desc ")
    IPage<MonitorDataIndexEntity> find(Page<MonitorDataIndexEntity> page,@Param("mdi") MonitorDataIndexEntity monitorDataIndexEntity);

    List<TreeIndexItemSelectVO> selectTreeIndex();

    @InterceptorIgnore(tenantLine = "true")
    List<TreeIndexItemSelectVO> selectAllIndex(Integer id,Integer mdiId,Integer mdiiId);

    @Delete("delete from tool_monitor_data_index where md_id = #{id}")
    void deleteByDataId(Integer id);

    @Select("select * from tool_monitor_data_index where md_id = #{id}")
    List<MonitorDataIndexEntity> findAll(Integer id);

    void insertBatch(List<MonitorDataIndexEntity> collect);
}
