package com.aizuda.easyManagerTool.mapper.monitor;

import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorAlarmEntity;
import com.aizuda.easyManagerTool.domain.vo.dashboard.DashboardFind15DayList;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MonitorAlarmMapper extends BaseMapper<MonitorAlarmEntity> {



    IPage<MonitorAlarmEntity> find(Page<MonitorAlarmEntity> page,@Param("data") MonitorAlarmEntity monitorAlarmEntity);

    @InterceptorIgnore(tenantLine = "true")
    @Select("select IFNULL(COUNT(id),0) from tool_monitor_alarm where tenant_id = #{tenantId} and ma_handle = 0")
    Integer findTotal(Integer tenantId);

    @InterceptorIgnore(tenantLine = "true")
    List<DashboardFind15DayList> find15DayList(Integer tenantId);

}
