package com.aizuda.easyManagerTool.mapper.monitor;

import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorDataEntity;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface MonitorDataMapper extends BaseMapper<MonitorDataEntity> {



    IPage<MonitorDataEntity> find(Page<MonitorDataEntity> page,@Param("data") MonitorDataEntity monitorDataEntity);

    @InterceptorIgnore(tenantLine = "true")
    @Select("select IFNULL(COUNT(id),0) from tool_monitor_data where tenant_id = #{tenantId}")
    Integer findTotal(Integer tenantId);

    MonitorDataEntity findByUrl(String url,Integer tenantId);

    @Select("select * from tool_monitor_data where id = #{id}")
    MonitorDataEntity findById(Integer id);
}
