package com.aizuda.easyManagerTool.mapper.server;

import com.aizuda.easyManagerTool.domain.entity.server.ServerEntity;
import com.aizuda.easyManagerTool.domain.entity.server.ServerLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;



/**
 * @author big uncle
 * @date 2020/6/29 13:59
 **/
@Mapper
@Repository
public interface ServerLogMapper extends BaseMapper<ServerLogEntity> {


    IPage<ServerLogEntity> find(Page<ServerEntity> page,@Param("sl") ServerLogEntity serverLogEntity,@Param("roleId") String roleId);
}
