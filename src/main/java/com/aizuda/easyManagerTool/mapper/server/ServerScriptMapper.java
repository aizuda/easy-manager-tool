package com.aizuda.easyManagerTool.mapper.server;

import com.aizuda.easyManagerTool.domain.dto.server.ServerScriptFindDTO;
import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptEntity;
import com.aizuda.easyManagerTool.domain.vo.server.ServerScriptFindVO;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


@Mapper
@Repository
public interface ServerScriptMapper extends BaseMapper<ServerScriptEntity> {

    IPage<ServerScriptFindVO> find(Page<ServerScriptFindDTO> page, @Param("data") ServerScriptFindDTO serverScriptFindDTO, String roleId);


    @InterceptorIgnore(tenantLine = "true")
    ServerScriptFindVO findByHookId(String hookId);
}
