package com.aizuda.easyManagerTool.mapper.server;

import com.aizuda.easyManagerTool.domain.entity.server.ServerProxyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface ServerProxyMapper extends BaseMapper<ServerProxyEntity> {

    List<ServerProxyEntity> findAll();
}
