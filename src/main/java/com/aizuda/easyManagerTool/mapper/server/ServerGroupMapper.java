package com.aizuda.easyManagerTool.mapper.server;

import com.aizuda.easyManagerTool.domain.entity.server.ServerGroupEntity;
import com.aizuda.easyManagerTool.domain.vo.server.ServerSettingVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author big uncle
 * @date 2020/6/29 13:59
 **/
@Mapper
@Repository
public interface ServerGroupMapper extends BaseMapper<ServerGroupEntity> {

    @Select("select *,group_name as settingName,role_id as settingRoles from tool_server_group")
    List<ServerSettingVO> findAll();

    @Delete("delete from tool_server_group where id = #{id}")
    void deleTrue(Integer id);

    @Select("select *,group_name as settingName,role_id as settingRoles from tool_server_group where FIND_IN_SET (#{id},role_id) > 0")
    List<ServerSettingVO> findByRoleId(String id);
}
