package com.aizuda.easyManagerTool.mapper.server;

import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface ServerScriptLogMapper extends BaseMapper<ServerScriptLogEntity> {

    @Select("select * from tool_server_script_log where script_id = #{scriptId} order by id desc")
    List<ServerScriptLogEntity> findAll(Integer scriptId);

    @Delete("delete from tool_server_script_log where script_id = #{id}")
    void delByScriptId(Integer id);
}
