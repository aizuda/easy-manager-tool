package com.aizuda.easyManagerTool.mapper.server;

import com.aizuda.easyManagerTool.domain.entity.server.ServerEntity;
import com.aizuda.easyManagerTool.domain.vo.server.ServerCompleteVO;
import com.aizuda.easyManagerTool.domain.vo.server.ServerVO;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface ServerMapper extends BaseMapper<ServerEntity> {

    IPage<ServerVO> find(Page<ServerEntity> page, @Param("se") ServerEntity se, @Param("roleId") String roleId);

    @Delete("delete from tool_server where id = #{id}")
    void delTrue(Integer id);

    List<ServerEntity> findBatchIp(List<String> split);

    @InterceptorIgnore(tenantLine = "true")
    @Select("select IFNULL(COUNT(id),0) from tool_server where tenant_id = #{tenantId}")
    Integer findTotal(Integer tenantId);

    @InterceptorIgnore(tenantLine = "true")
    ServerCompleteVO findById(Integer serverId);

    @Update("UPDATE tool_server SET proxy_id = NULL WHERE proxy_id = #{id}")
    void updateByProxyId(Integer id);

    @Update("UPDATE tool_server SET pk_id = NULL WHERE id = #{serverId} and pk_id = #{id}")
    void updatePK(Integer serverId,Integer id);

    List<ServerEntity> findAll(@Param("roleId") String roleId);

    List<ServerCompleteVO> findByIds(@Param("serverIds") List<Integer> serverIds);

    @InterceptorIgnore(tenantLine = "true")
    List<ServerCompleteVO> findByIdsNoTenantId(@Param("serverIds") List<Integer> serverIds);

}
