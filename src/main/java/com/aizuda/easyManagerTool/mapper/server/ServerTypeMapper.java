package com.aizuda.easyManagerTool.mapper.server;

import com.aizuda.easyManagerTool.domain.entity.server.ServerTypeEntity;
import com.aizuda.easyManagerTool.domain.vo.server.ServerSettingVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author big uncle
 * @date 2020/6/29 13:59
 **/
@Mapper
@Repository
public interface ServerTypeMapper extends BaseMapper<ServerTypeEntity> {


    @Select("select *,type_name as settingName from tool_server_type")
    List<ServerSettingVO> findAll();

    @Delete("delete from tool_server_type where id=#{id}")
    void delTrue(Integer id);
}
