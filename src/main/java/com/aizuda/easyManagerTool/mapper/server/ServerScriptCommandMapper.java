package com.aizuda.easyManagerTool.mapper.server;

import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptCommandEntity;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface ServerScriptCommandMapper extends BaseMapper<ServerScriptCommandEntity> {


    @InterceptorIgnore(tenantLine = "true")
    @Select("select * from tool_server_script_command where script_id = #{id}")
    List<ServerScriptCommandEntity> findByScriptId(Integer id);

    @Delete("delete from tool_server_script_command where script_id = #{id}")
    void delByScriptId(Integer id);
}
