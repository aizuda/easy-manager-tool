package com.aizuda.easyManagerTool.mapper.server;

import com.aizuda.easyManagerTool.domain.entity.server.ServerPKEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface ServerPKMapper extends BaseMapper<ServerPKEntity> {


    List<ServerPKEntity> findByAll(Integer serverId);

    @Delete("delete from tool_server_pk where server_id = #{id}")
    void deleteServerId(Integer id);
}
