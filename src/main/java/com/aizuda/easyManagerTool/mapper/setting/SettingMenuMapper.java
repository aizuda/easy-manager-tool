package com.aizuda.easyManagerTool.mapper.setting;

import com.aizuda.easyManagerTool.domain.entity.setting.SettingMenuEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingMenuListVO;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author big uncle
 * @date 2020/6/29 13:59
 **/
@Mapper
@Repository
public interface SettingMenuMapper extends BaseMapper<SettingMenuEntity> {

    @InterceptorIgnore(tenantLine = "true")
    @Select("select * from tool_setting_menu order by menu_sort")
    List<SettingMenuListVO> findAll();

    @InterceptorIgnore(tenantLine = "true")
    List<SettingMenuListVO> findMenuIds(List<Integer> menuIds);
}
