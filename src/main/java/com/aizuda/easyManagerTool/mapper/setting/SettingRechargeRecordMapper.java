

package com.aizuda.easyManagerTool.mapper.setting;

import com.aizuda.easyManagerTool.domain.entity.setting.SettingRechargeRecordEntity;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SettingRechargeRecordMapper extends BaseMapper<SettingRechargeRecordEntity> {

    IPage<SettingRechargeRecordEntity> find(Page<SettingRechargeRecordEntity> page,@Param("data") SettingRechargeRecordEntity settingRechargeRecord);

    SettingRechargeRecordEntity findByOrder(String orderNumber);

    void updateByOrder(@Param("data")SettingRechargeRecordEntity byOrder);

    @InterceptorIgnore(tenantLine = "true")
    @Select("SELECT * FROM tool_setting_recharge_record WHERE tenant_id = #{tenantId} AND expiration_date IS NOT NULL ORDER BY create_time DESC LIMIT 1")
    SettingRechargeRecordEntity findUserPackage(Integer tenantId);

    @Delete("delete from tool_setting_recharge_record where order_number = #{orderNumber}")
    void deleteByOrderNumber(String orderNumber);
}
