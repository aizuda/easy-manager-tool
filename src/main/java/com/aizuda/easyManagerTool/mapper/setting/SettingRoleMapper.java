package com.aizuda.easyManagerTool.mapper.setting;

import com.aizuda.easyManagerTool.domain.entity.setting.SettingRoleEntity;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author big uncle
 * @date 2020/6/29 13:59
 **/
@Mapper
@Repository
public interface SettingRoleMapper extends BaseMapper<SettingRoleEntity> {

    IPage<SettingRoleEntity> findAll(Page<SettingRoleEntity> page,@Param("data") SettingRoleEntity settingRoleEntity);

    @Select("select * from tool_setting_role")
    List<SettingRoleEntity> findList();

    @InterceptorIgnore(tenantLine = "true")
    @Select("select * from tool_setting_role where id = #{id}")
    SettingRoleEntity findById(Integer id);
}
