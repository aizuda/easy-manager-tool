package com.aizuda.easyManagerTool.mapper.setting;

import com.aizuda.easyManagerTool.domain.entity.setting.SettingAlarmEntity;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


/**
 * @author big uncle
 * @date 2020/6/29 13:59
 **/
@Mapper
@Repository
public interface SettingAlarmMapper extends BaseMapper<SettingAlarmEntity> {

    SettingAlarmEntity find();

    @InterceptorIgnore(tenantLine = "true")
    SettingAlarmEntity findByTenantId(Integer tenantId);
}
