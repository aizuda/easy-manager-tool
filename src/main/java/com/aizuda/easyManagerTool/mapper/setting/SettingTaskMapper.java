package com.aizuda.easyManagerTool.mapper.setting;

import com.aizuda.easyManagerTool.domain.entity.setting.SettingTaskEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingTaskFindVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author big uncle
 * @date 2020/6/29 13:59
 **/
@Mapper
@Repository
public interface SettingTaskMapper extends BaseMapper<SettingTaskEntity> {

    List<SettingTaskFindVO> findAllTask(@Param("data") SettingTaskEntity data,@Param("userId") Integer userId);
}
