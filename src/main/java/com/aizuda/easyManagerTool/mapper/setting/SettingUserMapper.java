package com.aizuda.easyManagerTool.mapper.setting;

import com.aizuda.easyManagerTool.domain.dto.setting.SettingUserLoginDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author big uncle
 * @date 2020/6/29 13:59
 **/
@Mapper
@Repository
public interface SettingUserMapper extends BaseMapper<SettingUserEntity> {


    SettingUserEntity findByAccountAndPassword(@Param("data") SettingUserLoginDTO data);

    IPage<SettingUserEntity> find(Page<SettingUserEntity> page,@Param("data") SettingUserEntity settingUserEntity,@Param("tenantId") Integer tenantId);

    @Delete("delete from tool_setting_user where id = #{id}")
    void delTrue(Integer id);

    @Select("select * from tool_setting_user where user_account = #{account}")
    SettingUserEntity findByAccount(String account);

    @Select("select * from tool_setting_user where user_account = #{account} and id != #{id}")
    SettingUserEntity findByAccounts(Integer id,String account);

    List<SettingUserEntity> findTenantId(Integer tenantId);

    List<SettingUserEntity> findAll(Integer tenantId);

}
