package com.aizuda.easyManagerTool.mapper.gpt;

import com.aizuda.easyManagerTool.domain.entity.gpt.GPTSettingEntity;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GPTSettingMapper extends BaseMapper<GPTSettingEntity> {


    GPTSettingEntity find();

    @InterceptorIgnore(tenantLine = "true")
    List<GPTSettingEntity> findAll();
}
