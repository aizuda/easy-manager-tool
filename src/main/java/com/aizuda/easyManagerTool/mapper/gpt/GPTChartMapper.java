package com.aizuda.easyManagerTool.mapper.gpt;

import com.aizuda.easyManagerTool.domain.entity.gpt.GPTChartEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GPTChartMapper extends BaseMapper<GPTChartEntity> {

    @Select("select id,gc_title,user_id from tool_gpt_chart where user_id = #{id}")
    List<GPTChartEntity> findAll(Integer id);

    @Update("update tool_gpt_chart set gc_title = #{data.gcTitle} where id = #{data.id} and user_id = #{data.userId}")
    void updateByTitle(@Param("data") GPTChartEntity data);
}
