package com.aizuda.easyManagerTool;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;


@SpringBootApplication
@EnableAsync
public class ToolBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ToolBootApplication.class, args);
    }

}
