package com.aizuda.easyManagerTool.config;

import com.baomidou.mybatisplus.extension.ddl.SimpleDdl;
import java.util.Arrays;
import java.util.List;


public class InitSQLConfig extends SimpleDdl {

    @Override
    public List<String> getSqlFiles() {
        return Arrays.asList("init.sql");
    }

}
