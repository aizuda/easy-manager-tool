package com.aizuda.easyManagerTool.config.security;

import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.aizuda.easy.security.code.BasicCode;
import com.aizuda.easy.security.domain.LocalEntity;
import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.exp.impl.BasicException;
import com.aizuda.easy.security.handler.RepFunctionHandler;
import com.aizuda.easy.security.handler.ReqFunctionHandler;
import com.aizuda.easy.security.util.LocalUtil;
import com.aizuda.easyManagerTool.config.TenantConfig;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingUserEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class RenewalHandler implements ReqFunctionHandler, RepFunctionHandler {

    private Map<String,Renewal> renewalMap = new ConcurrentHashMap<>();
    private Long exps = 60L * 1000 * 30;
    @Override
    public String exec(HttpServletRequest request, String json) throws BasicException, IOException {
        LocalEntity localEntity = LocalUtil.getLocalEntity();
        if(localEntity.getProject()){
            return json;
        }
        String token = request.getHeader("token");
//        Renewal renewal = renewalMap.computeIfAbsent(token,k -> new Renewal());
        checkSessionInvalid(token).setExp(System.currentTimeMillis() + exps);
        SettingUserEntity user = LocalUtil.<SettingUserEntity>getUser();
        TenantConfig.tenantContext.set(user.getTenantId().toString());
        return json;
    }

    public Renewal checkSessionInvalid(String token) throws BasicException {
        Renewal renewal =  renewalMap.get(token);
        // 判断是否失效
        if(ObjectUtil.isEmpty(renewal) || System.currentTimeMillis() > renewal.getExp()){
            throw new BasicException(BasicCode.BASIC_CODE_401);
        }
        return renewal;
    }

    @Override
    public Integer getIndex() {
        return 10;
    }

    @Override
    public String exec(HttpServletResponse response, String json) throws BasicException, IOException {
        String url = response.getHeader("requestUri");
        if("/setting/login".equals(url)){
            Rep<SettingUserVO> rep = JSONUtil.toBean(json, new TypeReference<Rep<SettingUserVO>>(){},true);
            SettingUserVO settingUserVO = rep.getData();
            if(ObjectUtil.isEmpty(settingUserVO) || StrUtil.isEmpty(settingUserVO.getToken())){
                return json;
            }
            renewalMap.computeIfAbsent(rep.getData().getToken(),k -> new Renewal());
        }
        return json;
    }

    @Data
    class Renewal{
        private String account;
        private Long exp = System.currentTimeMillis() + exps;
    }

}
