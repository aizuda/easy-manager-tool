package com.aizuda.easyManagerTool.config.socket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.function.Consumer;
import java.util.stream.IntStream;

@Slf4j
public class SocketSend {

    public static void send(WebSocketSession session,Object str){
        try {
            synchronized (session) {
                TextMessage textMessage = new TextMessage(str.toString());
                session.sendMessage(textMessage);
            }
        } catch (Exception e) {
            log.error("socket消息发送失败 {}",e.getMessage());
        }
    }

}
