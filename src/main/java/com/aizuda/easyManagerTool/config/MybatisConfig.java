package com.aizuda.easyManagerTool.config;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;
import java.util.List;

@Configuration
public class MybatisConfig implements MetaObjectHandler {

    @Value("${spring.datasource.tenant.ignoreTable:''}")
    private List<String> ignoreTable;

    @Value("${spring.datasource.tenant.idName:tenant_id}")
    private String idName;

    @Value("${spring.datasource.tenant.enable:false}")
    private Boolean enable;

    @Bean
    public InitSQLConfig initSQLConfig() {
        return new InitSQLConfig();
    }

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new TenantConfig(ignoreTable, idName));
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        // 乐观锁，在实体字段加 @Version即可
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        return interceptor;
    }


    @Override
    public void insertFill(MetaObject metaObject) {
        String tenantId = TenantConfig.tenantContext.get();
        if(StrUtil.isNotEmpty(tenantId)) {
            this.setFieldValByName("tenantId", Integer.valueOf(tenantId), metaObject);
        }
        this.setFieldValByName("createTime", new Date(), metaObject);
        this.setFieldValByName("updateTime", new Date(), metaObject);
//        encryption(metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateTime", new Date(), metaObject);
//        encryption(metaObject);
    }

//    private void encryption(MetaObject metaObject){
//        Class<?> aClass = metaObject.getOriginalObject().getClass();
//        List<String> strings = CiphertextHandler.get(aClass.getName());
//        if(CollUtil.isEmpty(strings)){
//            Field[] declaredFields = aClass.getDeclaredFields();
//            for (Field declaredField : declaredFields) {
//                FieldEncrypt annotation = declaredField.getAnnotation(FieldEncrypt.class);
//                if(ObjectUtil.isEmpty(annotation)){
//                    continue;
//                }
//                CiphertextHandler.put(aClass.getName(),declaredField.getName());
//                setValue(metaObject,declaredField.getName(),metaObject.getValue(declaredField.getName()));
//            }
//        }else {
//            for (String string : strings) {
//                setValue(metaObject,string,metaObject.getValue(string));
//            }
//        }
//    }
//
//
//    private void setValue(MetaObject metaObject,String name,Object value){
//        if(ObjectUtil.isEmpty(value)){
//            return;
//        }
//        if(!(value instanceof String)){
//            return;
//        }
//        this.setFieldValByName(name, CiphertextHandler.encrypt(String.valueOf(value)), metaObject);
//    }




}
