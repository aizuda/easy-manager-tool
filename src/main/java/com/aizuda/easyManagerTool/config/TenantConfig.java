package com.aizuda.easyManagerTool.config;

import com.alibaba.ttl.TransmittableThreadLocal;
import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;

import java.util.List;

public class TenantConfig extends TenantLineInnerInterceptor  {

    public static TransmittableThreadLocal<String> tenantContext = new TransmittableThreadLocal<>();

    public TenantConfig(List<String> ignoreTable, String idName) {
        super(new TenantLineHandler() {
            @Override
            public Expression getTenantId() {
                String tenantId = tenantContext.get();
                return new LongValue(tenantId == null? "-1": tenantId);
            }

            @Override
            public boolean ignoreTable(String tableName) {
                return ignoreTable.contains(tableName);
            }

            @Override
            public String getTenantIdColumn() {
                return idName;
            }
        });
    }

}
