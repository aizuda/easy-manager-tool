package com.aizuda.easyManagerTool.config;

import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.File;
import java.nio.file.Files;

@Component
@Data
public class YmlVarConfig implements InitializingBean {



    @Value("${setting.terminal.path}")
    private String path;

    @Value("${setting.terminal.source}")
    private String source;

    @Value("${setting.terminal.fullPath}")
    private String fullPath;

    @Override
    public void afterPropertiesSet() throws Exception {
        File files = new File(path);
        files.mkdirs();
    }

}
