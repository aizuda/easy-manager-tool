package com.aizuda.easyManagerTool.config.aspect.annotation;



import java.lang.annotation.*;

/**
 * @author BigUncle
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PackageAnnotation {

    int index() default 0;

}
