package com.aizuda.easyManagerTool.config.aspect;

import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.config.aspect.annotation.PackageAnnotation;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.setting.impl.PackageCheckStrategy;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


@Aspect
@Component
public class PackageAspect {

    @Resource
    PackageCheckStrategy packageCheckStrategy;

    @Before("execution(* com.aizuda.easyManagerTool.controller..*(..))")
    public void Before(JoinPoint point) throws Throwable {
        MethodSignature method = (MethodSignature) point.getSignature();
        PackageAnnotation packageAnnotation = method.getMethod().getDeclaredAnnotation(PackageAnnotation.class);
        if(ObjectUtil.isEmpty(packageAnnotation)){
            return;
        }
        int index = packageAnnotation.index();
        Object[] args = point.getArgs();
        if(args == null || args.length < 1){
            return;
        }
        Req<Object, SettingUserVO> req = (Req<Object, SettingUserVO>) args[0];
        SettingUserVO user = req.getUser();
//        PackagesBO packageBO = user.getPackageBO();
//        PackagesBO.Item item = packageBO.getItems().get(index);
//        packageCheckStrategy.check(item, user.getTenantId());
    }


}
