package com.aizuda.easyManagerTool.service.terminal;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.terminal.SSHMessageDTO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.domain.vo.terminal.MonitorVO;

public interface MonitorTerminalService {

    Rep<String> monitor(Req<SSHMessageDTO, SettingUserVO> req);

    MonitorVO monitor(Integer id);

}
