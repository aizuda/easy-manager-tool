package com.aizuda.easyManagerTool.service.terminal;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.terminal.SFTPFileDTO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.domain.vo.terminal.SFTPFileVO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ExecuteService {

    Rep<List<SFTPFileVO>> del(Req<SFTPFileDTO, SettingUserVO> req);

    Rep<String> upload(String path, String token, MultipartFile file);

    Rep<String> download(Req<SFTPFileDTO, SettingUserVO> req, HttpServletRequest request);
}
