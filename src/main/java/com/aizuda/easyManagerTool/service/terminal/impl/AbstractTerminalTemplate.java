package com.aizuda.easyManagerTool.service.terminal.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.aizuda.easyManagerTool.domain.dto.socket.SocketMessageDTO;
import com.aizuda.easyManagerTool.domain.dto.terminal.SSHMessageDTO;
import com.aizuda.easyManagerTool.domain.dto.terminal.SSHWindowsDTO;
import com.aizuda.easyManagerTool.domain.vo.socket.SocketMessageVO;
import com.aizuda.easyManagerTool.service.socket.SocketEventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

@Slf4j
public abstract class AbstractTerminalTemplate implements SocketEventService {
    SocketMessageVO<String> socketMessageVO = new SocketMessageVO();
    @Override
    public void onMessage(String token, WebSocketSession session, SocketMessageDTO message) throws IOException {
        JSONObject jsonObject = (JSONObject) message.getJson();
        SSHMessageDTO messageDTO = jsonObject.toBean(new TypeReference<SSHMessageDTO>() {});
        TerminalSessionManager.Manager manager = TerminalSessionManager.setGet(token);
        try {
            switch (messageDTO.getMethod()) {
                case "init": init(token,session, manager, messageDTO); break;
                case "command": connect(manager, messageDTO); break;
                case "window": window(manager, messageDTO); break;
//                case "close": closeEvent(manager);TerminalSessionManager.remove(token); break;
                default: break;
            }
        } catch (Exception e) {
            SocketMessageVO<String> messageVO = BeanUtil.copyProperties(socketMessageVO, SocketMessageVO.class);
            messageVO.setObj(StrUtil.isBlank(e.getMessage())? "连接错误，请检查配置": e.getMessage());
            messageVO.setType("command");
            session.sendMessage(new TextMessage(JSONUtil.toJsonStr(messageVO)));
            closeEvent(manager);
            TerminalSessionManager.remove(token);
            log.error("连接失败 {}",e.getMessage());
        }
    }

    private void window(TerminalSessionManager.Manager manager, SSHMessageDTO messageDTO) {
        SSHWindowsDTO window = messageDTO.getWindow();
        manager.getChannelShell().setPtySize( window.getClos(), window.getRows(), window.getWidth(), window.getHeight());
    }

    abstract void init(String token, WebSocketSession message, TerminalSessionManager.Manager manager, SSHMessageDTO messageDTO) throws Exception;

    abstract void connect(TerminalSessionManager.Manager manager, SSHMessageDTO messageDTO) throws Exception;

    abstract void close(TerminalSessionManager.Manager manager);

    public void closeEvent(TerminalSessionManager.Manager manager){
        manager.endLog();
        close(manager);
        manager.close();
    }



}
