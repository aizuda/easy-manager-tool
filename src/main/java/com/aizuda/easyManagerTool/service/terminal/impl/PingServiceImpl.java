package com.aizuda.easyManagerTool.service.terminal.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.aizuda.easyManagerTool.config.socket.SocketSend;
import com.aizuda.easyManagerTool.domain.dto.socket.SocketMessageDTO;
import com.aizuda.easyManagerTool.domain.vo.socket.SocketMessageVO;
import com.aizuda.easyManagerTool.service.socket.SocketEventService;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

@Service("ping")
public class PingServiceImpl implements SocketEventService {

    SocketMessageVO<String> socketMessageVO = new SocketMessageVO<String>();

    @Override
    public void onMessage(String token, WebSocketSession session, SocketMessageDTO message) throws IOException {
        SocketMessageVO messageVO = BeanUtil.copyProperties(socketMessageVO, SocketMessageVO.class);
        messageVO.setType("ping");
        SocketSend.send(session,JSONUtil.toJsonStr(messageVO));
    }

}
