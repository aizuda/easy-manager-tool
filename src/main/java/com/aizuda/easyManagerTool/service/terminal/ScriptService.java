package com.aizuda.easyManagerTool.service.terminal;

import com.aizuda.easyManagerTool.domain.dto.terminal.SSHInfoDTO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import org.springframework.web.socket.WebSocketSession;

import java.util.function.BiConsumer;

public interface ScriptService {

    String execScript(String topic, SettingUserVO user, SSHInfoDTO sshInfoDTO,String shPath,String parameter,BiConsumer<String, WebSocketSession> callBack);

//    String execScript(String topic, SettingUserVO user, SSHInfoDTO sshInfoDTO,String shPath,String parameter);
//
//    String execScript(String topic, SettingUserVO user, SSHInfoDTO sshInfoDTO,String command,BiConsumer<String, WebSocketSession> callBack);

    String execScript(String topic, SettingUserVO user, SSHInfoDTO sshInfoDTO,String command);
}
