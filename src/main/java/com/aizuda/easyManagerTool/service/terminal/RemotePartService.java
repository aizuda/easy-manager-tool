package com.aizuda.easyManagerTool.service.terminal;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easy.security.exp.impl.BasicException;
import com.aizuda.easyManagerTool.domain.dto.terminal.RemotePartDTO;
import com.aizuda.easyManagerTool.domain.vo.server.ServerCompleteVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface RemotePartService {


    Rep<List<ServerCompleteVO>> index(Req<RemotePartDTO, SettingUserVO> requestData) throws BasicException;

    Rep<String> upload(String token,String path,MultipartFile file);
}
