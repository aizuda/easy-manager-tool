package com.aizuda.easyManagerTool.service.socket;

import com.aizuda.easyManagerTool.domain.dto.socket.SocketMessageDTO;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

public interface SocketEventService {

    /**
     * 连接成功
     * @param token 标识
     * @param session session
     * @throws IOException
     */
    default void onConnect(String token, WebSocketSession session) throws IOException {
    }

    /**
     * 接收消息
     * @param token 标识
     * @param session session
     * @param message 接收内容
     * @throws IOException
     */
    void onMessage(String token, WebSocketSession session, SocketMessageDTO message) throws IOException;

    /**
     * 关闭连接
     * @param token 标识
     * @param session session
     * @param closeStatus 关闭状态
     * @throws IOException
     */
    default void onClose(String token, WebSocketSession session, CloseStatus closeStatus) throws IOException {

    };

}
