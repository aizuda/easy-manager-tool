package com.aizuda.easyManagerTool.service.socket.impl;


import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class SocketSessionManager {


    /**
     * 保存连接 session 的地方
     */
    private static final Map<Object, WebSocketSession> SESSION_POOL = new ConcurrentHashMap<>();

    /**
     * 添加 session
     * @param key
     */
    public static void add(Object key, WebSocketSession session) {
        // 添加 session
        SESSION_POOL.put(key, session);
    }

    /**
     * 删除 session,会返回删除的 session
     * @param token
     * @return
     */
    public static WebSocketSession remove(Object token) {
        // 删除 session
        return SESSION_POOL.remove(token);
    }

    /**
     * 获得 session
     * @param token
     * @return
     */
    public static WebSocketSession get(Object token) {
        return SESSION_POOL.get(token);
    }


    public static void sendToUser(WebSocketSession session,String json) throws IOException {
        if(!session.isOpen()){
            return;
        }
        sendMessage(session,json);
    }

    public static void sendToUser(Object token,String json) throws IOException {
        WebSocketSession webSocketSession = get(token);
        if(!webSocketSession.isOpen()){
            return;
        }
        sendMessage(webSocketSession,json);
    }

    public static void sendToUser(String token,String json) throws IOException {
        WebSocketSession webSocketSession = get(token);
        if(!webSocketSession.isOpen()){
            return;
        }
        sendMessage(webSocketSession,json);
    }

    public static void sendToTenant(String token,String json) throws IOException {
        Set<Object> set = SESSION_POOL.keySet();
        Iterator<Object> iterator = set.iterator();
        while (iterator.hasNext()){
            Object key = iterator.next();
            WebSocketSession webSocketSession = get(key);
            if(!webSocketSession.isOpen()){
                continue;
            }
            if(key instanceof SettingUserVO){
                Integer tenantId = ((SettingUserVO) key).getTenantId();
                if(tenantId.toString().equals(token)){
                    sendMessage(webSocketSession,json);
                }
            }
            // User:tenantId:业务:时间戳
            if(key instanceof String){
                if(key.toString().contains(token)){
                    sendMessage(webSocketSession,json);
                }
            }
        }
    }

    public static void sendToAll(String json) throws IOException {
        Set<Object> set =  SESSION_POOL.keySet();
        Iterator<Object> iterator = set.iterator();
        while (iterator.hasNext()){
            Object key = iterator.next();
            WebSocketSession webSocketSession = get(key);
            if(!webSocketSession.isOpen()){
                continue;
            }
            sendMessage(webSocketSession,json);
        }
    }

    private static void sendMessage(WebSocketSession webSocketSession, String json) throws IOException {
        synchronized (webSocketSession) {
            webSocketSession.sendMessage(new TextMessage(json));
        }
    }

}
