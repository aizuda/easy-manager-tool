package com.aizuda.easyManagerTool.service.server;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.server.ServerScriptFindDTO;
import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptCommandEntity;
import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptLogEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.server.ServerScriptFindVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

import java.util.List;
import java.util.Map;

public interface ServerScriptService {

    Rep<PageVO<ServerScriptFindVO>> find(Req<PageDTO<ServerScriptFindDTO>, SettingUserVO> req);

    Rep<ServerScriptFindVO> edit(Req<ServerScriptFindDTO, SettingUserVO> req);

    Rep<ServerScriptFindVO> del(Req<ServerScriptFindDTO, SettingUserVO> req);

    Rep<List<ServerScriptCommandEntity>> commandFind(Req<ServerScriptFindDTO, SettingUserVO> req);

    Rep<ServerScriptCommandEntity> commandDel(Req<ServerScriptCommandEntity, SettingUserVO> req);

    Rep<ServerScriptFindVO> exec(Req<ServerScriptFindDTO, SettingUserVO> req);

    Rep<ServerScriptFindVO> hook(String hookId,Map<String, Object> map);

    Rep<List<ServerScriptLogEntity>> logDel(Req<ServerScriptLogEntity, SettingUserVO> req);

    Rep<List<ServerScriptLogEntity>> logFind(Req<ServerScriptLogEntity, SettingUserVO> req);
}
