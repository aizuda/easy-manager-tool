package com.aizuda.easyManagerTool.service.server;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.server.ServerPKEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

import java.util.List;

public interface ServerPKService {

    Rep<List<ServerPKEntity>> find(Req<ServerPKEntity, SettingUserVO> req);

    Rep<ServerPKEntity> edit(Req<ServerPKEntity, SettingUserVO> req);

    Rep<ServerPKEntity> del(Req<ServerPKEntity, SettingUserVO> req);
}
