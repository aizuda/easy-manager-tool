package com.aizuda.easyManagerTool.service.server.impl;

import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptLogEntity;
import com.aizuda.easyManagerTool.mapper.server.ServerScriptLogMapper;
import com.aizuda.easyManagerTool.service.server.ServerScriptLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ServerScriptLogServiceImpl extends ServiceImpl<ServerScriptLogMapper, ServerScriptLogEntity> implements ServerScriptLogService {

    @Resource
    private ServerScriptLogMapper serverScriptLogMapper;

    @Override
    public ServerScriptLogMapper mapper() {
        return serverScriptLogMapper;
    }

}
