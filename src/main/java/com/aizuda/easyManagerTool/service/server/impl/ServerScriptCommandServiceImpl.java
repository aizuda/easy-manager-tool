package com.aizuda.easyManagerTool.service.server.impl;

import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptCommandEntity;
import com.aizuda.easyManagerTool.mapper.server.ServerScriptCommandMapper;
import com.aizuda.easyManagerTool.service.server.ServerScriptCommandService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ServerScriptCommandServiceImpl  extends ServiceImpl<ServerScriptCommandMapper, ServerScriptCommandEntity> implements ServerScriptCommandService {

    @Resource
    private ServerScriptCommandMapper serverScriptCommandMapper;

    @Override
    public void saveOrUpdates(List<ServerScriptCommandEntity> commands) {
        saveOrUpdateBatch(commands);
    }

    @Override
    public ServerScriptCommandMapper mapper() {
        return serverScriptCommandMapper;
    }

}
