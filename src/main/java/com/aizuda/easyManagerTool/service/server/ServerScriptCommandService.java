package com.aizuda.easyManagerTool.service.server;

import com.aizuda.easyManagerTool.domain.entity.server.ServerScriptCommandEntity;
import com.aizuda.easyManagerTool.mapper.server.ServerScriptCommandMapper;

import java.util.List;

public interface ServerScriptCommandService {

    void saveOrUpdates(List<ServerScriptCommandEntity> commands);

    ServerScriptCommandMapper mapper();

}
