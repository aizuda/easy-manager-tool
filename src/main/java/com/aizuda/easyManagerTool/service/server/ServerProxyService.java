package com.aizuda.easyManagerTool.service.server;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.server.ServerProxyEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

import java.util.List;

public interface ServerProxyService {

    Rep<List<ServerProxyEntity>> find(Req<ServerProxyEntity, SettingUserVO> req);

    Rep<ServerProxyEntity> edit(Req<ServerProxyEntity, SettingUserVO> req);

    Rep<ServerProxyEntity> del(Req<ServerProxyEntity, SettingUserVO> req);
}
