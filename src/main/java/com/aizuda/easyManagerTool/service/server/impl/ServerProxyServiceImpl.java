package com.aizuda.easyManagerTool.service.server.impl;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.server.ServerProxyEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.server.ServerMapper;
import com.aizuda.easyManagerTool.mapper.server.ServerProxyMapper;
import com.aizuda.easyManagerTool.service.server.ServerProxyService;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ServerProxyServiceImpl extends ServiceImpl<ServerProxyMapper, ServerProxyEntity> implements ServerProxyService {

    @Resource
    ServerProxyMapper serverProxyMapper;
    @Resource
    ServerMapper serverMapper;

    @Override
    public Rep<List<ServerProxyEntity>> find(Req<ServerProxyEntity, SettingUserVO> req) {
        List<ServerProxyEntity> serverProxyEntities = serverProxyMapper.findAll();
        return Rep.ok(serverProxyEntities);
    }

    @Override
    public Rep<ServerProxyEntity> edit(Req<ServerProxyEntity, SettingUserVO> req) {
        ServerProxyEntity data = req.getData();
        AssertUtil.objIsNull(data.getProxyMode(),"请选择代理类型");
        AssertUtil.objIsNull(data.getProxyAccount(),"请填写代理账号");
        AssertUtil.objIsNull(data.getProxyHost(),"请选择代理IP");
        AssertUtil.objIsNull(data.getProxyPort(),"请选择代理端口");
        AssertUtil.objIsNull(data.getProxyPassword(),"请选择代理密码");
        saveOrUpdate(data);
        return Rep.ok(data);
    }

    @Override
    public Rep<ServerProxyEntity> del(Req<ServerProxyEntity, SettingUserVO> req) {
        ServerProxyEntity data = req.getData();
        AssertUtil.objIsNull(data.getId(), "数据错误");
        serverProxyMapper.deleteById(data);
        serverMapper.updateByProxyId(data.getId());
        return Rep.ok();
    }
}
