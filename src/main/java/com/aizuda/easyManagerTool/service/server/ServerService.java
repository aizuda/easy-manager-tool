package com.aizuda.easyManagerTool.service.server;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.server.ServerEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.server.ServerVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

import java.util.List;

public interface ServerService {

    Rep<PageVO<ServerVO>> find(Req<PageDTO<ServerEntity>, SettingUserVO> req);

    Rep<ServerEntity> edit(Req<ServerEntity, SettingUserVO> req);

    Rep<ServerEntity> del(Req<ServerEntity, SettingUserVO> req);

    Rep<ServerEntity> findById(Req<ServerEntity, SettingUserVO> req);

    Rep<List<ServerEntity>> findAll(Req<ServerEntity, SettingUserVO> req);

    Object monitor(Integer id);

    Rep<ServerEntity> addMonitorData(Req<ServerEntity, SettingUserVO> req);
}
