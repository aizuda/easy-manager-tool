package com.aizuda.easyManagerTool.service.server.impl;

import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.server.ServerEntity;
import com.aizuda.easyManagerTool.domain.entity.server.ServerLogEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.server.ServerLogMapper;
import com.aizuda.easyManagerTool.service.server.ServerLogService;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ServerLogServiceImpl implements ServerLogService {

    @Resource
    ServerLogMapper serverLogMapper;

    @Override
    public Rep<PageVO<ServerLogEntity>> find(Req<PageDTO<ServerLogEntity>, SettingUserVO> req) {
        PageDTO<ServerLogEntity> pageDTO = req.getData();
        SettingUserVO user = req.getUser();
        Page<ServerEntity> page = new Page<ServerEntity>(pageDTO.getCurrent(), pageDTO.getSize());
        // 查询 唯一条件是要根据 权限查询
        ServerLogEntity log = pageDTO.getData() ;
        IPage<ServerLogEntity> serverListEntrtyIPage = serverLogMapper.find(page, ObjectUtil.isEmpty(log)? new ServerLogEntity():log,user.getRoleId().toString());
        PageVO<ServerLogEntity> pageVO = new PageVO<ServerLogEntity>(pageDTO)
                .setTotal(serverListEntrtyIPage.getTotal())
                .setRecords(serverListEntrtyIPage.getRecords());
        return Rep.ok(pageVO);
    }

    @Override
    public void insert(ServerLogEntity serverLogEntity) {
        serverLogMapper.insert(serverLogEntity);
    }

    @Override
    public Rep<ServerLogEntity> del(Req<ServerLogEntity,SettingUserVO> req) {
        ServerLogEntity data = req.getData();
        AssertUtil.objIsNull(data.getId(),"数据错误");
        serverLogMapper.deleteById(data.getId());
        return Rep.ok();
    }
}
