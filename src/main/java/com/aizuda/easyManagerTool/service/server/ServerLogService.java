package com.aizuda.easyManagerTool.service.server;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.server.ServerLogEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

public interface ServerLogService {

    Rep<PageVO<ServerLogEntity>> find(Req<PageDTO<ServerLogEntity>, SettingUserVO> req);

    void insert(ServerLogEntity serverLogEntity);

    Rep<ServerLogEntity> del(Req<ServerLogEntity, SettingUserVO> req);
}
