package com.aizuda.easyManagerTool.service.server.impl;

import cn.hutool.core.util.StrUtil;
import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.server.ServerPKEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.server.ServerMapper;
import com.aizuda.easyManagerTool.mapper.server.ServerPKMapper;
import com.aizuda.easyManagerTool.service.server.ServerPKService;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ServerPKServiceImpl extends ServiceImpl<ServerPKMapper, ServerPKEntity> implements ServerPKService {

    @Resource
    ServerPKMapper serverPKMapper;
    @Resource
    ServerMapper serverMapper;

    @Override
    public Rep<List<ServerPKEntity>> find(Req<ServerPKEntity, SettingUserVO> req) {
        ServerPKEntity data = req.getData();
        AssertUtil.objIsNull(data.getServerId(),"数据错误");
        List<ServerPKEntity> serverPKEntities = serverPKMapper.findByAll(data.getServerId());
        return Rep.ok(serverPKEntities);
    }

    @Override
    public Rep<ServerPKEntity> edit(Req<ServerPKEntity, SettingUserVO> req) {
        ServerPKEntity data = req.getData();
        AssertUtil.objIsNull(data.getPkName(),"数据错误");
        if(StrUtil.isEmpty(data.getPkPath()) && StrUtil.isEmpty(data.getPkContent())){
            return Rep.error(500,"请上传私钥或填写私钥");
        }
        save(data);
        return Rep.ok();
    }

    @Override
    public Rep<ServerPKEntity> del(Req<ServerPKEntity, SettingUserVO> req) {
        ServerPKEntity data = req.getData();
        AssertUtil.objIsNull(data.getId(),"数据错误");
        serverPKMapper.deleteById(data.getId());
        serverMapper.updatePK(data.getServerId(),data.getId());
        return Rep.ok();
    }
}
