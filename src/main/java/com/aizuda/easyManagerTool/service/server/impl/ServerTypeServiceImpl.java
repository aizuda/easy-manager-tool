package com.aizuda.easyManagerTool.service.server.impl;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.server.ServerSettingDTO;
import com.aizuda.easyManagerTool.domain.entity.server.ServerTypeEntity;
import com.aizuda.easyManagerTool.domain.vo.server.ServerSettingVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.server.ServerTypeMapper;
import com.aizuda.easyManagerTool.service.server.ServerSettingService;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("type")
public class ServerTypeServiceImpl extends ServiceImpl<ServerTypeMapper,ServerTypeEntity> implements ServerSettingService<ServerTypeEntity> {

    @Resource
    ServerTypeMapper serverTypeMapper;

    @Override
    public Rep<List> find(Req<ServerTypeEntity, SettingUserVO> req) {
        List<ServerSettingVO> serverTypeMappers =  serverTypeMapper.findAll();
        return Rep.ok(serverTypeMappers);
    }

    @Override
    public Rep<ServerTypeEntity> del(Req<ServerSettingDTO, SettingUserVO> req) {
        ServerSettingDTO data = req.getData();
        AssertUtil.objIsNull(data.getId(),"id不能为空");
        serverTypeMapper.delTrue(data.getId());
        return Rep.ok();
    }

    @Override
    public Rep<ServerTypeEntity> edit(Req<ServerSettingDTO, SettingUserVO> req) {
        ServerSettingDTO data = req.getData();
        AssertUtil.objIsNull(data.getSettingName(),"名称不能为空");
        ServerTypeEntity serverTypeEntity = new ServerTypeEntity();
        serverTypeEntity.setId(data.getId());
        serverTypeEntity.setTypeName(data.getSettingName());
        saveOrUpdate(serverTypeEntity);
        return Rep.ok();
    }

    @Override
    public Rep<List> findByRole(SettingUserVO settingUserVO) {
        List<ServerSettingVO> serverTypeMappers =  serverTypeMapper.findAll();
        return Rep.ok(serverTypeMappers);
    }
}
