package com.aizuda.easyManagerTool.service.server;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.server.ServerSettingDTO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

import java.util.List;

public interface ServerSettingService<T> {

    Rep<List> find(Req<T, SettingUserVO> req);

    Rep<T> del(Req<ServerSettingDTO, SettingUserVO> req);

    Rep<T> edit(Req<ServerSettingDTO, SettingUserVO> req);

    Rep<List> findByRole(SettingUserVO settingUserVO);
}
