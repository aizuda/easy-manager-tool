package com.aizuda.easyManagerTool.service.server;

import com.aizuda.easyManagerTool.mapper.server.ServerScriptLogMapper;

public interface ServerScriptLogService {

    ServerScriptLogMapper mapper();
}
