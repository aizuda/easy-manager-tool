package com.aizuda.easyManagerTool.service.server.impl;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.server.ServerSettingDTO;
import com.aizuda.easyManagerTool.domain.entity.server.ServerGroupEntity;
import com.aizuda.easyManagerTool.domain.vo.server.ServerSettingVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.server.ServerGroupMapper;
import com.aizuda.easyManagerTool.service.server.ServerSettingService;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("group")
public class ServerGroupServiceImpl extends ServiceImpl<ServerGroupMapper,ServerGroupEntity> implements ServerSettingService<ServerGroupEntity> {

    @Resource
    ServerGroupMapper serverGroupMapper;

    @Override
    public Rep<List> find(Req<ServerGroupEntity, SettingUserVO> req) {
        List<ServerSettingVO> serverGroupEntities = serverGroupMapper.findAll();
        return Rep.ok(serverGroupEntities);
    }

    @Override
    public Rep<ServerGroupEntity> del(Req<ServerSettingDTO, SettingUserVO> req) {
        ServerSettingDTO data = req.getData();
        AssertUtil.objIsNull(data.getId(),"id不能为空");
        serverGroupMapper.deleTrue(data.getId());
        return Rep.ok();
    }

    @Override
    public Rep<ServerGroupEntity> edit(Req<ServerSettingDTO, SettingUserVO> req) {
        ServerSettingDTO data = req.getData();
        AssertUtil.objIsNull(data.getSettingName(),"名称不能为空");
        AssertUtil.List.isEmpty(data.getSettingRoles(),"角色不能为空");
        ServerGroupEntity serverGroupEntity = new ServerGroupEntity();
        serverGroupEntity.setId(data.getId());
        serverGroupEntity.setGroupName(data.getSettingName());
        serverGroupEntity.setRoleId(String.join(",",data.getSettingRoles()));
        saveOrUpdate(serverGroupEntity);
        return Rep.ok();
    }

    @Override
    public Rep<List> findByRole(SettingUserVO settingUserVO) {
        Integer roleId = settingUserVO.getRoleId();
        List<ServerSettingVO> serverGroupEntities = serverGroupMapper.findByRoleId(roleId.toString());
        return Rep.ok(serverGroupEntities);
    }


}
