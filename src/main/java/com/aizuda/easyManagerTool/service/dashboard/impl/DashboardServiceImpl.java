package com.aizuda.easyManagerTool.service.dashboard.impl;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.vo.dashboard.DashboardFind15DayList;
import com.aizuda.easyManagerTool.domain.vo.dashboard.DashboardIndexVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.monitor.MonitorAlarmMapper;
import com.aizuda.easyManagerTool.mapper.monitor.MonitorDataMapper;
import com.aizuda.easyManagerTool.mapper.server.ServerMapper;
import com.aizuda.easyManagerTool.service.dashboard.DashboardService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class DashboardServiceImpl implements DashboardService {

    @Resource
    private ServerMapper serverMapper;
    @Resource
    private MonitorDataMapper monitorDataMapper;
    @Resource
    private MonitorAlarmMapper monitorAlarmMapper;

    @Override
    public Rep<DashboardIndexVO> index(Req<Object, SettingUserVO> req) {
        DashboardIndexVO dashboardIndexVO = new DashboardIndexVO();
        try {
            SettingUserVO user = req.getUser();
            List<CompletableFuture<?>> completableFutures = Arrays.asList(
                    CompletableFuture.supplyAsync(() -> serverMapper.findTotal(user.getTenantId())),
                    CompletableFuture.supplyAsync(() -> monitorDataMapper.findTotal(user.getTenantId())),
                    CompletableFuture.supplyAsync(() -> monitorAlarmMapper.findTotal(user.getTenantId())),
                    CompletableFuture.supplyAsync(() -> monitorAlarmMapper.find15DayList(user.getTenantId()))
            );
            dashboardIndexVO.setServerTotal(Integer.valueOf(completableFutures.get(0).get().toString()));
            dashboardIndexVO.setMonitorDataTotal(Integer.valueOf(completableFutures.get(1).get().toString()));
            dashboardIndexVO.setMonitorAlarmTotal(Integer.valueOf(completableFutures.get(2).get().toString()));
            dashboardIndexVO.setMonitorAlarmList((List<DashboardFind15DayList>) completableFutures.get(3).get());
        }catch (Exception e){
            e.printStackTrace();
        }
        return Rep.ok(dashboardIndexVO);
    }
}
