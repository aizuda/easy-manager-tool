package com.aizuda.easyManagerTool.service.dashboard;


import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.vo.dashboard.DashboardIndexVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

public interface DashboardService {
    Rep<DashboardIndexVO> index(Req<Object, SettingUserVO> req);
}
