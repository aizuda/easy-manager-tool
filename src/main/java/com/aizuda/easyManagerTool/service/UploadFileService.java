package com.aizuda.easyManagerTool.service;

import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface UploadFileService {

    boolean exec(String path, MultipartFile file, Map<String, String[]> map, SettingUserVO settingUserVO);

}
