package com.aizuda.easyManagerTool.service.dbc.db;

public interface DBPublicService<T> {

    void insertOrUpdate(T t);

}
