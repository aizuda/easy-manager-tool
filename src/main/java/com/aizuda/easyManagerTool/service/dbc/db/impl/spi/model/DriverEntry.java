
package com.aizuda.easyManagerTool.service.dbc.db.impl.spi.model;

import com.aizuda.easyManagerTool.service.dbc.db.impl.spi.config.DriverConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.sql.Driver;

/**
 * @author jipengfei
 * @version : DriverEntry.java
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class DriverEntry {

    private DriverConfig driverConfig;

    private Driver driver;

}