package com.aizuda.easyManagerTool.service.dbc.db.impl;

import cn.hutool.core.collection.CollUtil;
import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBEnvEntity;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBPropertiesEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.dbc.db.DBPropertiesMapper;
import com.aizuda.easyManagerTool.service.dbc.db.DBPropertiesService;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DBPropertiesServiceImpl extends ServiceImpl<DBPropertiesMapper, DBPropertiesEntity> implements DBPropertiesService<DBPropertiesEntity> {

    @Resource
    DBPropertiesMapper dbPropertiesMapper;

    @Override
    public void insertOrUpdate(DBPropertiesEntity dbPropertiesEntity) {
        // 按照name查找
        saveOrUpdate(dbPropertiesEntity);
    }

    @Override
    public void insertBatchOrUpdateBatch(List<DBPropertiesEntity> list) {
        if(CollUtil.isNotEmpty(list)) {
            saveOrUpdateBatch(list);
        }
    }

    @Override
    public Rep<List<DBPropertiesEntity>> find(Req<Integer, SettingUserVO> req) {
        Integer data = req.getData();
        List<DBPropertiesEntity> list = dbPropertiesMapper.find(data);
        return Rep.ok(list);
    }

    @Override
    public Rep<List<DBEnvEntity>> del(Req<DBPropertiesEntity, SettingUserVO> req) {
        DBPropertiesEntity data = req.getData();
        AssertUtil.objIsNull(data.getId(), "数据错误");
        AssertUtil.objIsNull(data.getDbId(), "数据错误");
        dbPropertiesMapper.del(data.getId(),data.getDbId());
        return Rep.ok();
    }

}
