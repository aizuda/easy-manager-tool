package com.aizuda.easyManagerTool.service.dbc.db.impl;

import cn.hutool.core.bean.BeanUtil;
import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBDriverEntity;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBTypeEntity;
import com.aizuda.easyManagerTool.domain.vo.dbc.db.DBTypeVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.dbc.db.DBDriverMapper;
import com.aizuda.easyManagerTool.mapper.dbc.db.DBTypeMapper;
import com.aizuda.easyManagerTool.service.dbc.db.DBTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DBTypeServiceImpl extends ServiceImpl<DBTypeMapper, DBTypeEntity> implements DBTypeService {

    @Resource
    private DBTypeMapper dbTypeMapper;
    @Resource
    private DBDriverMapper dbDriverMapper;

    @Override
    public Rep<List<DBTypeVO>> find(Req<Object, SettingUserVO> req) {
        List<DBTypeEntity> typeList = dbTypeMapper.findAll();
        List<DBDriverEntity> driverList = dbDriverMapper.findAll();
        Map<Integer, List<DBDriverEntity>> collect = driverList.stream().collect(Collectors.groupingBy(DBDriverEntity::getTypeId));
        List<DBTypeVO> list = typeList.stream().map(i -> {
            DBTypeVO dbTypeVO = new DBTypeVO();
            BeanUtil.copyProperties(i, dbTypeVO);
            dbTypeVO.setDriverList(collect.get(i.getId()));
            return dbTypeVO;
        }).collect(Collectors.toList());
        return Rep.ok(list);
    }

}
