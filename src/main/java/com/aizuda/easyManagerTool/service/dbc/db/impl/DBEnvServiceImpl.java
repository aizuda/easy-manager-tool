package com.aizuda.easyManagerTool.service.dbc.db.impl;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBEnvEntity;
import com.aizuda.easyManagerTool.mapper.dbc.db.DBEnvMapper;
import com.aizuda.easyManagerTool.service.dbc.db.DBEnvService;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DBEnvServiceImpl extends ServiceImpl<DBEnvMapper, DBEnvEntity> implements DBEnvService<DBEnvEntity> {

    @Resource
    private DBEnvMapper dbEnvMapper;

    @Override
    public void insertOrUpdate(DBEnvEntity dbEnvEntity) {
        AssertUtil.objIsNull(dbEnvEntity.getEnvName(), "请选择或创建环境");
        saveOrUpdate(dbEnvEntity);
    }

    @Override
    public Rep<List<DBEnvEntity>> find() {
        List<DBEnvEntity> list = dbEnvMapper.findAll();
        return Rep.ok(list);
    }

}
