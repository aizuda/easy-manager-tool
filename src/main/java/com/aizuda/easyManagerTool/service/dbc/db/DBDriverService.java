package com.aizuda.easyManagerTool.service.dbc.db;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

public interface DBDriverService<DBDriverEntity> extends DBPublicService<DBDriverEntity>{

    // 删除
    Rep del(Req<DBDriverEntity, SettingUserVO> req);

}
