package com.aizuda.easyManagerTool.service.dbc.db.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.dbc.db.DBEditDTO;
import com.aizuda.easyManagerTool.domain.dto.terminal.SSHInfoDTO;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBDriverEntity;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBEntity;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBEnvEntity;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBPropertiesEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.dbc.db.DBListVO;
import com.aizuda.easyManagerTool.domain.vo.server.ServerCompleteVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.dbc.db.DBMapper;
import com.aizuda.easyManagerTool.mapper.server.ServerMapper;
import com.aizuda.easyManagerTool.service.dbc.db.DBDriverService;
import com.aizuda.easyManagerTool.service.dbc.db.DBEnvService;
import com.aizuda.easyManagerTool.service.dbc.db.DBPropertiesService;
import com.aizuda.easyManagerTool.service.dbc.db.DBService;
import com.aizuda.easyManagerTool.service.dbc.db.impl.spi.util.JdbcUtils;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DBServiceImpl extends ServiceImpl<DBMapper, DBEntity> implements DBService {

    @Resource
    private DBMapper dbMapper;
    @Resource
    private DBDriverService dbDriverService;
    @Resource
    private DBEnvService dbEnvService;
    @Resource
    private DBPropertiesService dbPropertiesService;
    @Resource
    private ServerMapper serverMapper;

    @Override
    public Rep<PageVO<DBListVO>> find(Req<PageDTO<DBEditDTO>, SettingUserVO> req) {
        PageDTO<DBEditDTO> pageDTO = req.getData();
        SettingUserVO user = req.getUser();
        Page<DBEditDTO> page = new Page<DBEditDTO>(pageDTO.getCurrent(), pageDTO.getSize(),false);
        // 查询 唯一条件是要根据 权限查询
        DBEditDTO data = pageDTO.getData() ;
        data.setTenantId(user.getTenantId());
        long count = dbMapper.findCount();
        IPage<DBListVO> dbListVOIPage = dbMapper.find(page, ObjectUtil.isEmpty(data)? new DBEditDTO():data);
        PageVO<DBListVO> pageVO = new PageVO<DBListVO>(pageDTO)
                .setTotal(count)
                .setRecords(dbListVOIPage.getRecords());
        return Rep.ok(pageVO);
    }

    @Transactional
    @Override
    public Rep<DBListVO> edit(Req<DBEditDTO, SettingUserVO> req) {
        DBEditDTO data = req.getData();
        check(data);
        // 1. 添加驱动
        DBDriverEntity driver = data.getDriver();
        dbDriverService.insertOrUpdate(driver);
        // 2. 添加环境
        DBEnvEntity env = data.getEnv();
        dbEnvService.insertOrUpdate(env);
        // 3. 添加基本信息
        data.setDriverId(driver.getId());
        data.setEnvId(env.getId());
        saveOrUpdate(data);
        // 4. 添加高级设置
        List<DBPropertiesEntity> dbProperties = data.getDbProperties();
        dbProperties.stream().forEach(i -> i.setDbId(data.getId()));
        dbPropertiesService.insertBatchOrUpdateBatch(dbProperties);
        return Rep.ok();
    }

    @Override
    public Rep<DBListVO> del(Req<DBEditDTO, SettingUserVO> req) {
        DBEditDTO data = req.getData();
        AssertUtil.objIsNull(data.getId(),"数据错误");
        dbMapper.deleteById(data);
        return Rep.ok();
    }

    @Override
    public Rep<String> testCon(Req<DBEditDTO, SettingUserVO> req) {
        try {
            DBEditDTO data = req.getData();
            check(data);
            ServerCompleteVO byId = serverMapper.findById(data.getServerId());
            SSHInfoDTO sshInfoDTO = new SSHInfoDTO();
            BeanUtil.copyProperties(byId, sshInfoDTO);
            JdbcUtils.testConnect( data,sshInfoDTO , DBPropertiesEntity.toMap(data.getDbProperties()));
        }catch (Exception e){
            return Rep.error(500,e.getMessage());
        }
        return Rep.ok();
    }

    private void check(DBEditDTO data){
        AssertUtil.objIsNull(data.getDbName(),"请输入名称" );
        AssertUtil.objIsNull(data.getDbHosts(),"请输入主机" );
        AssertUtil.objIsNull(data.getDbPort(),"请输入端口" );
        AssertUtil.objIsNull(data.getDbUrl(),"请输入DB-URL" );
    }

}
