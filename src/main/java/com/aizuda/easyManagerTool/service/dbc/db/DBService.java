package com.aizuda.easyManagerTool.service.dbc.db;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.dbc.db.DBEditDTO;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.dbc.db.DBListVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

public interface DBService {

    Rep<PageVO<DBListVO>> find(Req<PageDTO<DBEditDTO>, SettingUserVO> req);

    Rep<DBListVO> edit(Req<DBEditDTO, SettingUserVO> req);

    Rep<DBListVO> del(Req<DBEditDTO, SettingUserVO> req);

    Rep<String> testCon(Req<DBEditDTO, SettingUserVO> req);
}
