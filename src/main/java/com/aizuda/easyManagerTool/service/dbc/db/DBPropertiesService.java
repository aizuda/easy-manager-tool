package com.aizuda.easyManagerTool.service.dbc.db;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBEnvEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

import java.util.List;

public interface DBPropertiesService<DBPropertiesEntity> extends DBPublicService<DBPropertiesEntity> {

    void insertBatchOrUpdateBatch(List<DBPropertiesEntity> list);

    Rep<List<DBPropertiesEntity>> find(Req<Integer, SettingUserVO> req);

    Rep<List<DBEnvEntity>> del(Req<DBPropertiesEntity, SettingUserVO> req);
}
