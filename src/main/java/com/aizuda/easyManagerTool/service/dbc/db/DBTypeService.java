package com.aizuda.easyManagerTool.service.dbc.db;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.vo.dbc.db.DBTypeVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

import java.util.List;

public interface DBTypeService {

    Rep<List<DBTypeVO>> find(Req<Object, SettingUserVO> req);

}
