
package com.aizuda.easyManagerTool.service.dbc.db.impl.spi.config;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

import java.util.List;

/**
 * @author jipengfei
 * @version : DriverConfig.java
 */
@Data
public class DriverConfig {
    /**
     * jdbcDriver
     */
    private String jdbcDriver;

    /**
     * jdbcDriverClass
     */
    private String jdbcDriverClass;

    ///**
    // * name
    // */
    //private String name;

    /**
     * downloadJdbcDriverUrls
     */
    private List<String> downloadJdbcDriverUrls;


    private String dbType;

    /**
     * 自定义
     */
    private boolean custom;


    public boolean notEmpty() {
        return StrUtil.isNotBlank(getJdbcDriver()) && StrUtil.isNotBlank(getJdbcDriverClass());
    }
}