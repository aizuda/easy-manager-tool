package com.aizuda.easyManagerTool.service.dbc.db;

import com.aizuda.easy.security.domain.Rep;

import java.util.List;

public interface DBEnvService<DBEnvEntity> extends DBPublicService<DBEnvEntity> {


    Rep<List<DBEnvEntity>> find();
}
