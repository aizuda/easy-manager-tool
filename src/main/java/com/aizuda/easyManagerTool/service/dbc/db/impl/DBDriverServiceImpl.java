package com.aizuda.easyManagerTool.service.dbc.db.impl;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.dbc.db.DBDriverEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.dbc.db.DBDriverMapper;
import com.aizuda.easyManagerTool.service.UploadFileService;
import com.aizuda.easyManagerTool.service.dbc.db.DBDriverService;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Map;

@Service("DBDriver")
public class DBDriverServiceImpl extends ServiceImpl<DBDriverMapper, DBDriverEntity> implements DBDriverService<DBDriverEntity>, UploadFileService {

    @Resource
    DBDriverMapper dbDriverMapper;

    @Override
    public void insertOrUpdate(DBDriverEntity dbDriverEntity) {
        AssertUtil.objIsNull(dbDriverEntity.getDriverName(), "请选择驱动");
        AssertUtil.objIsNull(dbDriverEntity.getDriverClassName(), "请选择驱动类名");
        saveOrUpdate(dbDriverEntity);
    }

    @Override
    public boolean exec(String path, MultipartFile file, Map<String, String[]> map,SettingUserVO settingUserVO) {
        AssertUtil.List.isEmpty(map.get("typeId"), "请选择数据库");
        AssertUtil.objIsNull(map.get("typeId")[0], "请选择数据库");
        DBDriverEntity dbDriverEntity = new DBDriverEntity();
        dbDriverEntity.setTypeId(Integer.valueOf(map.get("typeId")[0]));
        dbDriverEntity.setDriverName(file.getOriginalFilename());
        dbDriverEntity.setTenantId(settingUserVO.getTenantId());
        dbDriverMapper.insert(dbDriverEntity);
        return true;
    }

    @Override
    public Rep del(Req<DBDriverEntity, SettingUserVO> req) {
        DBDriverEntity data = req.getData();
        SettingUserVO user = req.getUser();
        AssertUtil.objIsNull(data.getId(),"数据错误");
        dbDriverMapper.deleteById(data.getId());
        return Rep.ok();
    }

}
