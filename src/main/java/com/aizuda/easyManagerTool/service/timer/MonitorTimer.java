package com.aizuda.easyManagerTool.service.timer;

import com.aizuda.easyManagerTool.service.monitor.MonitorDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@EnableScheduling
@Slf4j
public class MonitorTimer {

    @Resource
    private MonitorDataService monitorDataService;

    @Async("newAsyncExecutor")
    @Scheduled(cron = "0/5 * * * * ?")
    public void exec(){
        monitorDataService.exec();
    }

    @Async("newAsyncExecutor")
    @Scheduled(cron = "0/3 * * * * ?")
    public void pull(){
        monitorDataService.pull();
    }

}
