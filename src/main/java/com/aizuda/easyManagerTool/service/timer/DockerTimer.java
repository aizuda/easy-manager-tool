package com.aizuda.easyManagerTool.service.timer;

import com.aizuda.easyManagerTool.service.docker.DockerListServer;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@EnableScheduling
public class DockerTimer {

    @Resource
    private DockerListServer dockerListServer;

    @Async("newAsyncExecutor")
    @Scheduled(cron = "0/5 * * * * ?")
    public void pullData(){
        dockerListServer.refresh();
    }


}