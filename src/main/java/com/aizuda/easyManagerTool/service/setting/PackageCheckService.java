package com.aizuda.easyManagerTool.service.setting;

import com.aizuda.easy.security.exp.impl.BasicException;
import com.aizuda.easyManagerTool.domain.bo.PackagesBO;

public interface PackageCheckService {

    void packageCheck(PackagesBO.Item item,Integer tenantId) throws BasicException;

}
