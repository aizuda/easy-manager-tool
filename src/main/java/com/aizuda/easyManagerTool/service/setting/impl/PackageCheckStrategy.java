package com.aizuda.easyManagerTool.service.setting.impl;

import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easy.security.exp.impl.BasicException;
import com.aizuda.easyManagerTool.domain.bo.PackagesBO;
import com.aizuda.easyManagerTool.service.setting.PackageCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class PackageCheckStrategy {

    private Map<String, PackageCheckService> packageCheckServiceMap = new ConcurrentHashMap<>();

    @Autowired
    public PackageCheckStrategy(Map<String, PackageCheckService> packageCheckServiceMap){
        this.packageCheckServiceMap = packageCheckServiceMap;
    }

    public void check(PackagesBO.Item item,Integer tenantId) throws BasicException {
        if(ObjectUtil.isEmpty(item.getService())){
            return;
        }
        packageCheckServiceMap.get(item.getService()).packageCheck(item,tenantId);
    }

}
