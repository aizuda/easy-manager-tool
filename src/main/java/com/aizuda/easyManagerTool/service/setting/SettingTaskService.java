package com.aizuda.easyManagerTool.service.setting;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingTaskEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingTaskFindVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingTaskVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

public interface SettingTaskService {

    Rep<SettingTaskVO> find(Req<SettingTaskFindVO, SettingUserVO> req);

    Rep<SettingTaskEntity> edit(Req<SettingTaskEntity, SettingUserVO> req);

    Rep<SettingTaskEntity> del(Req<SettingTaskEntity, SettingUserVO> req);

}
