package com.aizuda.easyManagerTool.service.setting.impl.alarm;

import com.aizuda.easyManagerTool.domain.dto.setting.SettingAlarmTestDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingAlarmEntity;

public interface AlarmService {


    void send(String title,String text,SettingAlarmEntity settingAlarmEntity) throws Exception;

    void send(String title,String text, SettingAlarmTestDTO settingAlarmTestDTO);

}
