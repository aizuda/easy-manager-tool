package com.aizuda.easyManagerTool.service.setting.impl.alarm;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easyManagerTool.domain.dto.setting.SettingAlarmTestDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingAlarmEntity;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingUserEntity;
import com.aizuda.easyManagerTool.mapper.setting.SettingUserMapper;
import com.aizuda.easyManagerTool.util.AssertUtil;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service("mail")
public class AlarmMailService implements AlarmService{
    @Resource
    SettingUserMapper settingUserMapper;

    @Override
    public void send(String title,String text,SettingAlarmEntity settingAlarmEntity) {
        if(ObjectUtil.isEmpty(settingAlarmEntity.getMailHost()) ||
                ObjectUtil.isEmpty(settingAlarmEntity.getMailUserName()) ||
                ObjectUtil.isEmpty(settingAlarmEntity.getMailPort()) ||
                ObjectUtil.isEmpty(settingAlarmEntity.getMailPassword()) ||
                ObjectUtil.isEmpty(settingAlarmEntity.getMailNickName())
        ) {return;}

        JavaMailSender javaMailSender = setConfig(settingAlarmEntity);
        List<SettingUserEntity> mails = settingUserMapper.findTenantId(settingAlarmEntity.getTenantId());
        if(CollUtil.isEmpty(mails)){return;}
        List<String> collect = mails.stream().map(item -> item.getUserMail()).collect(Collectors.toList());
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject(title);
        message.setFrom(settingAlarmEntity.getMailNickName()+'<'+settingAlarmEntity.getMailUserName()+'>');
        message.setTo(collect.toArray(new String[mails.size()]));
        message.setSentDate(new Date());
        message.setText(text);
        javaMailSender.send(message);
    }

    @Override
    public void send(String title, String text, SettingAlarmTestDTO settingAlarmTestDTO) {
        AssertUtil.objIsNull(settingAlarmTestDTO.getRecipients(),"请填写接收人");
        AssertUtil.objIsNull(settingAlarmTestDTO.getMailHost(),"服务器地址不能为空");
        AssertUtil.objIsNull(settingAlarmTestDTO.getMailPort(),"服务器端口不能为空");
        AssertUtil.objIsNull(settingAlarmTestDTO.getMailUserName(),"发送者邮箱不能为空");
        AssertUtil.objIsNull(settingAlarmTestDTO.getMailPassword(),"收取按吗不能为空");
        AssertUtil.objIsNull(settingAlarmTestDTO.getMailNickName(),"发送者名称不能为空");
        JavaMailSender javaMailSender = setConfig(settingAlarmTestDTO);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject(title);
        message.setFrom(settingAlarmTestDTO.getMailNickName()+'<'+settingAlarmTestDTO.getMailUserName()+'>');
        message.setTo(settingAlarmTestDTO.getRecipients());
        message.setSentDate(new Date());
        message.setText(text);
        javaMailSender.send(message);
    }


    private JavaMailSender setConfig(SettingAlarmEntity settingAlarmEntity){
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setDefaultEncoding(CharsetUtil.UTF_8);
        javaMailSender.setHost(settingAlarmEntity.getMailHost());
        javaMailSender.setPort(settingAlarmEntity.getMailPort());
        javaMailSender.setUsername(settingAlarmEntity.getMailUserName());
        javaMailSender.setPassword(settingAlarmEntity.getMailPassword());
        return javaMailSender;
    }

}
