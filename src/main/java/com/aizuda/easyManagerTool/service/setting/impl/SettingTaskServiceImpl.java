package com.aizuda.easyManagerTool.service.setting.impl;


import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingTaskEntity;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingUserEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingTaskFindVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingTaskVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.setting.SettingTaskMapper;
import com.aizuda.easyManagerTool.mapper.setting.SettingUserMapper;
import com.aizuda.easyManagerTool.service.setting.SettingTaskService;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SettingTaskServiceImpl extends ServiceImpl<SettingTaskMapper, SettingTaskEntity> implements SettingTaskService {

    @Resource
    SettingTaskMapper settingTaskMapper;
    @Resource
    SettingUserMapper settingUserMapper;


    @Override
    public Rep<SettingTaskVO> find(Req<SettingTaskFindVO, SettingUserVO> req) {
        SettingTaskFindVO data = req.getData();
        SettingUserVO user = req.getUser();
        List<SettingTaskFindVO> list = settingTaskMapper.findAllTask(data, user.getId());
        Map<Integer, List<SettingTaskFindVO>> collect = list.stream().collect(Collectors.groupingBy(SettingTaskFindVO::getTaskState));
        SettingTaskVO settingTaskVO = new SettingTaskVO();
        settingTaskVO.setStays(collect.get(0));
        settingTaskVO.setExecutions(collect.get(1));
        settingTaskVO.setCompletes(collect.get(2));
        settingTaskVO.setCloses(collect.get(3));
        List<SettingUserEntity> settingUserEntities = settingUserMapper.findAll(user.getTenantId());
        settingTaskVO.setUsers(settingUserEntities);
        settingTaskVO.setUserId(user.getId());
        return Rep.ok(settingTaskVO);
    }

    @Override
    public Rep<SettingTaskEntity> edit(Req<SettingTaskEntity, SettingUserVO> req) {
        SettingTaskEntity data = req.getData();
        SettingUserVO user = req.getUser();
        if(ObjectUtil.isNotEmpty(data.getId())) {
            AssertUtil.objEQ(data.getUserId(), user.getId(), "不能修改其他人的任务");
        }
        AssertUtil.objIsNull(data.getTaskTitle(),"任务标题不能为空");
        AssertUtil.objIsNull(data.getTaskState(),"任务状态不嫩为空");
        AssertUtil.objIsNull(data.getTaskCoefficient(),"任务难度系数不能为空");
        AssertUtil.objIsNull(data.getTaskContent(),"任务能容不能为空");
        AssertUtil.objIsNull(data.getStartDate(),"任务开始日期不能为空");
        AssertUtil.objIsNull(data.getEndDate(),"任务结束日期不能为空");
        AssertUtil.Time.eqDate(data.getStartDate(),data.getEndDate(),"结束时间不能早于开始时间");
        data.setUserId(user.getId());
        saveOrUpdate(data);
        return Rep.ok(data);
    }

    @Override
    public Rep<SettingTaskEntity> del(Req<SettingTaskEntity, SettingUserVO> req) {
        SettingTaskEntity data = req.getData();
        SettingUserVO user = req.getUser();
        AssertUtil.objEQ(data.getUserId(), user.getId(), "不能删除其他人的任务");
        AssertUtil.objIsNull(data.getId(), "数据错误");
        settingTaskMapper.deleteById(data.getId());
        return Rep.ok();
    }

}
