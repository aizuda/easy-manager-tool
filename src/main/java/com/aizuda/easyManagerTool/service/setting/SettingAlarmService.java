package com.aizuda.easyManagerTool.service.setting;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.setting.SettingAlarmTestDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingAlarmEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

public interface SettingAlarmService {

    Rep<SettingAlarmEntity> find(Req<SettingAlarmEntity, SettingUserVO> req);

    Rep<SettingAlarmEntity> edit(Req<SettingAlarmEntity, SettingUserVO> req);

    Rep<SettingAlarmEntity> test(Req<SettingAlarmTestDTO, SettingUserVO> req);
}
