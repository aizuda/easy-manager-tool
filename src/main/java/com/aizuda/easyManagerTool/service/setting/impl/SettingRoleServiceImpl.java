package com.aizuda.easyManagerTool.service.setting.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingRoleEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingMenuListVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingMenuVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingRoleListVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.setting.SettingMenuMapper;
import com.aizuda.easyManagerTool.mapper.setting.SettingRoleMapper;
import com.aizuda.easyManagerTool.service.setting.SettingRoleService;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SettingRoleServiceImpl extends ServiceImpl<SettingRoleMapper, SettingRoleEntity> implements SettingRoleService {

    @Resource
    private SettingRoleMapper settingRoleMapper;
    @Resource
    private SettingMenuMapper settingMenuMapper;


    @Override
    public Rep<PageVO<SettingRoleEntity>> find(Req<PageDTO<SettingRoleEntity>, SettingUserVO> req) {
        PageDTO<SettingRoleEntity> data = req.getData();
        SettingUserVO user = req.getUser();
        Page<SettingRoleEntity> page = new Page<SettingRoleEntity>(data.getCurrent(), data.getSize());
        SettingRoleEntity settingRoleEntity = data.getData();
        settingRoleEntity = ObjectUtil.isEmpty(settingRoleEntity)? new SettingRoleEntity():settingRoleEntity;
        IPage<SettingRoleEntity> roles = settingRoleMapper.findAll(page, settingRoleEntity);
        PageVO<SettingRoleEntity> pageVO = new PageVO<SettingRoleEntity>(data)
                .setTotal(roles.getTotal())
                .setRecords(roles.getRecords());
        return Rep.ok(pageVO);
    }

    @Override
    public Rep<List<SettingRoleEntity>> findAll(Req<SettingRoleEntity, SettingUserVO> req) {
        List<SettingRoleEntity> list = settingRoleMapper.findList();
        if(CollUtil.isEmpty(list)){
            list = new ArrayList<>();
        }
        return Rep.ok(list);
    }

    @Override
    public Rep<SettingRoleEntity> edit(Req<SettingRoleEntity, SettingUserVO> req) {
        SettingUserVO user = req.getUser();
        SettingRoleEntity data = req.getData();
        AssertUtil.objIsNull(data.getRoleName(), "角色名称不能为空");
        saveOrUpdate(data);
        return Rep.ok();
    }

    @Override
    public Rep<SettingRoleEntity> del(Req<SettingRoleEntity, SettingUserVO> req) {
        SettingUserVO user = req.getUser();
        SettingRoleEntity data = req.getData();
        AssertUtil.objIsNull(data.getId(), "数据错误");
        settingRoleMapper.deleteById(data.getId());
        return Rep.ok();
    }

    @Override
    public Rep<SettingMenuVO> menu(Req<SettingRoleEntity, SettingUserVO> req) {
        SettingRoleEntity data = req.getData();
        List<SettingMenuListVO> allMenu = new ArrayList<>();
        List<SettingMenuListVO> settingMenuListVO = settingMenuMapper.findAll();
        if(CollUtil.isNotEmpty(settingMenuListVO)){
            parentMenu(settingMenuListVO,allMenu);
        }
        List<SettingMenuListVO> roleMenu = new ArrayList<>();
        if(StrUtil.isNotBlank(data.getMenuIds())){
            List<Integer> collect = Arrays.stream(data.getMenuIds().split(",")).map(Integer::valueOf).collect(Collectors.toList());
            List<SettingMenuListVO> settingMenuListVOS = settingMenuMapper.findMenuIds(collect);
            parentMenu(settingMenuListVOS,roleMenu);
        }
        SettingMenuVO settingMenuVO = new SettingMenuVO();
        settingMenuVO.setAllMenu(allMenu);
        settingMenuVO.setRoleMenu(roleMenu);
        return Rep.ok(settingMenuVO);
    }

    @Override
    public SettingRoleListVO findRoleId(Integer id) {
        SettingRoleEntity settingRoleEntity = settingRoleMapper.findById(id);
        SettingRoleListVO settingRoleListVO = BeanUtil.copyProperties(settingRoleEntity, SettingRoleListVO.class);
        AssertUtil.objIsNull(settingRoleEntity, "您还没有权限");
        List<Integer> collect = Arrays.stream(settingRoleEntity.getMenuIds().split(",")).map(Integer::valueOf).collect(Collectors.toList());
        List<SettingMenuListVO> settingMenuListVOS = settingMenuMapper.findMenuIds(collect);
        settingRoleListVO.setMenus(settingMenuListVOS);
        return settingRoleListVO;
    }

    private List<SettingMenuListVO> parentMenu(List<SettingMenuListVO> list, List<SettingMenuListVO> result){
        List<SettingMenuListVO> collect = list.stream().filter(i -> ObjectUtil.isEmpty(i.getMenuParentId())).collect(Collectors.toList());
        result.addAll(collect);
        result.forEach(i -> treeMenu(list,i));
        return result;
    }

    private void treeMenu(List<SettingMenuListVO> list, SettingMenuListVO vo){
        for (SettingMenuListVO settingMenuListVO : list) {
            if (ObjectUtil.isNotEmpty(settingMenuListVO.getMenuParentId()) && settingMenuListVO.getMenuParentId().equals(vo.getId())) {
                vo.getChildren().add(settingMenuListVO);
                treeMenu(list, settingMenuListVO);
            }
        }
    }


}
