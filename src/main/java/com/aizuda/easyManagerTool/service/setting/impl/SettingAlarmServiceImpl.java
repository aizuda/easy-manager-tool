package com.aizuda.easyManagerTool.service.setting.impl;

import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easy.security.exp.impl.BasicException;
import com.aizuda.easyManagerTool.domain.bo.PackagesBO;
import com.aizuda.easyManagerTool.domain.dto.setting.SettingAlarmTestDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingAlarmEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.setting.SettingAlarmMapper;
import com.aizuda.easyManagerTool.service.setting.PackageCheckService;
import com.aizuda.easyManagerTool.service.setting.SettingAlarmService;
import com.aizuda.easyManagerTool.service.setting.impl.alarm.AlarmStrategy;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("alarmNotice")
public class SettingAlarmServiceImpl extends ServiceImpl<SettingAlarmMapper, SettingAlarmEntity> implements SettingAlarmService, PackageCheckService {

    @Resource
    SettingAlarmMapper settingAlarmMapper;
    @Resource
    AlarmStrategy alarmStrategy;

    @Override
    public Rep<SettingAlarmEntity> find(Req<SettingAlarmEntity, SettingUserVO> req) {
        SettingAlarmEntity data = req.getData();
        SettingUserVO user = req.getUser();
        SettingAlarmEntity settingAlarmEntity = settingAlarmMapper.find();
        if(ObjectUtil.isEmpty(settingAlarmEntity)){
            settingAlarmEntity = new SettingAlarmEntity();
        }
        return Rep.ok(settingAlarmEntity);
    }

    @Override
    public Rep<SettingAlarmEntity> edit(Req<SettingAlarmEntity, SettingUserVO> req) {
        SettingAlarmEntity data = req.getData();
        saveOrUpdate(data);
        return Rep.ok(data);
    }

    @Override
    public Rep<SettingAlarmEntity> test(Req<SettingAlarmTestDTO, SettingUserVO> req) {
        SettingUserVO user = req.getUser();
        SettingAlarmTestDTO data = req.getData();
        alarmStrategy.send("Easy-Manager-Tool-测试邮件",
                "Easy-Manager-Tool-测试邮件", data);
        return Rep.ok();
    }

    @Override
    public void packageCheck(PackagesBO.Item item,Integer tenantId) throws BasicException {
        if (!item.getOpen()) {
            throw new BasicException(500,"当前套餐不支持此业务");
        }
    }
}
