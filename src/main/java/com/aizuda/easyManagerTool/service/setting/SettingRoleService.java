package com.aizuda.easyManagerTool.service.setting;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingRoleEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingMenuVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingRoleListVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

import java.util.List;

public interface SettingRoleService {

    Rep<PageVO<SettingRoleEntity>> find(Req<PageDTO<SettingRoleEntity>, SettingUserVO> req);

    Rep<List<SettingRoleEntity>> findAll(Req<SettingRoleEntity, SettingUserVO> req);

    Rep<SettingRoleEntity> edit(Req<SettingRoleEntity, SettingUserVO> req);

    Rep<SettingRoleEntity> del(Req<SettingRoleEntity, SettingUserVO> req);

    Rep<SettingMenuVO> menu(Req<SettingRoleEntity, SettingUserVO> req);

    SettingRoleListVO findRoleId(Integer id);

}
