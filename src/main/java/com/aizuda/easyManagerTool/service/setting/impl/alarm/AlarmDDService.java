package com.aizuda.easyManagerTool.service.setting.impl.alarm;

import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easyManagerTool.domain.dto.setting.SettingAlarmTestDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingAlarmEntity;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.aizuda.robot.autoconfigure.RobotProperties;
import com.aizuda.robot.message.DingTalkSendMessage;
import com.aizuda.robot.message.ISendMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service("dd")
public class AlarmDDService  implements AlarmService{

    @Override
    public void send(String title,String text,SettingAlarmEntity settingAlarmEntity) throws Exception {
        if(ObjectUtil.isEmpty(settingAlarmEntity.getDdAgentId()) ||
                ObjectUtil.isEmpty(settingAlarmEntity.getDdSecret()) ||
                ObjectUtil.isEmpty(settingAlarmEntity.getDdAppKey()) ||
                ObjectUtil.isEmpty(settingAlarmEntity.getDdAccessToken())
        ) {return;}
        RestTemplate restTemplate = new RestTemplate();
        RobotProperties properties = setDDConfig(settingAlarmEntity);
        ISendMessage message = new DingTalkSendMessage(properties, restTemplate);
        message.send(text);
    }

    @Override
    public void send(String title, String text, SettingAlarmTestDTO settingAlarmTestDTO) {
        try {
            AssertUtil.objIsNull(settingAlarmTestDTO.getDdAppKey(),"请填写AppKey");
            AssertUtil.objIsNull(settingAlarmTestDTO.getDdSecret(),"请填写Secret");
            AssertUtil.objIsNull(settingAlarmTestDTO.getDdAccessToken(),"请填写AccessToken");
            AssertUtil.objIsNull(settingAlarmTestDTO.getDdContent(),"请填写内容");
            SettingAlarmEntity settingAlarm = settingAlarmTestDTO;
            send(title,settingAlarmTestDTO.getDdContent(),settingAlarm);
        }catch (Exception e){
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private RobotProperties setDDConfig(SettingAlarmEntity settingAlarmEntity){
        RobotProperties properties = new RobotProperties();
        RobotProperties.DingTalk dingTalk = new RobotProperties.DingTalk();
        dingTalk.setAccessToken(settingAlarmEntity.getDdAccessToken());
        dingTalk.setSecret(settingAlarmEntity.getDdSecret());
        properties.setDingTalk(dingTalk);
        return properties;
    }

}
