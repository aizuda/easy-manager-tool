package com.aizuda.easyManagerTool.service.setting;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.setting.SettingUserLoginDTO;
import com.aizuda.easyManagerTool.domain.dto.setting.SettingUserRegisterDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingUserEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

import java.util.List;
import java.util.Map;

public interface SettingUserService {

    Rep<SettingUserVO> login(Req<SettingUserLoginDTO, SettingUserVO> req);

    Rep<SettingUserVO> register(Req<SettingUserRegisterDTO, SettingUserVO> req);

    Rep<PageVO<SettingUserEntity>> find(Req<PageDTO<SettingUserEntity>, SettingUserVO> req);

    Rep<SettingUserEntity> edit(Req<SettingUserRegisterDTO, SettingUserVO> req);

    Rep<SettingUserEntity> del(Req<SettingUserEntity, SettingUserVO> req);

    Map<String,Object> code();

    Rep<SettingUserEntity> editUser(Req<SettingUserEntity, SettingUserVO> req);

    Rep<SettingUserEntity> findUser(Req<SettingUserRegisterDTO, SettingUserVO> req);

    Rep<List<SettingUserEntity>> findAll(Req<SettingUserEntity, SettingUserVO> req);

}
