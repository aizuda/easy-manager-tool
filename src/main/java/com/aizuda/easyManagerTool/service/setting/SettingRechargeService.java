package com.aizuda.easyManagerTool.service.setting;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.bo.PackagesBO;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.setting.SettingRechargeRecordEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.PackageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

public interface SettingRechargeService {

    Rep<PageVO<SettingRechargeRecordEntity>> record(Req<PageDTO<SettingRechargeRecordEntity>, SettingUserVO> req);

    Rep<String> pay(Req<PackagesBO, SettingUserVO> req);

    Rep<PackageVO> packages(Req<Object, SettingUserVO> req);

    Rep<SettingRechargeRecordEntity> refreshResults(Req<SettingRechargeRecordEntity, SettingUserVO> req) throws Exception;

    PackagesBO findPackage(Integer tenantId);

    Rep<SettingRechargeRecordEntity> delRecode(Req<SettingRechargeRecordEntity, SettingUserVO> req);
}
