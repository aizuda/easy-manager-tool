package com.aizuda.easyManagerTool.service.monitor;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorDataEntity;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorDataIndexEntity;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorDataIndexItemEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.domain.vo.terminal.TreeIndexItemVO;

import java.util.List;

public interface MonitorDataIndexService {

    Rep<PageVO<MonitorDataIndexEntity>> find(Req<PageDTO<MonitorDataIndexEntity>, SettingUserVO> req);

    Rep<MonitorDataIndexEntity> edit(Req<MonitorDataIndexEntity, SettingUserVO> req);

    Rep<MonitorDataIndexEntity> del(Req<MonitorDataIndexEntity, SettingUserVO> req);

    Rep<MonitorDataIndexItemEntity> delIndexItem(Req<MonitorDataIndexItemEntity, SettingUserVO> req);

    Rep<List<MonitorDataIndexItemEntity>> findIndexItem(Req<MonitorDataIndexItemEntity, SettingUserVO> req);

    Rep<MonitorDataIndexItemEntity> editIndexItem(Req<MonitorDataIndexItemEntity, SettingUserVO> req);

    Rep<List<TreeIndexItemVO>> treeIndexItem(Req<MonitorDataEntity, SettingUserVO> req);

    Rep<MonitorDataIndexEntity> editIndexAlarm(Req<MonitorDataIndexEntity, SettingUserVO> req);
}
