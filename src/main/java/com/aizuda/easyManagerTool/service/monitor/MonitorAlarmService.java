package com.aizuda.easyManagerTool.service.monitor;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorAlarmEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.service.monitor.impl.MonitorListManager;

public interface MonitorAlarmService {

    Rep<PageVO<MonitorAlarmEntity>> find(Req<PageDTO<MonitorAlarmEntity>, SettingUserVO> req);

    Rep<MonitorAlarmEntity> edit(Req<MonitorAlarmEntity, SettingUserVO> req);

    Rep<MonitorAlarmEntity> del(Req<MonitorAlarmEntity, SettingUserVO> req);

    void save(MonitorListManager.MonitorData data);

}
