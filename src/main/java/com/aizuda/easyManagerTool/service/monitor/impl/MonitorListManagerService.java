package com.aizuda.easyManagerTool.service.monitor.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorUrlEntity;
import com.aizuda.easyManagerTool.domain.vo.terminal.TreeIndexItemSelectVO;
import com.aizuda.easyManagerTool.mapper.monitor.MonitorDataIndexMapper;
import com.aizuda.easyManagerTool.mapper.monitor.MonitorUrlMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class MonitorListManagerService implements CommandLineRunner{

    @Resource
    MonitorDataIndexMapper monitorDataIndexMapper;
    @Resource
    MonitorUrlMapper monitorUrlMapper;

    MonitorListManager.MonitorData monitorData = new MonitorListManager.MonitorData();

    MonitorListManager.MonitorDataIndex monitorDataIndex = new MonitorListManager.MonitorDataIndex();

    /**
     * 初始化
     */
    public void init(){
        List<TreeIndexItemSelectVO> treeIndexItemSelectVOS = monitorDataIndexMapper.selectAllIndex(null,null,null);
        update(treeIndexItemSelectVOS);
    }

    public void updateMonitorData(Integer id){
        List<TreeIndexItemSelectVO> treeIndexItemSelectVOS = monitorDataIndexMapper.selectAllIndex(id, null,null);
        update(treeIndexItemSelectVOS);
    }

    public void updateMonitorDataIndex(Integer id){
        List<TreeIndexItemSelectVO> treeIndexItemSelectVOS = monitorDataIndexMapper.selectAllIndex(null,null, id);
        update(treeIndexItemSelectVOS);
    }

    public void delMonitorDataIndex(Integer id){
        List<TreeIndexItemSelectVO> treeIndexItemSelectVOS = monitorDataIndexMapper.selectAllIndex(null, id,null);
        treeIndexItemSelectVOS.forEach(item -> MonitorListManager.removeIndex(item.getTmdiiId()));
    }

    private void update(List<TreeIndexItemSelectVO> treeIndexItemSelectVOS){
        treeIndexItemSelectVOS.forEach(item -> {
            if(ObjectUtil.isEmpty(item.getTmdiId())){
                return;
            }
            MonitorListManager.MonitorData md = BeanUtil.copyProperties(monitorData, MonitorListManager.MonitorData.class);
            md = MonitorListManager.getAndPut(item.getId(),md);
            md.setId(item.getTmdParentId());
            md.setTenantId(item.getTenantId());
            List<MonitorUrlEntity> monitorUrlEntities = monitorUrlMapper.selectByMdId(item.getTmdParentId());
            md.setUrls(monitorUrlEntities);

            MonitorListManager.MonitorDataIndex mdi = BeanUtil.copyProperties(monitorDataIndex, MonitorListManager.MonitorDataIndex.class);
            mdi.setId(item.getTmdiiId());
            mdi.setName(item.getMdiiName());
            mdi.setFullName(item.getMdTitle()+"_"+item.getMdiTitle()+"_"+item.getMdiiName());
            mdi.setExp(item.getMdiiExp());
            mdi.setFrequency(item.getMdiFrequency());
            mdi.setThreshold(item.getMdiThreshold());
            mdi.setMdiAlarm(item.getMdiAlarm());
            MonitorListManager.putIndex(item.getId(),mdi);
        });
    }

    @Override
    public void run(String... args) throws Exception {
        init();
    }
}
