package com.aizuda.easyManagerTool.service.monitor;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorScreenEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

public interface MonitorScreenService {

    Rep<PageVO<MonitorScreenEntity>> find(Req<PageDTO<MonitorScreenEntity>, SettingUserVO> req);

    Rep<MonitorScreenEntity> edit(Req<MonitorScreenEntity, SettingUserVO> req);

    Rep<MonitorScreenEntity> del(Req<MonitorScreenEntity, SettingUserVO> req);

    Rep<MonitorScreenEntity> findById(Req<MonitorScreenEntity, SettingUserVO> req);
}
