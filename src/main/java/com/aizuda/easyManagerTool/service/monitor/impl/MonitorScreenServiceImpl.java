package com.aizuda.easyManagerTool.service.monitor.impl;

import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorScreenEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.monitor.MonitorScreenMapper;
import com.aizuda.easyManagerTool.service.monitor.MonitorScreenService;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class MonitorScreenServiceImpl extends ServiceImpl<MonitorScreenMapper, MonitorScreenEntity> implements MonitorScreenService {

    @Resource
    MonitorScreenMapper monitorScreenMapper;


    @Override
    public Rep<PageVO<MonitorScreenEntity>> find(Req<PageDTO<MonitorScreenEntity>, SettingUserVO> req) {
        PageDTO<MonitorScreenEntity> pageDTO = req.getData();
        Page<MonitorScreenEntity> page = new Page<MonitorScreenEntity>(pageDTO.getCurrent(), pageDTO.getSize());
        // 查询 唯一条件是要根据 权限查询
        MonitorScreenEntity monitorScreenEntity = pageDTO.getData() ;
        IPage<MonitorScreenEntity> serverListEntrtyIPage = monitorScreenMapper.find(page, ObjectUtil.isEmpty(monitorScreenEntity)? new MonitorScreenEntity():monitorScreenEntity);
        PageVO<MonitorScreenEntity> pageVO = new PageVO<MonitorScreenEntity>(pageDTO)
                .setTotal(serverListEntrtyIPage.getTotal())
                .setRecords(serverListEntrtyIPage.getRecords());
        return Rep.ok(pageVO);
    }

    @Override
    public Rep<MonitorScreenEntity> edit(Req<MonitorScreenEntity, SettingUserVO> req) {
        MonitorScreenEntity data = req.getData();
        AssertUtil.objIsNull(data.getMsName(),"名称不能为空");
        saveOrUpdate(data);
        return Rep.ok();
    }

    @Override
    public Rep<MonitorScreenEntity> del(Req<MonitorScreenEntity, SettingUserVO> req) {
        MonitorScreenEntity data = req.getData();
        AssertUtil.objIsNull(data.getId(), "数据错误");
        monitorScreenMapper.deleteById(data.getId());
        return Rep.ok();
    }

    @Override
    public Rep<MonitorScreenEntity> findById(Req<MonitorScreenEntity, SettingUserVO> req) {
        MonitorScreenEntity data = req.getData();
        AssertUtil.objIsNull(data.getId(), "数据错误");
        MonitorScreenEntity monitorScreenEntity = monitorScreenMapper.findById(data.getId());
        AssertUtil.objIsNull(monitorScreenEntity,"数据错误");
        return Rep.ok(monitorScreenEntity);
    }


}
