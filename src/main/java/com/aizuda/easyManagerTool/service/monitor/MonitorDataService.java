package com.aizuda.easyManagerTool.service.monitor;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.monitor.MonitorEditDTO;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorDataEntity;
import com.aizuda.easyManagerTool.domain.entity.monitor.MonitorUrlEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

import java.util.List;
import java.util.Map;

public interface MonitorDataService {

    Rep<PageVO<MonitorDataEntity>> find(Req<PageDTO<MonitorDataEntity>, SettingUserVO> req);

    Rep<MonitorDataEntity> edit(Req<MonitorEditDTO, SettingUserVO> req);

    void edit(MonitorEditDTO monitorDataEntity);

    Rep<MonitorDataEntity> del(Req<MonitorDataEntity, SettingUserVO> req);

    Rep<Map<String,Object>> getUrl(Req<MonitorUrlEntity, SettingUserVO> req);

    void exec();

    Rep<MonitorDataEntity> clone(Req<MonitorEditDTO, SettingUserVO> req);

    Rep<List<MonitorUrlEntity>> urls(Req<MonitorEditDTO, SettingUserVO> req);

    Rep<List<MonitorUrlEntity>> delUrls(Req<MonitorUrlEntity, SettingUserVO> req);

    Rep<List<MonitorUrlEntity>> changeSwitch(Req<MonitorEditDTO, SettingUserVO> req);

    Rep<Map<String, Object>> getAllUrl(Req<MonitorDataEntity, SettingUserVO> req);

    Rep<Map<String, Object>> getAllUrls(Req<MonitorEditDTO, SettingUserVO> req);

    void pull();
}
