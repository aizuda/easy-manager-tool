package com.aizuda.easyManagerTool.service.gpt;

import com.plexpt.chatgpt.entity.chat.Message;
import lombok.Data;

@Data
public class MessageModel {

    /**
     * 指明发送给哪个 窗口
     */
    private Integer id;
    /**
     * 发送的内容
     */
    private Message msg;

}
