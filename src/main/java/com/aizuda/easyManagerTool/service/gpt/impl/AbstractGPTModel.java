package com.aizuda.easyManagerTool.service.gpt.impl;

import com.aizuda.easyManagerTool.domain.dto.gpt.GPTMessageDTO;
import com.aizuda.easyManagerTool.service.gpt.GPTModelService;
import com.aizuda.easyManagerTool.service.gpt.GPTService;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;

@Service
public class AbstractGPTModel implements GPTService {

    @Resource
    GPTStrategy gptStrategy;

    @Override
    public void action(GPTMessageDTO messageDTO, WebSocketSession webSocketSession){
        GPTModelService gptModelService = gptStrategy.get(messageDTO.getType());
        // 建立连接
        gptModelService.connect(messageDTO);
        // 回复
        gptModelService.streamReply(messageDTO,webSocketSession);
    }


}
