package com.aizuda.easyManagerTool.service.gpt;

import com.aizuda.easyManagerTool.domain.dto.gpt.GPTMessageDTO;
import org.springframework.web.socket.WebSocketSession;

public interface GPTService {

    void action(GPTMessageDTO messageDTO, WebSocketSession webSocketSession);

}
