package com.aizuda.easyManagerTool.service.gpt.impl;

import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.gpt.GPTMessageDTO;
import com.aizuda.easyManagerTool.domain.entity.gpt.GPTChartEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.mapper.gpt.GPTChartMapper;
import com.aizuda.easyManagerTool.service.gpt.GPTChartService;
import com.aizuda.easyManagerTool.service.gpt.GPTModel;
import com.aizuda.easyManagerTool.service.gpt.GPTService;
import com.aizuda.easyManagerTool.service.socket.impl.SocketSessionManager;
import com.aizuda.easyManagerTool.util.AssertUtil;
import com.aizuda.easyManagerTool.util.ThreadPoolUtil;
import com.plexpt.chatgpt.entity.chat.Message;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class GPTChartServiceImpl implements GPTChartService {

    @Resource
    private GPTChartMapper gptChartMapper;
    @Resource
    GPTService gptService;
    private ThreadPoolUtil executorService = new ThreadPoolUtil("GPT发送消息-");

    @Override
    public Rep<List<GPTChartEntity>> find(Req<GPTChartEntity, SettingUserVO> req){
        List<GPTChartEntity> gptChartEntities = gptChartMapper.findAll(req.getUser().getId());
        return Rep.ok(gptChartEntities);
    }

    @Override
    public Rep<List<Message>> findByContent(Req<GPTChartEntity, SettingUserVO> req) {
        GPTChartEntity data = req.getData();
        AssertUtil.objIsNull(data.getId(),"数据错误");
        GPTChartEntity gptChartEntity = gptChartMapper.selectById(data.getId());
        if(ObjectUtil.isEmpty(gptChartEntity)){
            return Rep.ok(new ArrayList<>());
        }
        List<Message> messages = (List<Message>) gptChartEntity.getGcContents();
        return Rep.ok(messages);
    }

    @Override
    public Rep<GPTMessageDTO> sendMessage(Req<GPTMessageDTO, SettingUserVO> req) {
        GPTModel gptModel = GPTModelManager.get(req.getUser().getTenantId());
        AssertUtil.objIsNull(gptModel,"还未进行GPT设置，请先设置");
        GPTMessageDTO data = req.getData();
        AssertUtil.objIsNull(data.getType(),"请选择AI类型");
        AssertUtil.objIsNull(data.getModel(),"请选择AI模型");
        AssertUtil.objIsNull(data.getContent(),"内容不能为空");
        GPTChartEntity gptChartEntity = gptChartMapper.selectById(data.getId());
        Message message = Message.builder()
                .content(data.getContent()).name(data.getName())
                .role(data.getRole()).functionCall(data.getFunction()).build();
        if(ObjectUtil.isEmpty(gptChartEntity)){
            gptChartEntity = new GPTChartEntity();
            gptChartEntity.setGcTitle(data.getGcTitle());
            gptChartEntity.setUserId(req.getUser().getId());
            gptChartEntity.setGcContents(Arrays.asList(message));
            gptChartMapper.insert(gptChartEntity);
        }else{
            gptChartEntity.setUserId(req.getUser().getId());
            List<Message> messages = (List<Message>) gptChartEntity.getGcContents();
            messages.add(message);
            gptChartMapper.updateById(gptChartEntity);
        }
        data.setId(gptChartEntity.getId());
        data.setGcTitle(gptChartEntity.getGcTitle());
        data.setTenantId(req.getUser().getTenantId());
        data.setGcContents(gptChartEntity.getGcContents());
        WebSocketSession webSocketSession = SocketSessionManager.get(req.getUser());
        // 调用接口
        executorService.execute(() -> gptService.action(data,webSocketSession));
        return Rep.ok(data);
    }

    @Override
    public Rep<GPTChartEntity> del(Req<GPTChartEntity, SettingUserVO> req) {
        GPTChartEntity data = req.getData();
        AssertUtil.objIsNull(data.getId(),"数据错误");
        gptChartMapper.deleteById(data.getId());
        return Rep.ok();
    }

    @Override
    public Rep<GPTChartEntity> editTitle(Req<GPTChartEntity, SettingUserVO> req) {
        GPTChartEntity data = req.getData();
        AssertUtil.objIsNull(data.getId(),"数据错误");
        AssertUtil.objIsNull(data.getGcTitle(),"标题不能为空");
        data.setUserId(req.getUser().getId());
        gptChartMapper.updateByTitle(data);
        return Rep.ok();
    }

    @Override
    public Rep<GPTChartEntity> addWindow(Req<GPTChartEntity, SettingUserVO> req) {
        GPTChartEntity data = req.getData();
        AssertUtil.objIsNull(data.getGcTitle(),"窗口名称不能为空");
        AssertUtil.objIsNotNull(data.getId(),"数据错误");
        data.setUserId(req.getUser().getId());
        gptChartMapper.insert(data);
        return Rep.ok(data);
    }

}
