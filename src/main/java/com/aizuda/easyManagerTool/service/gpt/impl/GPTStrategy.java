package com.aizuda.easyManagerTool.service.gpt.impl;

import com.aizuda.easyManagerTool.service.gpt.GPTModelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Slf4j
public class GPTStrategy {

    private Map<String, GPTModelService> gptModelServiceMap = new ConcurrentHashMap<>();

    @Autowired
    public GPTStrategy(Map<String, GPTModelService> gptModelServiceMap) {
        this.gptModelServiceMap = gptModelServiceMap;
    }

    public GPTModelService get(String name){
        return gptModelServiceMap.get(name);
    }

}
