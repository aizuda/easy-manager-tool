package com.aizuda.easyManagerTool.service.gpt;

import com.aizuda.easyManagerTool.domain.entity.gpt.GPTSettingEntity;
import com.plexpt.chatgpt.ChatGPTStream;
import lombok.Data;

@Data
public class GPTModel {
    /**
     * openai 温馨遗言 讯飞 ..
     */
    private String type;

    private ChatGPTStream openAiStream;
    /**
     * 所有GPT链接信息
     */
    private GPTSettingEntity gptSettingEntity;
    /**
     * 讯飞解析安全地址
     */
    private String xfUrl;
}
