package com.aizuda.easyManagerTool.service.gpt;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.entity.gpt.GPTSettingEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

public interface GPTSettingService {

    Rep<GPTSettingEntity> find(Req<GPTSettingEntity, SettingUserVO> req);

    Rep<GPTSettingEntity> edit(Req<GPTSettingEntity, SettingUserVO> req);

}
