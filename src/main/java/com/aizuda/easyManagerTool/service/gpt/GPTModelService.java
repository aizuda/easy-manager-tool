package com.aizuda.easyManagerTool.service.gpt;

import com.aizuda.easyManagerTool.domain.dto.gpt.GPTMessageDTO;
import org.springframework.web.socket.WebSocketSession;

public interface GPTModelService {

    void streamReply(GPTMessageDTO messageDTO, WebSocketSession webSocketSession);

    void reply(GPTMessageDTO messageDTO, WebSocketSession webSocketSession);

    void connect(GPTMessageDTO messageDTO);
}
