package com.aizuda.easyManagerTool.service.gpt.impl;

import com.aizuda.easyManagerTool.service.gpt.GPTModel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class GPTModelManager {

    private static Map<Integer, GPTModel> gptModelMap = new ConcurrentHashMap<>();

    public static void put(Integer tenantId,GPTModel gptModel){
        gptModelMap.put(tenantId,gptModel);
    }

    public static GPTModel get(Integer tenantId){
        return gptModelMap.get(tenantId);
    }

}
