package com.aizuda.easyManagerTool.service.gpt;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.gpt.GPTMessageDTO;
import com.aizuda.easyManagerTool.domain.entity.gpt.GPTChartEntity;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.plexpt.chatgpt.entity.chat.Message;

import java.util.List;

public interface GPTChartService {

    Rep<List<GPTChartEntity>> find(Req<GPTChartEntity, SettingUserVO> req);

    Rep<List<Message>> findByContent(Req<GPTChartEntity, SettingUserVO> req);

    Rep<GPTMessageDTO> sendMessage(Req<GPTMessageDTO, SettingUserVO> req);

    Rep<GPTChartEntity> del(Req<GPTChartEntity, SettingUserVO> req);

    Rep<GPTChartEntity> editTitle(Req<GPTChartEntity, SettingUserVO> req);

    Rep<GPTChartEntity> addWindow(Req<GPTChartEntity, SettingUserVO> req);

}
