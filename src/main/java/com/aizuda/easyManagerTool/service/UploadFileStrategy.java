package com.aizuda.easyManagerTool.service;

import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.aizuda.easyManagerTool.util.AssertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class UploadFileStrategy {

    private Map<String, UploadFileService> fileMap = new ConcurrentHashMap<>();

    @Autowired
    public UploadFileStrategy(Map<String, UploadFileService> fileMap) {
        this.fileMap = fileMap;
    }

    public boolean exec(String path, MultipartFile file, Map<String, String[]> map, SettingUserVO settingUserVO){
        AssertUtil.List.isEmpty(map.get("path"), "上传服务不存在");
        AssertUtil.objIsNull(fileMap.get(map.get("path")[0]), "上传服务不存在");
        return fileMap.get(map.get("path")[0]).exec(path,file,map,settingUserVO);
    }

    public void execAll(String path,MultipartFile file,Map<String, String[]> map,SettingUserVO settingUserVO){
        for (UploadFileService uploadFileService : fileMap.values()) {
            uploadFileService.exec(path,file,map,settingUserVO);
        }
    }

}
