package com.aizuda.easyManagerTool.service.docker;

import com.github.dockerjava.api.model.Statistics;

public interface DockerMonitorService {

    String monitor(Integer id, String containerId);

}
