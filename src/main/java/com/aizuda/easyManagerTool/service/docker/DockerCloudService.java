package com.aizuda.easyManagerTool.service.docker;


import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.docker.DockerPullImagesDTO;
import com.aizuda.easyManagerTool.domain.dto.docker.DockerTagDelDTO;
import com.aizuda.easyManagerTool.domain.entity.docker.DockerCloudEntity;
import com.aizuda.easyManagerTool.domain.entity.docker.DockerConfigEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerCloudFindVO;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerCloudTagListVO;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerConfigFindVO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;

import java.util.List;

public interface DockerCloudService {
    Rep<PageVO<DockerCloudFindVO>> find(Req<PageDTO<DockerCloudEntity>, SettingUserVO> req);

    Rep<List<DockerConfigFindVO>> findConfig(Req<DockerConfigEntity, SettingUserVO> req);

    Rep<DockerCloudEntity> edit(Req<DockerCloudEntity, SettingUserVO> req);

    Rep<DockerCloudEntity> del(Req<DockerCloudEntity, SettingUserVO> req);

    Rep<DockerConfigEntity> editConfig(Req<DockerConfigEntity, SettingUserVO> req);

    Rep<List<DockerCloudTagListVO>> tagList(Req<DockerCloudEntity, SettingUserVO> req);

    Rep<List<DockerCloudTagListVO>> tagDel(Req<DockerTagDelDTO, SettingUserVO> req);

    Rep<List<DockerCloudEntity>> pullImages(Req<DockerPullImagesDTO, SettingUserVO> req);
}
