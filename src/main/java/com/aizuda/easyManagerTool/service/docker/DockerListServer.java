package com.aizuda.easyManagerTool.service.docker;

import com.aizuda.easy.security.domain.Rep;
import com.aizuda.easy.security.domain.Req;
import com.aizuda.easyManagerTool.domain.dto.PageDTO;
import com.aizuda.easyManagerTool.domain.dto.docker.*;
import com.aizuda.easyManagerTool.domain.entity.docker.DockerListEntity;
import com.aizuda.easyManagerTool.domain.vo.PageVO;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerListVO;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerNetworkListVO;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerVolumeListDTO;
import com.aizuda.easyManagerTool.domain.vo.setting.SettingUserVO;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;

import java.util.List;

public interface DockerListServer {

    Rep<PageVO<DockerListVO>> find(Req<PageDTO<DockerListEntity>, SettingUserVO> req);

    Rep<DockerListVO> edit(Req<DockerListEntity, SettingUserVO> req);

    Rep<DockerListVO> del(Req<DockerListEntity, SettingUserVO> req);

    void refresh();

    Rep<List<Image>> imageList(Req<DockerListImageDTO, SettingUserVO> req);

    Rep<List<Image>> imageDel(Req<DockerListImageDTO, SettingUserVO> req);

    Rep<List<Image>> imageTagDel(Req<DockerListImageDTO, SettingUserVO> req);

    Rep<List<Image>> imagePush(Req<DockerListImageDTO, SettingUserVO> req);

    Rep<List<Image>> imagePull(Req<DockerListImageDTO, SettingUserVO> req);

    Rep<String> imageInfo(Req<DockerListImageDTO, SettingUserVO> req);


    Rep<List<Container>> containerList(Req<DockerListContainerDTO, SettingUserVO> req);

    Rep<List<Container>> containerRun(Req<DockerListContainerDTO, SettingUserVO> req);

    Rep<List<Container>> containerStop(Req<DockerListContainerDTO, SettingUserVO> req);

    Rep<List<Container>> containerDel(Req<DockerListContainerDTO, SettingUserVO> req);

    Rep<List<Container>> containerLog(Req<DockerListContainerDTO, SettingUserVO> req);

    Rep<List<Container>> containerTerminal(Req<DockerListContainerDTO, SettingUserVO> req);

    Rep<List<Container>> containerAdd(Req<DockerContainerAddDTO, SettingUserVO> req);

    Rep<String> containerInfo(Req<DockerListContainerDTO, SettingUserVO> req);

    Rep<List<DockerVolumeListDTO>> volumeList(Req<DockervolumeDTO, SettingUserVO> req);

    Rep<List<DockerVolumeListDTO>> volumeAdd(Req<DockervolumeDTO, SettingUserVO> req);

    Rep<List<DockerVolumeListDTO>> volumeDel(Req<DockervolumeDTO, SettingUserVO> req);

    Rep<List<DockerNetworkListVO>> networkList(Req<DockerNetworkDTO, SettingUserVO> req);

    Rep<List<DockerNetworkListVO>> networkAdd(Req<DockerNetworkDTO, SettingUserVO> req);

    Rep<List<DockerNetworkListVO>> networkDel(Req<DockerNetworkDTO, SettingUserVO> req);

    Rep<List<Container>> containerSynchronizationData(Req<DockerListContainerDTO, SettingUserVO> req);
}
