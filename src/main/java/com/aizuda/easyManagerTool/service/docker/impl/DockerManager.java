package com.aizuda.easyManagerTool.service.docker.impl;

import cn.hutool.core.util.ObjectUtil;
import com.aizuda.easyManagerTool.domain.vo.docker.DockerManagerVO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DockerManager {

    private static Map<Integer, DockerManagerVO> map = new ConcurrentHashMap<>();

    public static DockerManagerVO put(Integer id,DockerManagerVO dockerManagerVO){
        if(!map.containsKey(id)){
            map.put(id,dockerManagerVO);
            dockerManagerVO.refresh();
        }
        return dockerManagerVO;
    }

    public static DockerManagerVO del(Integer id){
        DockerManagerVO dockerManagerVO = map.get(id);
        try {
            dockerManagerVO.getDockerClient().close();
            if(ObjectUtil.isNotEmpty(dockerManagerVO.getManager())) {
                dockerManagerVO.getManager().close();
            }
            map.remove(id);
        }catch (Exception e){

        }
        return dockerManagerVO;
    }

    public static DockerManagerVO get(Integer id){
        return map.get(id);
    }

    public static List<DockerManagerVO> getList(){
        return new ArrayList<DockerManagerVO>(map.values());
    }

}
