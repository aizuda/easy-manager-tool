# 基础镜像
FROM openjdk:8-jre-slim
# 作者
MAINTAINER biguncle
# 配置
ENV JAVA_ARGS=""
ENV JAVA_OPTS=""
EXPOSE 8081
# 时区
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
# 添加应用
ADD ./target/easymt.jar /server/easymt.jar
# 创建一个工作目录，并将外部配置文件复制到镜像中
RUN mkdir /server/config
COPY /src/main/resources/application.yml /app/config/
WORKDIR /server
## 在镜像运行为容器后执行的命令
ENTRYPOINT java -jar $JAVA_OPTS -Dpolyglot.engine.WarnInterpreterOnly=false easymt.jar  $JAVA_ARGS
